package com.imweb.ers.base.root.dto;

import com.common.module.base.BaseDTO;

public class RootDTO extends BaseDTO {

    private String rootcode = "";
    private String status   = "";
    private String rootid   = "";
    private String rootpw   = "";
    private String rootname = "";
    private String authcode = "";

    public String getRootcode() {
        return rootcode;
    }

    public void setRootcode(String rootcode) {
        this.rootcode = rootcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRootid() {
        return rootid;
    }

    public void setRootid(String rootid) {
        this.rootid = rootid;
    }

    public String getRootpw() {
        return rootpw;
    }

    public void setRootpw(String rootpw) {
        this.rootpw = rootpw;
    }

    public String getRootname() {
        return rootname;
    }

    public void setRootname(String rootname) {
        this.rootname = rootname;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

}