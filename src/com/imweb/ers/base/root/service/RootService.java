package com.imweb.ers.base.root.service;

public interface RootService {

    public Object listProcess(Object param) throws Exception;

    public Object readProcess(Object param) throws Exception;

    public Object saveProcess(Object param) throws Exception;

}