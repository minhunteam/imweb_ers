package com.imweb.ers.base.root.service;

import java.util.ArrayList;
import java.util.List;

import com.common.module.base.BaseService;
import com.common.module.create.helper.CreateTranKeyHelper;
import com.common.module.create.vo.CreateTranKeyVO;
import com.common.module.transaction.TransactionHelper;
import com.imweb.ers.base.root.dao.RootDao;
import com.imweb.ers.base.root.dto.RootDTO;
import com.imweb.ers.base.root.vo.RootVO;
import com.imweb.ers.common.dto.SearchDTO;
import com.imweb.ers.common.vo.SearchVO;

public class RootServiceImpl extends BaseService implements RootService {

    private TransactionHelper transactionHelper;

    private CreateTranKeyHelper createTranKeyHelper;

    private RootDao rootDao;

    public void setTransactionHelper(TransactionHelper transactionHelper) {
        this.transactionHelper = transactionHelper;
    }

    public void setCreateTranKeyHelper(CreateTranKeyHelper createTranKeyHelper) {
        this.createTranKeyHelper = createTranKeyHelper;
    }

    public void setRootDao(RootDao rootDao) {
        this.rootDao = rootDao;
    }

    @Override
    public Object listProcess(Object param) throws Exception {

        SearchVO result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] listProcess START");

            SearchDTO search = new SearchDTO();

            SearchDTO dto = new SearchDTO();

            dto = (SearchDTO) param;

            logs.append(NL + "[" + TAG + "] SearchDTO dto [" + dto + "]");

            search.setLimit((Integer.parseInt(dto.getRowno()) - 1) * Integer.parseInt(dto.getRownum()));
            search.setRowno(dto.getRowno());
            search.setRownum(dto.getRownum());

            logs.append(NL + "[" + TAG + "] SearchDTO search [" + search + "]");

            String totalRecords = "";

            List items = new ArrayList();

            totalRecords = (String) rootDao.totalRecords(search);

            items = (ArrayList) rootDao.listProcess(search);

            result = new SearchVO();

            result.setTotalRecords(totalRecords);
            result.setItems(items);
            result.setRowno(dto.getRowno());
            result.setRownum(dto.getRownum());

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] listProcess Error | ", e);
            result = new SearchVO();
        } finally {
            logs.append(NL + "[" + TAG + "] listProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object readProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] readProcess START");

            SearchDTO search = new SearchDTO();

            SearchDTO dto = new SearchDTO();

            dto = (SearchDTO) param;

            search.setRootcode(dto.getRootcode());

            result = rootDao.readProcess(search);

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] readProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] readProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object saveProcess(Object param) throws Exception {

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] saveProcess START");

            processCode = "";

            RootDTO val = null;

            RootDTO dto = new RootDTO();

            RootVO check = new RootVO();

            dto = (RootDTO) param;

            if("".equals(dto.getRootcode())) {

                RootVO search = new RootVO();

                search.setRootid(dto.getRootid());

                check = (RootVO) rootDao.checkProcess(search);

                if(check == null) {

                    CreateTranKeyVO ctk = new CreateTranKeyVO();

                    ctk.setSeqType("ROOT");
                    ctk.setSeqLen(7);

                    String rootcode = (String) createTranKeyHelper.createProcess(ctk);

                    logs.append(NL + "[" + TAG + "] rootcode [" + rootcode + "]");

                    val = new RootDTO();

                    val.setRootcode(rootcode);
                    val.setRootid(dto.getRootid());
                    val.setRootpw(ase.encrypt(dto.getRootpw()));
                    val.setRootname(dto.getRootname());
                    val.setAuthcode(dto.getAuthcode());

                    processCode = "2001";

                } else {
                    returnCode = "4001";
                }

            } else {

                RootVO search = new RootVO();

                search.setRootcode(dto.getRootcode());

                check = (RootVO) rootDao.checkProcess(search);

                if(check == null) {
                    returnCode = "5001";
                } else {

                    val = new RootDTO();

                    val.setRootpw(dto.getRootcode());

                    if(!"".equals(dto.getRootpw())) {
                        val.setRootpw(ase.encrypt(dto.getRootpw()));
                    }

                    if(!"".equals(dto.getAuthcode())) {
                        val.setAuthcode(dto.getAuthcode());
                    }

                    processCode = "2002";
                }
            }

            if("2001".equals(processCode) || "2002".equals(processCode)) {

                transactionHelper.start();

                returnCode = (String) rootDao.saveProcess(val);

                if("0000".equals(returnCode)) {
                    transactionHelper.commit();
                }

                transactionHelper.end();

            }

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] saveProcess Error | ", e);
            returnCode = "9999";
            if("2001".equals(processCode) || "2002".equals(processCode)) {
                transactionHelper.end();
            }
        } finally {
            logs.append(NL + "[" + TAG + "] saveProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return returnCode;
    }

}