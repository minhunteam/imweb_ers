package com.imweb.ers.base.root.dao;

public interface RootDao {

    public Object checkProcess(Object param) throws Exception;

    public Object totalRecords(Object param) throws Exception;

    public Object listProcess(Object param) throws Exception;

    public Object readProcess(Object param) throws Exception;

    public Object saveProcess(Object param) throws Exception;

}