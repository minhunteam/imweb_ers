package com.imweb.ers.base.root.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.common.module.base.BaseAction;
import com.common.module.conf.Constants;
import com.common.module.custom.pager.PageNavigation;
import com.imweb.ers.base.root.dto.RootDTO;
import com.imweb.ers.base.root.service.RootService;
import com.imweb.ers.base.root.vo.RootVO;
import com.imweb.ers.common.dto.SearchDTO;
import com.imweb.ers.common.vo.SearchVO;
import com.imweb.ers.connect.dto.ConnectDTO;

public class RootAction extends BaseAction {

    private static boolean listInstanceRunning     = false;
    private static boolean viewInstanceRunning     = false;
    private static boolean saveFormInstanceRunning = false;
    private static boolean saveDoneInstanceRunning = false;

    private RootService rootService;

    public void setRootService(RootService rootService) {
        this.rootService = rootService;
    }

    public synchronized ActionForward list(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        while (listInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] list Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] list INTERRUPTED!!! error | ", e);
            }
        }

        listInstanceRunning = true;

        SearchDTO param = new SearchDTO();

        try {
            logs.append(NL + "[" + TAG + "] list START");

            fp.formRequestToVo(req, param);

            PageNavigation pager = new PageNavigation("20", param.getRowno());

            SearchVO item = (SearchVO) rootService.listProcess(param);

            if(isDevMode()) {
                logs.append(NL).append("================ * SearchVO * START ================");
                logs.append(NL).append(item);
                logs.append(NL).append(pager.toString());
                logs.append(NL).append("================ * SearchVO * END   ================");
            }

            pager.setTotalPages(item.getTotalRecords());

            req.setAttribute(Constants.OBJ_ITEM, item);
            req.setAttribute(Constants.OBJ_ITEMS, item.getItems());
            req.setAttribute(Constants.OBJ_PAGER, pager);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] list error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] list END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        listInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_LIST_KEY);
    }

    public synchronized ActionForward view(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        while (viewInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] view Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] view INTERRUPTED!!! error | ", e);
            }
        }

        viewInstanceRunning = true;

        SearchVO param = new SearchVO();

        try {
            logs.append(NL + "[" + TAG + "] view START");

            fp.formRequestToVo(req, param);

            RootVO item = (RootVO) rootService.readProcess(param);

            if(isDevMode()) {
                logs.append(NL).append("================ * returnCode * START ================");

                logs.append(NL).append("================ * returnCode * END   ================");
            }

            req.setAttribute(Constants.OBJ_ITEM, item);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] view error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] view END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        viewInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_READ_KEY);
    }

    public synchronized ActionForward saveForm(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        while (saveFormInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] saveForm Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] saveForm INTERRUPTED!!! error | ", e);
            }
        }

        saveFormInstanceRunning = true;

        try {
            logs.append(NL + "[" + TAG + "] saveForm START");

            RootVO vo = new RootVO();

            req.setAttribute("view", vo);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] saveForm error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] saveForm END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        saveFormInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_SAVEFORM_KEY);
    }

    public synchronized void saveDone(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        while (saveDoneInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] saveDone Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] saveDone INTERRUPTED!!! error | ", e);
            }
        }

        saveDoneInstanceRunning = true;

        RootDTO param = new RootDTO();

        try {
            logs.append(NL + "[" + TAG + "] saveDone START");

            fp.formRequestToVo(req, param);

            returnCode = (String) rootService.saveProcess(param);

            if(isDevMode()) {
                logs.append(NL).append("================ * returnCode * START ================");

                logs.append(NL).append("================ * returnCode * END   ================");
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] saveDone error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] saveDone END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        saveDoneInstanceRunning = false;

        notifyAll();

        res.setContentType("application/json; charset=UTF-8");
//        res.getWriter().append(gson.toJson(data));
    }
}