package com.imweb.ers.base.auth.service;

import java.util.ArrayList;
import java.util.List;

import com.common.module.base.BaseService;
import com.common.module.transaction.TransactionHelper;
import com.imweb.ers.base.auth.dao.AuthDao;
import com.imweb.ers.base.auth.vo.AuthVO;
import com.imweb.ers.common.dto.SearchDTO;
import com.imweb.ers.common.vo.SearchVO;

public class AuthServiceImpl extends BaseService implements AuthService {

    private TransactionHelper transactionHelper;

    private AuthDao authDao;

    public void setTransactionHelper(TransactionHelper transactionHelper) {
        this.transactionHelper = transactionHelper;
    }

    public void setAuthDao(AuthDao authDao) {
        this.authDao = authDao;
    }

    @Override
    public Object select(Object param) throws Exception {

        AuthVO result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] listProcess START");

            SearchVO search = new SearchVO();

            search = (SearchVO) param;

            logs.append(NL + "[" + TAG + "] SearchDTO search [" + search + "]");

            result = (AuthVO) authDao.select(search);
            
            logs.append(NL + "[" + TAG + "] AuthVO result [" + result + "]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] listProcess Error | ", e);
            result = new AuthVO();
        } finally {
            logs.append(NL + "[" + TAG + "] listProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object totalRecords(Object vo) throws Exception {
        return authDao.totalRecords(vo);
    }

    @Override
    public Object list(Object param) throws Exception {
        SearchVO result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] listProcess START");

            SearchDTO search = new SearchDTO();

            SearchDTO dto = new SearchDTO();

            dto = (SearchDTO) param;

            logs.append(NL + "[" + TAG + "] SearchDTO dto [" + dto + "]");

            search.setLimit((Integer.parseInt(dto.getRowno()) - 1) * Integer.parseInt(dto.getRownum()));
            search.setRowno(dto.getRowno());
            search.setRownum(dto.getRownum());

            logs.append(NL + "[" + TAG + "] SearchDTO search [" + search + "]");

            String totalRecords = "";

            List items = new ArrayList();

            totalRecords = (String) authDao.totalRecords(search);

            items = (ArrayList) authDao.list(search);

            result = new SearchVO();

            result.setTotalRecords(totalRecords);
            result.setItems(items);
            result.setRowno(dto.getRowno());
            result.setRownum(dto.getRownum());

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] listProcess Error | ", e);
            result = new SearchVO();
        } finally {
            logs.append(NL + "[" + TAG + "] listProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object saveProcess(Object o) throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] saveProcess START");
            AuthVO vo = new AuthVO();
            AuthVO save = new AuthVO();
            vo = (AuthVO) o;
            processCode = "";
            if("".equals(vo.getAuthname())) {
                returnCode = "1001";
            } else {
                if("".equals(vo.getAuthcode())) {
                    save.setAuthname(vo.getAuthname());
                    processCode = "2001";
                } else {
                    save.setAuthcode(vo.getAuthcode());
                    save.setAuthname(vo.getAuthname());
                    processCode = "2002";
                }
            }

            if("2001".equals(processCode) || "2002".equals(processCode)) {
                transactionHelper.start();
                if("2001".equals(processCode)) {
                    returnCode = (String) authDao.insertProcess(save);
                } else if("2002".equals(processCode)) {
                    returnCode = (String) authDao.updateProcess(save);
                }
                if("0000".equals(returnCode)) {
                    transactionHelper.commit();
                }
                transactionHelper.end();
            }
        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] saveProcess Error | ", e);
            returnCode = "9999";
            if("2001".equals(processCode) || "2002".equals(processCode)) {
                transactionHelper.end();
            }
        } finally {
            logs.append(NL + "[" + TAG + "] saveProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            }
        }
        return returnCode;
    }

    @Override
    public Object deleteProcess(Object o) throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] deleteProcess START");
            AuthVO vo = new AuthVO();
            AuthVO save = new AuthVO();
            vo = (AuthVO) o;
            processCode = "";
            if("".equals(vo.getAuthcode())) {
                returnCode = "1002";
            } else {
                save.setAuthcode(vo.getAuthcode());
                save.setAuthstate("20");
                processCode = "2001";
            }
            if("2001".equals(processCode)) {
                transactionHelper.start();
                returnCode = (String) authDao.stateProcess(save);
                if("0000".equals(returnCode)) {
                    returnCode = (String) authDao.updateProcess(save);
                }
                if("0000".equals(returnCode)) {
                    transactionHelper.commit();
                }
                transactionHelper.end();
            }
        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] deleteProcess Error | ", e);
            returnCode = "9999";
            if("2001".equals(processCode)) {
                transactionHelper.end();
            }
        } finally {
            logs.append(NL + "[" + TAG + "] deleteProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            }
        }
        return returnCode;
    }
}