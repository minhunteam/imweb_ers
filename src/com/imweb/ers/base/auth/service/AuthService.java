package com.imweb.ers.base.auth.service;

public interface AuthService {
    public Object select(Object vo)         throws Exception;
    public Object totalRecords(Object vo)   throws Exception;
    public Object list(Object vo)           throws Exception;
    public Object saveProcess(Object vo)    throws Exception;
    public Object deleteProcess(Object vo)  throws Exception;
}