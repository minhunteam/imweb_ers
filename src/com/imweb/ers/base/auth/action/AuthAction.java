package com.imweb.ers.base.auth.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.common.module.base.BaseAction;
import com.common.module.conf.Constants;
import com.common.module.custom.pager.PageNavigation;
import com.common.module.json.data.CommonJsonData;
import com.imweb.ers.base.auth.service.AuthService;
import com.imweb.ers.base.auth.vo.AuthVO;
import com.imweb.ers.common.dto.SearchDTO;
import com.imweb.ers.common.vo.SearchVO;

public class AuthAction extends BaseAction {

    private AuthService authService;

    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }

    public synchronized ActionForward list(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] list START");

            SearchDTO param = new SearchDTO();

            fp.formRequestToVo(req, param);

            PageNavigation pager = new PageNavigation("20", param.getRowno());

            SearchVO item = (SearchVO) authService.list(param);

            req.setAttribute(Constants.OBJ_ITEM, item);
            req.setAttribute(Constants.OBJ_ITEMS, item.getItems());
            req.setAttribute(Constants.OBJ_PAGER, pager);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] list error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] list END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return mapping.findForward(Constants.ACTION_FORWARD_LIST_KEY);
    }

    public synchronized ActionForward view(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] view START");

            SearchVO param = new SearchVO();

            fp.formRequestToVo(req, param);

            AuthVO item = (AuthVO) authService.select(param);

            if(isDevMode()) {
                logs.append(NL).append("================ * SearchVO * START ================");
                logs.append(NL).append(item);
                logs.append(NL).append("================ * SearchVO * END   ================");
            }

            req.setAttribute(Constants.OBJ_ITEM, item);
            req.setAttribute(Constants.OBJ_ITEMS, item.getItems());

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] view error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] view END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
        
        return mapping.findForward(Constants.ACTION_FORWARD_READ_KEY);
    }

    public synchronized ActionForward saveForm(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] saveForm START");

            AuthVO item = null;

            AuthVO param = new AuthVO();

            fp.formRequestToVo(req, param);

            if("".equals(param.getAuthcode())) {
                item = new AuthVO();
            } else {
                item = (AuthVO) authService.select(param);
            }

            req.setAttribute(Constants.OBJ_ITEM, item);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] saveForm error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] saveForm END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
        
        return mapping.findForward(Constants.ACTION_FORWARD_SAVEFORM_KEY);
    }

    public synchronized void saveDone(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {
        try {
            logs = new StringBuilder();
            String msg = "";
            StringBuilder restful = new StringBuilder();

            logs.append(NL + "[" + TAG + "] saveProcess START");

            AuthVO param = new AuthVO();

            fp.formRequestToVo(req, param);

            returnCode = (String) authService.saveProcess(param);

            restful.append("{" + NL);
            restful.append(" \"success\": \"" + returnCode + "\"," + NL);
            restful.append(" \"result\" : {" + NL);
            if("0000".equals(returnCode)) {
                restful.append(NL + "\"message\" : \"[성공] 저장이 완료 되었습니다.\"" + NL);
            } else if("1001".equals(returnCode)) {
                restful.append(NL + "\"message\" : \"[실패] 권한 명을 입력해 주세요.\"" + NL);
            } else {
                restful.append(NL + "\"message\" : \"[실패] 시스템 에러 입니다. 관리자에게 문의를 해주세요.\"" + NL);
            }
            restful.append("            }" + NL);
            restful.append("}");
            logs.append(NL + "[" + TAG + "] restful [" + NL + restful.toString() + NL + "]");

            if("0000".equals(returnCode)) {
                msg = "저장";
            } else if("1001".equals(returnCode)) {
                msg = "권한 명";
            }

            CommonJsonData data = new CommonJsonData();

            data.setSuccess(returnCode);

            logs.append(NL + "[" + TAG + "] restful [" + NL + gson.toJson(data) + NL + "]");

            res.setContentType("application/json; charset=UTF-8");
            res.getWriter().append(gson.toJson(data));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] saveProcess error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] saveProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
    }

    public synchronized void delDone(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {
        try {
            logs = new StringBuilder();
            String msg = "";
            StringBuilder restful = new StringBuilder();
            logs.append(NL + "[" + TAG + "] delDone START");

            AuthVO param = new AuthVO();

            fp.formRequestToVo(req, param);

            returnCode = (String) authService.deleteProcess(param);

            restful.append("{" + NL);
            restful.append(" \"success\": \"" + returnCode + "\"," + NL);
            restful.append(" \"result\" : {" + NL);
            if("0000".equals(returnCode)) {
                restful.append(NL + "\"message\" : \"[성공] 삭제가 완료 되었습니다.\"" + NL);
            } else if("1001".equals(returnCode)) {
                restful.append(NL + "\"message\" : \"[실패] 권한 코드를 입력해 주세요.\"" + NL);
            } else {
                restful.append(NL + "\"message\" : \"[실패] 시스템 에러 입니다. 관리자에게 문의를 해주세요.\"" + NL);
            }
            restful.append("            }" + NL);
            restful.append("}");
            logs.append(NL + "[" + TAG + "] restful [" + NL + restful.toString() + NL + "]");

            if("0000".equals(returnCode)) {
                msg = "저장";
            } else if("1002".equals(returnCode)) {
                msg = "권한 코드";
            }

            CommonJsonData data = new CommonJsonData();

            data.setSuccess(returnCode);

            logs.append(NL + "[" + TAG + "] restful [" + NL + gson.toJson(data) + NL + "]");

            res.setContentType("application/json; charset=UTF-8");
            res.getWriter().append(gson.toJson(data));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] delDone error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] delDone END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
    }
}