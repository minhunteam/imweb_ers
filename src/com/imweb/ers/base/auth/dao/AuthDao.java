package com.imweb.ers.base.auth.dao;

public interface AuthDao {
    public Object select(Object vo)         throws Exception;
    public Object totalRecords(Object vo)   throws Exception;
    public Object list(Object vo)           throws Exception;
    public Object checkProcess(Object vo)   throws Exception;
    public Object insertProcess(Object vo)  throws Exception;
    public Object stateProcess(Object vo)   throws Exception;
    public Object updateProcess(Object vo)  throws Exception;
}