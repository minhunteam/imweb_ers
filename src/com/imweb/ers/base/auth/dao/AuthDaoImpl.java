package com.imweb.ers.base.auth.dao;

import com.common.module.base.BaseDao;

public class AuthDaoImpl extends BaseDao implements AuthDao {

    private final static String TAG = "AuthDao";

    @Override
    public Object select(Object vo) throws Exception {
        return dao.select("ibatis.mysql.sixmac.base.auth.select", vo);
    }

    @Override
    public Object totalRecords(Object vo) throws Exception {
        return dao.select("ibatis.mysql.sixmac.base.auth.totalRecords", vo);
    }

    @Override
    public Object list(Object vo) throws Exception {
        return dao.list("ibatis.mysql.sixmac.base.auth.list", vo);
    }

    @Override
    public Object checkProcess(Object vo) throws Exception {
        return dao.list("ibatis.mysql.sixmac.base.auth.checkProcess", vo);
    }

    @Override
    public Object insertProcess(Object vo) throws Exception {
        int i = 0;
        try{
            i = dao.add(vo,"ibatis.mysql.sixmac.base.auth.insertProcess");
            if(i == 0){
                returnCode = "0001";
            } else if(i == -1){
                returnCode = "9999";
            } else {
                returnCode = "0000";
            }
        } catch (Exception e){
            log.error(NL + "["+ TAG +"] insertProcess Error | ", e);
            returnCode = "9999";
        } finally {
            return returnCode;
        }
    }

    @Override
    public Object stateProcess(Object vo) throws Exception {
        int i = 0;
        try{
            i = dao.add(vo,"ibatis.mysql.sixmac.base.auth.stateProcess");
            if(i == 0){
                returnCode = "0001";
            } else if(i == -1){
                returnCode = "9999";
            } else {
                returnCode = "0000";
            }
        } catch (Exception e){
            log.error(NL + "["+ TAG +"] stateProcess Error | ", e);
            returnCode = "9999";
        } finally {
            return returnCode;
        }
    }

    @Override
    public Object updateProcess(Object vo) throws Exception {
        int i = 0;
        try{
            i = dao.mod("ibatis.mysql.sixmac.base.auth.updateProcess",vo);
            if(i == 0){
                returnCode = "0001";
            } else if(i == -1){
                returnCode = "9999";
            } else {
                returnCode = "0000";
            }
        } catch (Exception e){
            log.error(NL + "["+ TAG +"] updateProcess Error | ", e);
            returnCode = "9999";
        } finally {
            return returnCode;
        }
    }
}