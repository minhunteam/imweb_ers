package com.imweb.ers.base.auth.vo;

import com.common.module.base.BaseVO;

public class AuthVO extends BaseVO {

    private String authcode  = "";
    private String authname  = "";
    private String authstate = "";

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public String getAuthname() {
        return authname;
    }

    public void setAuthname(String authname) {
        this.authname = authname;
    }

    public String getAuthstate() {
        return authstate;
    }

    public void setAuthstate(String authstate) {
        this.authstate = authstate;
    }
}