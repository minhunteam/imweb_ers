package com.imweb.ers.common.parser;

import java.util.ArrayList;
import java.util.List;

import com.common.module.base.BaseResultVO;

public class ResultExcelParserVO extends BaseResultVO {

    private List items = new ArrayList();

    public List getItems() {
        return items;
    }

    public void setItems(List items) {
        this.items = items;
    }
}