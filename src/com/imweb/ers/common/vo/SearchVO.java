package com.imweb.ers.common.vo;

import com.common.module.base.BaseVO;

public class SearchVO extends BaseVO {

    private String authcode  = "";
    private String storecode = "";

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public String getStorecode() {
        return storecode;
    }

    public void setStorecode(String storecode) {
        this.storecode = storecode;
    }

}