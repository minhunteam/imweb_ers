package com.imweb.ers.common.dto;

import com.common.module.base.BaseDTO;

public class SearchDTO extends BaseDTO {

    private String rootid    = "";
    private String rootcode  = "";
    private String storecode = "";
    private String startNo   = "";
    private String endNo     = "";

    public String getRootid() {
        return rootid;
    }

    public void setRootid(String rootid) {
        this.rootid = rootid;
    }

    public String getRootcode() {
        return rootcode;
    }

    public void setRootcode(String rootcode) {
        this.rootcode = rootcode;
    }

    public String getStorecode() {
        return storecode;
    }

    public void setStorecode(String storecode) {
        this.storecode = storecode;
    }

    public String getStartNo() {
        return startNo;
    }

    public void setStartNo(String startNo) {
        this.startNo = startNo;
    }

    public void setStartNo(int startNo) {
        this.startNo = Integer.toString(startNo);;
    }

    public String getEndNo() {
        return endNo;
    }

    public void setEndNo(String endNo) {
        this.endNo = endNo;
    }

    public void setEndNo(int endNo) {
        this.endNo = Integer.toString(endNo);
    }
}