package com.imweb.ers.connect.dao;

import org.springframework.util.StopWatch;

import com.common.module.base.BaseDao;

public class ConnectRootDaoImpl extends BaseDao implements ConnectRootDao {

    @Override
    public Object readProcess(Object param) throws Exception {
        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] readProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.select("ibatis.mysql.imweb.ers.base.root.readProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] readProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] readProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] readProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }
}