package com.imweb.ers.connect.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.common.module.base.BaseAction;
import com.common.module.conf.Constants;
import com.common.module.session.vo.UserSessionVO;
import com.imweb.ers.connect.dto.ConnectDTO;
import com.imweb.ers.connect.service.ConnectService;

public class ConnectAction extends BaseAction {

    private static boolean loginInstanceRunning  = false;
    private static boolean logoutInstanceRunning = false;

    private ConnectService connectService;

    public void setConnectService(ConnectService connectService) {
        this.connectService = connectService;
    }

    public synchronized void login(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        while (loginInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] login Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] login INTERRUPTED!!! error | ", e);
            }
        }

        loginInstanceRunning = true;

        ConnectDTO param = new ConnectDTO();

        try {
            logs.append(NL + "[" + TAG + "] login START");

            fp.formRequestToVo(req, param);

            if("R001".equals(param.getRoute()) || "R002".equals(param.getRoute())) {
                connectService.loginProcess(param, req);
            }

            if(isDevMode()) {
                logs.append(NL).append("================ * returnCode * START ================");

                logs.append(NL).append("================ * returnCode * END   ================");
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] login error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] login END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        loginInstanceRunning = false;

        notifyAll();

        HttpSession session = req.getSession(true);

        UserSessionVO user = new UserSessionVO();

        user = (UserSessionVO) session.getAttribute(Constants.USER_SESSION);

//        System.out.println("user prefix[" + user + "]");

        if(user == null) {
            System.out.println("1");
            if("R001".equals(param.getRoute())) {
                System.out.println("1-1");
                res.sendRedirect("/connect/root.do");
            } else if("R002".equals(param.getRoute())) {
                System.out.println("1-2");
                res.sendRedirect("/connect/root.do");
            } else {
                System.out.println("1-3");
                res.sendRedirect("/");
            }
        } else {
            System.out.println("2");
            if("R001".equals(user.getRoute())) {
                System.out.println("2-1");
                res.sendRedirect("/main/root.do");
            } else if("R002".equals(user.getRoute())) {
                System.out.println("2-2");
                res.sendRedirect("/main/store.do");
            } else {
                System.out.println("2-3");
                res.sendRedirect("/");
            }
        }

    }

    public synchronized void logout(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        while (logoutInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] login Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] login INTERRUPTED!!! error | ", e);
            }
        }

        logoutInstanceRunning = true;

        ConnectDTO param = new ConnectDTO();

        try {
            logs.append(NL + "[" + TAG + "] login START");

            fp.formRequestToVo(req, param);

            if("R001".equals(param.getRoute()) || "R002".equals(param.getRoute())) {
                connectService.logoutProcess(req);
            }

            if(isDevMode()) {
                logs.append(NL).append("================ * returnCode * START ================");

                logs.append(NL).append("================ * returnCode * END   ================");
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] login error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] login END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        logoutInstanceRunning = false;

        notifyAll();

        if("R001".equals(param.getRoute())) {
            res.sendRedirect("/connect/root.do");
        } else if("R002".equals(param.getRoute())) {
            res.sendRedirect("/connect/root.do");
        } else {
            res.sendRedirect("/");
        }

    }
}