package com.imweb.ers.connect.service;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.common.module.base.BaseService;
import com.common.module.conf.Constants;
import com.common.module.session.vo.UserSessionVO;
import com.imweb.ers.base.root.vo.RootVO;
import com.imweb.ers.connect.dao.ConnectRootDao;
import com.imweb.ers.connect.dao.ConnectStoreDao;
import com.imweb.ers.connect.dto.ConnectDTO;
import com.imweb.ers.store.info.vo.StoreVO;

public class ConnectServiceImpl extends BaseService implements ConnectService {

    private ConnectRootDao  connectRootDao;
    private ConnectStoreDao connectStoreDao;

    public void setConnectRootDao(ConnectRootDao connectRootDao) {
        this.connectRootDao = connectRootDao;
    }

    public void setConnectStoreDao(ConnectStoreDao connectStoreDao) {
        this.connectStoreDao = connectStoreDao;
    }

    @Override
    public void loginProcess(Object param, HttpServletRequest req) throws Exception {

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] loginProcess START");

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            ConnectDTO dto = new ConnectDTO();

            HttpSession session = req.getSession(true);
            session.setMaxInactiveInterval(60 * 60 * 3);
            UserSessionVO login = null;

            dto = (ConnectDTO) param;

            /* 슈퍼 관리자 */
            if(login == null || !login.isLogin()) {
                if(dto.getUserid().equals(UserSessionVO.SUPERVISOR_ID)) {
                    login = new UserSessionVO();
                    login.initSuperVisor();
                    // - 세션정보 설정
                    login = UserSessionVO.getInstance(login);
                    session.setAttribute(Constants.USER_SESSION, login);
                    returnCode = "0000";
                }
            }

            /* 관리자 */
            if(login == null || !login.isLogin()) {
                if("R001".equals(dto.getRoute())) {

                    String pw = ase.encrypt(dto.getUserpw().toLowerCase());

                    RootVO val = new RootVO();

                    val.setRootid(dto.getUserid());
                    val.setRootpw(pw);

                    RootVO vo = (RootVO) connectRootDao.readProcess(val);

                    if(vo == null) {
                        session.setAttribute(Constants.USER_SESSION, null);
                    } else {
                        logs.append(NL + "[" + TAG + "] decrypt[" + ase.decrypt(vo.getRootpw()) + "]");

                        if(pw.equals(vo.getRootpw())) {
                            login = new UserSessionVO();
                            // - 세션정보 설정
                            login = UserSessionVO.getInstance(vo);
                            session.setAttribute(Constants.USER_SESSION, login);
                            returnCode = "0000";
                        }
                    }

                }
            }

            /* 매장 운영자 */
            if(login == null || !login.isLogin()) {
                String pw = ase.encrypt(dto.getUserpw().toLowerCase());

                StoreVO val = new StoreVO();

                val.setStoreid(dto.getUserid());
                val.setStorepw(pw);

                StoreVO vo = (StoreVO) connectStoreDao.readProcess(val);

                if(vo == null) {
                    session.setAttribute(Constants.USER_SESSION, null);
                } else {
                    logs.append(NL + "[" + TAG + "] decrypt[" + ase.decrypt(vo.getStorepw()) + "]");

                    if(pw.equals(vo.getStorepw())) {
                        login = new UserSessionVO();
                        // - 세션정보 설정
                        login = UserSessionVO.getInstance(vo);
                        session.setAttribute(Constants.USER_SESSION, login);
                        returnCode = "0000";
                    }
                }
            }

            if("0000".equals(returnCode)) {
                logs.append(NL + "[" + TAG + "] session isNew[" + session.isNew() + "]");
                UserSessionVO user = new UserSessionVO();
                user = (UserSessionVO) session.getAttribute(Constants.USER_SESSION);
                logs.append(NL + "[" + TAG + "] session user[" + user + "]");
                logs.append(NL + "[" + TAG + "] session getId[" + session.getId() + "]");
                logs.append(NL + "[" + TAG + "] session isNew[" + session.isNew() + "]");
                logs.append(NL + "[" + TAG + "] session getCreationTime[" + formatter.format(session.getCreationTime()) + "]");
                logs.append(NL + "[" + TAG + "] session LastAccessedTime[" + formatter.format(session.getLastAccessedTime()) + "]");
                logs.append(NL + "[" + TAG + "] session MaxInactiveInterval[" + session.getMaxInactiveInterval() + "]");
            } else {
                session.setAttribute(Constants.USER_SESSION, null);
            }

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] loginProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] loginProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
    }

    @Override
    public void logoutProcess(HttpServletRequest req) throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] logoutProcess START");
            HttpSession session = req.getSession(true);
            session.setMaxInactiveInterval(60 * 60 * 3);
            session.setAttribute(Constants.USER_SESSION, null);
        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] logoutProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] logoutProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
    }

}