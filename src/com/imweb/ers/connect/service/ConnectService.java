package com.imweb.ers.connect.service;

import javax.servlet.http.HttpServletRequest;

public interface ConnectService {

    public void loginProcess(Object param, HttpServletRequest req) throws Exception;

    public void logoutProcess(HttpServletRequest req) throws Exception;

}