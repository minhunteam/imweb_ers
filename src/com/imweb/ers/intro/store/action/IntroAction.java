package com.imweb.ers.intro.store.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.common.module.base.BaseAction;
import com.common.module.conf.Constants;

public class IntroAction extends BaseAction {

    private static boolean introInstanceRunning = false;

    public synchronized ActionForward intro(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        while (introInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] intro Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] intro INTERRUPTED!!! error | ", e);
            }
        }

        introInstanceRunning = true;

        try {
            logs.append(NL + "[" + TAG + "] list START");

            /* SearchVo param = new SearchVo(); */
            /* BestMateJson json = new BestMateJson(); */

            /* fp.formRequestToVo(req, param); */

            /* json = (BestMateJson) bestMateV3Service.listProcess(param); */

            if(isDevMode()) {
                logs.append(NL).append("================ * gson * START ================");
                /* logs.append(NL).append(gson.toJson(json)).append("]"); */
                logs.append(NL).append("================ * gson * END   ================");
            }

            req.setAttribute(Constants.ACTION_FORWARD_SUCCESS_KEY, gson.toJson(""));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] list error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] list END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        introInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_SUCCESS_KEY);

    }
}