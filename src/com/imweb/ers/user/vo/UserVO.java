package com.imweb.ers.user.vo;

import com.common.module.base.BaseDTO;

public class UserVO extends BaseDTO {

    public static final String SUPERVISOR_ID  = "supervisor";
    public static final String SUPERVISOR_PWD = "supervisor";

    private String userno   = "";
    private String userid   = "";
    private String userpw   = "";
    private String username = "";
    private String authcode = "";

    public void initSuperVisor() {
        this.userno = "ROOT2019030100000001";
        this.userid = UserVO.SUPERVISOR_ID;
        this.userpw = UserVO.SUPERVISOR_PWD;
        this.username = "슈퍼관리자";
        this.authcode = "A0001";
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserpw() {
        return userpw;
    }

    public void setUserpw(String userpw) {
        this.userpw = userpw;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public static String getSupervisorId() {
        return SUPERVISOR_ID;
    }

    public static String getSupervisorPwd() {
        return SUPERVISOR_PWD;
    }
}