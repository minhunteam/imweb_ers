package com.imweb.ers.store.item.vo;

import com.common.module.base.BaseVO;

public class StoreItemVO extends BaseVO {

    private String storeitemcode = "";
    private String storeitemname = "";
    private String storecode     = "";
    private String storename     = "";
    private String itemsize      = "";
    private String price         = "";
    private String dcprice       = "";

    public String getStoreitemcode() {
        return storeitemcode;
    }

    public void setStoreitemcode(String storeitemcode) {
        this.storeitemcode = storeitemcode;
    }

    public String getStorecode() {
        return storecode;
    }

    public void setStorecode(String storecode) {
        this.storecode = storecode;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getStoreitemname() {
        return storeitemname;
    }

    public void setStoreitemname(String storeitemname) {
        this.storeitemname = storeitemname;
    }

    public String getItemsize() {
        return itemsize;
    }

    public void setItemsize(String itemsize) {
        this.itemsize = itemsize;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDcprice() {
        return dcprice;
    }

    public void setDcprice(String dcprice) {
        this.dcprice = dcprice;
    }
}