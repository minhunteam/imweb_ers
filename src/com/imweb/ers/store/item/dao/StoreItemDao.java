package com.imweb.ers.store.item.dao;

public interface StoreItemDao {

    public Object checkProcess(Object param) throws Exception;

    public Object totalRecords(Object param) throws Exception;

    public Object listProcess(Object param) throws Exception;

    public Object readProcess(Object param) throws Exception;

    public Object excelProcess(Object param) throws Exception;

    public Object updateProcess(Object param) throws Exception;

    public Object saveProcess(Object param) throws Exception;

}