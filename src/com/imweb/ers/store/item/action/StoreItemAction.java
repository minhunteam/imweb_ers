package com.imweb.ers.store.item.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.bytecode.opencsv.CSVWriter;
import com.common.module.base.BaseAction;
import com.common.module.conf.Constants;
import com.common.module.custom.pager.PageNavigation;
import com.common.module.json.data.CommonJsonData;
import com.imweb.ers.base.root.dto.RootDTO;
import com.imweb.ers.base.root.vo.RootVO;
import com.imweb.ers.common.dto.SearchDTO;
import com.imweb.ers.common.vo.SearchVO;
import com.imweb.ers.store.item.service.StoreItemService;
import com.imweb.ers.store.item.vo.StoreItemVO;

public class StoreItemAction extends BaseAction {

    private static boolean listInstanceRunning     = false;
    private static boolean readInstanceRunning     = false;
    private static boolean saveFormInstanceRunning = false;
    private static boolean saveDoneInstanceRunning = false;
    private static boolean batchInstanceRunning    = false;
    private static boolean excelInstanceRunning    = false;

    private StoreItemService storeItemService;

    public void setStoreItemService(StoreItemService storeItemService) {
        this.storeItemService = storeItemService;
    }

    public synchronized ActionForward list(ActionMapping mapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logs = new StringBuilder();

        while (listInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] list Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] list INTERRUPTED!!! error | ", e);
            }
        }

        listInstanceRunning = true;

        SearchDTO param = new SearchDTO();

        try {
            logs.append(NL + "[" + TAG + "] list START");

            fp.formRequestToVo(request, param);

            PageNavigation pager = new PageNavigation("15", param.getRowno());

            SearchVO item = (SearchVO) storeItemService.listProcess(param);

            pager.setTotalRecords(item.getTotalRecords());

            if(isDevMode()) {
                logs.append(NL).append("================ * SearchVO * START ================");
                logs.append(NL).append(item.getTotalRecords());
                logs.append(NL).append(pager.getTotalRecords());
                logs.append(NL).append("================ * SearchVO * END   ================");
            }

            request.setAttribute(Constants.OBJ_ITEM, item);
            request.setAttribute(Constants.OBJ_ITEMS, item.getItems());
            request.setAttribute(Constants.OBJ_PAGER, pager);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] list error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] list END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        listInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_LIST_KEY);
    }

    public synchronized ActionForward read(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        while (readInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] view Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] view INTERRUPTED!!! error | ", e);
            }
        }

        readInstanceRunning = true;

        SearchVO param = new SearchVO();

        try {
            logs.append(NL + "[" + TAG + "] view START");

            fp.formRequestToVo(req, param);

//            RootVO item = (RootVO) rootService.readProcess(param);

            if(isDevMode()) {
                logs.append(NL).append("================ * returnCode * START ================");

                logs.append(NL).append("================ * returnCode * END   ================");
            }

//            req.setAttribute(Constants.OBJ_ITEM, item);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] view error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] view END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        readInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_READ_KEY);
    }

    public synchronized ActionForward saveForm(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        while (saveFormInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] saveForm Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] saveForm INTERRUPTED!!! error | ", e);
            }
        }

        saveFormInstanceRunning = true;

        try {
            logs.append(NL + "[" + TAG + "] saveForm START");

            RootVO vo = new RootVO();

            req.setAttribute("view", vo);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] saveForm error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] saveForm END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        saveFormInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_SAVEFORM_KEY);
    }

    public synchronized void saveDone(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        while (saveDoneInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] saveDone Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] saveDone INTERRUPTED!!! error | ", e);
            }
        }

        saveDoneInstanceRunning = true;

        RootDTO param = new RootDTO();

        try {
            logs.append(NL + "[" + TAG + "] saveDone START");

            fp.formRequestToVo(req, param);

//            returnCode = (String) rootService.saveProcess(param);

            if(isDevMode()) {
                logs.append(NL).append("================ * returnCode * START ================");

                logs.append(NL).append("================ * returnCode * END   ================");
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] saveDone error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] saveDone END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        saveDoneInstanceRunning = false;

        notifyAll();

        res.setContentType("application/json; charset=UTF-8");
//        res.getWriter().append(gson.toJson(data));
    }

    public synchronized ActionForward batchForm(ActionMapping mapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logs = new StringBuilder();

        try {
            logs.append(NL + "[" + TAG + "] batchForm START");

            RootVO vo = new RootVO();

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] batchForm error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] batchForm END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return mapping.findForward(Constants.ACTION_FORWARD_BATCHFORM_KEY);
    }

    public synchronized void batchProcess(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        try {
            logs.append(NL + "[" + TAG + "] batchProcess START");

            storeItemService.batchProcess(req);

            CommonJsonData data = new CommonJsonData();

            data.setSuccess("0000");

            logs.append(NL + "[" + TAG + "] restful [" + NL + gson.toJson(data) + NL + "]");

            res.setContentType("application/json; charset=UTF-8");
            res.getWriter().append(gson.toJson(data));

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] batchProcess error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] batchProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

    }

    public synchronized void excelProcess(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        try {
            logs.append(NL + "[" + TAG + "] batchProcess START");

            String docuType = req.getParameter("docuType") != null ? req.getParameter("docuType") : "item_";

            List<StoreItemVO> items = (ArrayList<StoreItemVO>) storeItemService.excelProcess(req);

            Calendar c1 = null;
            Date date1 = null;
            String unq = "";
            c1 = Calendar.getInstance();
            date1 = (Date) c1.getTime();

            SimpleDateFormat sdf = new SimpleDateFormat();
            sdf.applyPattern("yyyyMMddhhmmss");

            unq = sdf.format(date1);

            String csvfilePath = "";

            String fileTempname = docuType + unq + ".csv";
            // String csvfilePath = "C:\\test" + fileTempname;
//            String csvfilePath = "/data1/amcs/temp/" + fileTempname;
            if("192.168.0.250".equals(getIpAddress())) {
                csvfilePath = "D:\\workspaces\\was\\imweb_ers\\WebContent\\data\\upload\\" + fileTempname;
            } else {
                csvfilePath = "/source/app/web/imweb_ers/WebContent/data/upload/" + fileTempname;
            }

            String[] entrynames = null;

            entrynames = new String[11];
            entrynames[0] = "매장코드";
            entrynames[1] = "매장상품코드";
            entrynames[2] = "매장상품명";
            entrynames[3] = "사이즈";
            entrynames[4] = "할인이전가격";
            entrynames[5] = "판매가격";

            /**
             * <pre>
             * CSVWriter writer = new CSVWriter(new java.io.FileWriter(csvfilePath), ',', '\u0000', '\u0000');
             * </pre>
             */

            CSVWriter writer = new CSVWriter(new FileWriterWithEncoding(csvfilePath, "EUC-KR"), ',', '\u0000', '\u0000');

            writer.writeNext(entrynames);

            String[] entries = null;

            for(StoreItemVO row : items) {

                entries = new String[11];

                entries[0] = row.getStorecode();
                entries[1] = row.getStoreitemcode();
                entries[2] = row.getStoreitemname();
                entries[3] = row.getItemsize();
                entries[4] = row.getPrice();
                entries[5] = row.getDcprice();

                writer.writeNext(entries);
            }

            writer.close();

            download(req, res, csvfilePath, fileTempname);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] batchProcess error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] batchProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
    }

    public static void download(HttpServletRequest request, HttpServletResponse response, String filePath, String saveFileName) throws Exception {

        String encodedFileName = URLEncoder.encode(saveFileName, "EUC-KR");

        BufferedInputStream in = null;
        BufferedOutputStream out = null;

        try {
            /**
             * 다운로드 처리
             */
            File downFile = new File(filePath);

            int fileSize = (int) downFile.length();

            request.setCharacterEncoding("EUC-KR");

            String strClient = request.getHeader("User-Agent");
            String fileType = null;

            if(strClient.indexOf("MSIE 5.5") != -1) {
                response.setHeader("Content-Disposition", "filename=" + encodedFileName + ";");
                fileType = "doesn/matter";
            } else {
                response.setHeader("Content-Disposition", "attachment;filename=" + encodedFileName + ";");
                fileType = "application/x-msdownload";
            }

            response.setContentType(fileType + ";charset=EUC-KR");
            response.setContentLength(fileSize);

            byte b[] = new byte[1024];

            if(downFile.isFile()) {
                in = new BufferedInputStream(new FileInputStream(downFile));
                out = new BufferedOutputStream(response.getOutputStream());

                int read = -1;
                while ((read = in.read(b)) != -1) {
                    out.write(b, 0, read);
                }
            }

        } catch (FileNotFoundException fnfe) {
            throw fnfe;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if(in != null) {
                in.close();
            }

            if(out != null) {
                out.close();
            }
        }
    }
}