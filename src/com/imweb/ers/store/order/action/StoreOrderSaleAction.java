package com.imweb.ers.store.order.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.bytecode.opencsv.CSVWriter;
import com.common.module.base.BaseAction;
import com.common.module.conf.Constants;
import com.common.module.custom.pager.PageNavigation;
import com.imweb.ers.common.dto.SearchDTO;
import com.imweb.ers.common.vo.SearchVO;
import com.imweb.ers.store.order.service.StoreOrderSaleService;
import com.imweb.ers.store.order.vo.StoreOrderDetailVO;

public class StoreOrderSaleAction extends BaseAction {

    private static boolean listInstanceRunning   = false;
    private static boolean detailInstanceRunning = false;
    private static boolean excelInstanceRunning  = false;

    private StoreOrderSaleService storeOrderSaleService;

    public void setStoreOrderSaleService(StoreOrderSaleService storeOrderSaleService) {
        this.storeOrderSaleService = storeOrderSaleService;
    }

    public synchronized ActionForward list(ActionMapping mapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logs = new StringBuilder();

        while (listInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] list Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] list INTERRUPTED!!! error | ", e);
            }
        }

        listInstanceRunning = true;

        SearchDTO param = new SearchDTO();

        try {
            logs.append(NL + "[" + TAG + "] list START");

            fp.formRequestToVo(request, param);

            PageNavigation pager = new PageNavigation("15", param.getRowno());

            SearchVO item = (SearchVO) storeOrderSaleService.listProcess(param);

            pager.setTotalRecords(item.getTotalRecords());

            if(isDevMode()) {
                logs.append(NL).append("================ * SearchVO * START ================");
                logs.append(NL).append(item.getTotalRecords());
                logs.append(NL).append(pager.getTotalRecords());
                logs.append(NL).append("================ * SearchVO * END   ================");
            }

            request.setAttribute(Constants.OBJ_ITEM, item);
            request.setAttribute(Constants.OBJ_ITEMS, item.getItems());
            request.setAttribute(Constants.OBJ_PAGER, pager);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] list error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] list END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        listInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_LIST_KEY);
    }

    public synchronized ActionForward detail(ActionMapping mapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logs = new StringBuilder();

        while (detailInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] detail Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] detail INTERRUPTED!!! error | ", e);
            }
        }

        detailInstanceRunning = true;

        SearchDTO param = new SearchDTO();

        try {
            logs.append(NL + "[" + TAG + "] detail START");

            fp.formRequestToVo(request, param);

            SearchVO item = (SearchVO) storeOrderSaleService.detailProcess(param);

            if(isDevMode()) {
                logs.append(NL).append("================ * SearchVO * START ================");
                logs.append(NL).append(item.getTotalRecords());
                logs.append(NL).append("================ * SearchVO * END   ================");
            }

            request.setAttribute(Constants.OBJ_ITEMS, item.getItems());

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] detail error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] detail END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        detailInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_DETAIL_KEY);
    }

    public synchronized void excelProcess(ActionMapping mapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logs = new StringBuilder();

        try {
            logs.append(NL + "[" + TAG + "] excelProcess START");

            String docuType = request.getParameter("docuType") != null ? request.getParameter("docuType") : "item_";

            SearchDTO param = new SearchDTO();

            fp.formRequestToVo(request, param);

            List<StoreOrderDetailVO> items = (ArrayList<StoreOrderDetailVO>) storeOrderSaleService.excelProcess(param);

            Calendar c1 = null;
            Date date1 = null;
            String unq = "";
            c1 = Calendar.getInstance();
            date1 = (Date) c1.getTime();

            SimpleDateFormat sdf = new SimpleDateFormat();
            sdf.applyPattern("yyyyMMddhhmmss");

            unq = sdf.format(date1);

            String csvfilePath = "";

            String fileTempname = docuType + unq + ".csv";
            // String csvfilePath = "C:\\test" + fileTempname;
//            String csvfilePath = "/data1/amcs/temp/" + fileTempname;
            if("192.168.0.250".equals(getIpAddress())) {
                csvfilePath = "D:\\workspaces\\was\\imweb_ers\\WebContent\\data\\upload\\" + fileTempname;
            } else {
                csvfilePath = "/source/app/web/imweb_ers/WebContent/data/upload/" + fileTempname;
            }

            String[] entrynames = null;

            entrynames = new String[11];
            entrynames[0] = "날짜";
            entrynames[1] = "매장명";
            entrynames[2] = "매장코드";
            entrynames[3] = "원가";
            entrynames[4] = "할인가격";
            entrynames[5] = "수량";

            CSVWriter writer = new CSVWriter(new FileWriterWithEncoding(csvfilePath, "EUC-KR"), ',', '\u0000', '\u0000');

            writer.writeNext(entrynames);

            String[] entries = null;

            SimpleDateFormat original = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat new_formate = new SimpleDateFormat("yyyy-MM-dd");
            for(StoreOrderDetailVO row : items) {

                entries = new String[11];

                Date od = original.parse(row.getCreatedate());

                entries[0] = new_formate.format(od);
                entries[1] = row.getStorename();
                entries[2] = row.getStorecode();
                entries[3] = row.getPrice();
                entries[4] = row.getDcprice();
                entries[5] = row.getQty();

                writer.writeNext(entries);
            }

            writer.close();

            download(request, response, csvfilePath, fileTempname);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] excelProcess error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] excelProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
    }

    public static void download(HttpServletRequest request, HttpServletResponse response, String filePath, String saveFileName) throws Exception {

        String encodedFileName = URLEncoder.encode(saveFileName, "EUC-KR");

        BufferedInputStream in = null;
        BufferedOutputStream out = null;

        try {
            /**
             * 다운로드 처리
             */
            File downFile = new File(filePath);

            int fileSize = (int) downFile.length();

            request.setCharacterEncoding("EUC-KR");

            String strClient = request.getHeader("User-Agent");
            String fileType = null;

            if(strClient.indexOf("MSIE 5.5") != -1) {
                response.setHeader("Content-Disposition", "filename=" + encodedFileName + ";");
                fileType = "doesn/matter";
            } else {
                response.setHeader("Content-Disposition", "attachment;filename=" + encodedFileName + ";");
                fileType = "application/x-msdownload";
            }

            response.setContentType(fileType + ";charset=EUC-KR");
            response.setContentLength(fileSize);

            byte b[] = new byte[1024];

            if(downFile.isFile()) {
                in = new BufferedInputStream(new FileInputStream(downFile));
                out = new BufferedOutputStream(response.getOutputStream());

                int read = -1;
                while ((read = in.read(b)) != -1) {
                    out.write(b, 0, read);
                }
            }

        } catch (FileNotFoundException fnfe) {
            throw fnfe;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if(in != null) {
                in.close();
            }

            if(out != null) {
                out.close();
            }
        }
    }
}