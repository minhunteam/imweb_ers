package com.imweb.ers.store.order.vo;

import com.common.module.base.BaseVO;

public class StoreOrderDetailVO extends BaseVO {

    private String ordercode     = "";
    private String subordercode  = "";
    private String storeitemcode = "";
    private String storeitemname = "";
    private String storecode     = "";
    private String storename     = "";
    private String itemsize      = "";
    private String price         = "";
    private String dcprice       = "";
    private String itemprice     = "";
    private String itemdcprice   = "";
    private String qty           = "";
    private String description   = "";

    public String getOrdercode() {
        return ordercode;
    }

    public void setOrdercode(String ordercode) {
        this.ordercode = ordercode;
    }

    public String getSubordercode() {
        return subordercode;
    }

    public void setSubordercode(String subordercode) {
        this.subordercode = subordercode;
    }

    public String getStoreitemcode() {
        return storeitemcode;
    }

    public void setStoreitemcode(String storeitemcode) {
        this.storeitemcode = storeitemcode;
    }

    public String getStoreitemname() {
        return storeitemname;
    }

    public void setStoreitemname(String storeitemname) {
        this.storeitemname = storeitemname;
    }

    public String getStorecode() {
        return storecode;
    }

    public void setStorecode(String storecode) {
        this.storecode = storecode;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getItemsize() {
        return itemsize;
    }

    public void setItemsize(String itemsize) {
        this.itemsize = itemsize;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDcprice() {
        return dcprice;
    }

    public void setDcprice(String dcprice) {
        this.dcprice = dcprice;
    }

    public String getItemprice() {
        return itemprice;
    }

    public void setItemprice(String itemprice) {
        this.itemprice = itemprice;
    }

    public String getItemdcprice() {
        return itemdcprice;
    }

    public void setItemdcprice(String itemdcprice) {
        this.itemdcprice = itemdcprice;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}