package com.imweb.ers.store.order.dao;

public interface StoreOrderItemDao {

    public Object totalRecords(Object param) throws Exception;

    public Object listProcess(Object param) throws Exception;

    public Object excelProcess(Object param) throws Exception;

}