package com.imweb.ers.store.order.dao;

import org.springframework.util.StopWatch;

import com.common.module.base.BaseDao;

public class StoreOrderDetailDaoImpl extends BaseDao implements StoreOrderDetailDao {

    @Override
    public Object checkProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] checkProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.select("ibatis.mysql.imweb.ers.store.order.detail.checkProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] checkProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] checkProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] checkProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object totalRecords(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] totalRecords START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.select("ibatis.mysql.imweb.ers.store.order.detail.totalRecords", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] totalRecords 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] totalRecords Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] totalRecords END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object listProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] listProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.list("ibatis.mysql.imweb.ers.store.order.detail.listProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] listProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] listProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] listProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object excelProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] excelProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.list("ibatis.mysql.imweb.ers.store.order.detail.excelProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] excelProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] excelProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] excelProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object readProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] readProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.select("ibatis.mysql.imweb.ers.store.order.detail.readProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] readProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] readProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] readProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object insertProcess(Object param) throws Exception {

        int i = 0;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] insertProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            i = dao.add(param, "ibatis.mysql.imweb.ers.store.order.detail.insertProcess");

            if(i == 0) {
                returnCode = "0001";
            } else if(i == -1) {
                returnCode = "9999";
            } else {
                returnCode = "0000";
            }

            watch.stop();

            logs.append(NL + "[" + TAG + "] insertProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] insertProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] insertProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return returnCode;
    }

    @Override
    public Object updateProcess(Object param) throws Exception {

        int i = 0;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] updateProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            i = dao.mod("ibatis.mysql.imweb.ers.store.order.detail.updateProcess", param);

            if(i == 0) {
                returnCode = "0001";
            } else if(i == -1) {
                returnCode = "9999";
            } else {
                returnCode = "0000";
            }

            watch.stop();

            logs.append(NL + "[" + TAG + "] updateProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] updateProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] updateProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return returnCode;
    }

    @Override
    public Object saveProcess(Object param) throws Exception {

        int i = 0;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] saveProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            i = dao.save("ibatis.mysql.imweb.ers.store.order.detail.updateProcess", "ibatis.mysql.imweb.ers.store.order.detail.insertProcess", param);

            if(i == 0) {
                returnCode = "0001";
            } else if(i == -1) {
                returnCode = "9999";
            } else {
                returnCode = "0000";
            }

            watch.stop();

            logs.append(NL + "[" + TAG + "] saveProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] saveProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] saveProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return returnCode;
    }
}