package com.imweb.ers.store.order.dao;

import org.springframework.util.StopWatch;

import com.common.module.base.BaseDao;

public class StoreOrderSaleDaoImpl extends BaseDao implements StoreOrderSaleDao {

    @Override
    public Object totalRecords(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] totalRecords START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.select("ibatis.mysql.imweb.ers.store.order.sale.totalRecords", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] totalRecords 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] totalRecords Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] totalRecords END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object listProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] listProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.list("ibatis.mysql.imweb.ers.store.order.sale.listProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] listProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] listProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] listProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object detailProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] detailProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.list("ibatis.mysql.imweb.ers.store.order.sale.detailProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] detailProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] detailProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] detailProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object excelProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] excelProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.list("ibatis.mysql.imweb.ers.store.order.sale.excelProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] excelProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] excelProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] excelProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }
}