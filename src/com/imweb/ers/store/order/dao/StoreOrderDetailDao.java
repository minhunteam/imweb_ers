package com.imweb.ers.store.order.dao;

public interface StoreOrderDetailDao {

    public Object checkProcess(Object param) throws Exception;

    public Object totalRecords(Object param) throws Exception;

    public Object listProcess(Object param) throws Exception;

    public Object readProcess(Object param) throws Exception;

    public Object excelProcess(Object param) throws Exception;

    public Object insertProcess(Object param) throws Exception;

    public Object updateProcess(Object param) throws Exception;

    public Object saveProcess(Object param) throws Exception;

}