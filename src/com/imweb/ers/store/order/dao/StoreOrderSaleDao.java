package com.imweb.ers.store.order.dao;

public interface StoreOrderSaleDao {

    public Object totalRecords(Object param) throws Exception;

    public Object listProcess(Object param) throws Exception;

    public Object detailProcess(Object param) throws Exception;

    public Object excelProcess(Object param) throws Exception;

}