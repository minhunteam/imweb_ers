package com.imweb.ers.store.order.service;

public interface StoreOrderItemService {

    public Object listProcess(Object param) throws Exception;

    public Object excelProcess(Object param) throws Exception;

}