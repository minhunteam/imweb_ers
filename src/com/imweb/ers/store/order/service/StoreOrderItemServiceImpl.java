package com.imweb.ers.store.order.service;

import java.util.ArrayList;
import java.util.List;

import com.common.module.base.BaseService;
import com.imweb.ers.common.dto.SearchDTO;
import com.imweb.ers.common.vo.SearchVO;
import com.imweb.ers.store.order.dao.StoreOrderItemDao;

public class StoreOrderItemServiceImpl extends BaseService implements StoreOrderItemService {

    private StoreOrderItemDao storeOrderItemDao;

    public void setStoreOrderItemDao(StoreOrderItemDao storeOrderItemDao) {
        this.storeOrderItemDao = storeOrderItemDao;
    }

    @Override
    public Object listProcess(Object param) throws Exception {

        SearchVO result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] listProcess START");

            SearchDTO search = new SearchDTO();

            SearchDTO dto = new SearchDTO();

            dto = (SearchDTO) param;

            logs.append(NL + "[" + TAG + "] SearchDTO dto [" + dto + "]");

            search.setStorecode(dto.getStorecode());
            search.setLimit((Integer.parseInt(dto.getRowno()) - 1) * Integer.parseInt(dto.getRownum()));
            search.setRowno(dto.getRowno());
            search.setRownum(dto.getRownum());

            logs.append(NL + "[" + TAG + "] SearchDTO search [" + search + "]");

            String totalRecords = "";

            List items = new ArrayList();

            totalRecords = (String) storeOrderItemDao.totalRecords(search);

            items = (ArrayList) storeOrderItemDao.listProcess(search);

            result = new SearchVO();

            result.setTotalRecords(totalRecords);
            result.setItems(items);
            result.setRowno(dto.getRowno());
            result.setRownum(dto.getRownum());

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] listProcess Error | ", e);
            result = new SearchVO();
        } finally {
            logs.append(NL + "[" + TAG + "] listProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object excelProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] excelProcess START");

            SearchDTO search = new SearchDTO();

            SearchDTO dto = new SearchDTO();

            dto = (SearchDTO) param;

            logs.append(NL + "[" + TAG + "] SearchDTO dto [" + dto + "]");

            search.setStorecode(dto.getStorecode());

            logs.append(NL + "[" + TAG + "] SearchDTO search [" + search + "]");

            result = (ArrayList) storeOrderItemDao.excelProcess(search);

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] excelProcess Error | ", e);
            result = new SearchVO();
        } finally {
            logs.append(NL + "[" + TAG + "] excelProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }
}