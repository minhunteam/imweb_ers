package com.imweb.ers.store.order.service;

public interface StoreOrderSaleService {

    public Object listProcess(Object param) throws Exception;
    public Object detailProcess(Object param) throws Exception;

    public Object excelProcess(Object param) throws Exception;

}