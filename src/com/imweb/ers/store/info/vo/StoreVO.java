package com.imweb.ers.store.info.vo;

import com.common.module.base.BaseVO;

public class StoreVO extends BaseVO {

    private String storecode = "";
    private String status    = "";
    private String storename = "";
    private String storeid   = "";
    private String storepw   = "";

    public String getStorecode() {
        return storecode;
    }

    public void setStorecode(String storecode) {
        this.storecode = storecode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getStoreid() {
        return storeid;
    }

    public void setStoreid(String storeid) {
        this.storeid = storeid;
    }

    public String getStorepw() {
        return storepw;
    }

    public void setStorepw(String storepw) {
        this.storepw = storepw;
    }

}