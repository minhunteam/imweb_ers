package com.imweb.ers.store.info.dao;

public interface StoreInfoDao {

    public Object checkProcess(Object param) throws Exception;

    public Object totalRecords(Object param) throws Exception;

    public Object listProcess(Object param) throws Exception;

    public Object readProcess(Object param) throws Exception;

    public Object saveProcess(Object param) throws Exception;

}