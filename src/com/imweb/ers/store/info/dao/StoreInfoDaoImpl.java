package com.imweb.ers.store.info.dao;

import org.springframework.util.StopWatch;

import com.common.module.base.BaseDao;

public class StoreInfoDaoImpl extends BaseDao implements StoreInfoDao {

    @Override
    public Object checkProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] checkProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.select("ibatis.mysql.imweb.ers.store.info.checkProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] checkProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] checkProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] checkProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object totalRecords(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] totalRecords START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.select("ibatis.mysql.imweb.ers.store.info.totalRecords", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] totalRecords 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] totalRecords Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] totalRecords END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object listProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] listProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.list("ibatis.mysql.imweb.ers.store.info.listProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] listProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] listProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] listProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object readProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] readProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.select("ibatis.mysql.imweb.ers.store.info.readProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] readProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] readProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] readProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object saveProcess(Object param) throws Exception {

        int i = 0;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] saveProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            i = dao.save("ibatis.mysql.imweb.ers.store.info.updateProcess", "ibatis.mysql.imweb.ers.store.info.insertProcess", param);

            if(i == 0) {
                returnCode = "0001";
            } else if(i == -1) {
                returnCode = "9999";
            } else {
                returnCode = "0000";
            }

            watch.stop();

            logs.append(NL + "[" + TAG + "] saveProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] saveProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] saveProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return returnCode;
    }
}