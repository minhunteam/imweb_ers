package com.imweb.ers.store.info.service;

import javax.servlet.http.HttpServletRequest;

public interface StoreInfoService {

    public Object listProcess(Object param) throws Exception;

    public Object readProcess(Object param) throws Exception;

    public Object saveProcess(Object param) throws Exception;

    public Object batchProcess(HttpServletRequest request) throws Exception;

    public Object excelProcess(Object param) throws Exception;

}