package com.imweb.ers.store.info.service;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.common.module.base.BaseService;
import com.common.module.create.vo.CreateTranKeyVO;
import com.common.module.transaction.TransactionHelper;
import com.imweb.ers.base.root.dto.RootDTO;
import com.imweb.ers.base.root.vo.RootVO;
import com.imweb.ers.common.dto.SearchDTO;
import com.imweb.ers.common.parser.ResultExcelParserVO;
import com.imweb.ers.common.vo.SearchVO;
import com.imweb.ers.store.info.dao.StoreInfoDao;
import com.imweb.ers.store.info.vo.StoreVO;

public class StoreInfoServiceImpl extends BaseService implements StoreInfoService {

    private TransactionHelper transactionHelper;

    private StoreInfoDao storeInfoDao;

    public void setTransactionHelper(TransactionHelper transactionHelper) {
        this.transactionHelper = transactionHelper;
    }

    public void setStoreInfoDao(StoreInfoDao storeInfoDao) {
        this.storeInfoDao = storeInfoDao;
    }

    @Override
    public Object listProcess(Object param) throws Exception {

        SearchVO result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] listProcess START");

            SearchDTO search = new SearchDTO();

            SearchDTO dto = new SearchDTO();

            dto = (SearchDTO) param;

            logs.append(NL + "[" + TAG + "] SearchDTO dto [" + dto + "]");

            search.setLimit((Integer.parseInt(dto.getRowno()) - 1) * Integer.parseInt(dto.getRownum()));
            search.setRowno(dto.getRowno());
            search.setRownum(dto.getRownum());

            logs.append(NL + "[" + TAG + "] SearchDTO search [" + search + "]");

            String totalRecords = "";

            List items = new ArrayList();

            totalRecords = (String) storeInfoDao.totalRecords(search);

            items = (ArrayList) storeInfoDao.listProcess(search);

            result = new SearchVO();

            result.setTotalRecords(totalRecords);
            result.setItems(items);
            result.setRowno(dto.getRowno());
            result.setRownum(dto.getRownum());

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] listProcess Error | ", e);
            result = new SearchVO();
        } finally {
            logs.append(NL + "[" + TAG + "] listProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object readProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] readProcess START");

            SearchDTO search = new SearchDTO();

            SearchDTO dto = new SearchDTO();

            dto = (SearchDTO) param;

            search.setRootcode(dto.getRootcode());

//            result = rootDao.readProcess(search);

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] readProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] readProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object saveProcess(Object param) throws Exception {

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] saveProcess START");

            processCode = "";

            RootDTO val = null;

            RootDTO dto = new RootDTO();

            RootVO check = new RootVO();

            dto = (RootDTO) param;

            if("".equals(dto.getRootcode())) {

                RootVO search = new RootVO();

                search.setRootid(dto.getRootid());

//                check = (RootVO) rootDao.checkProcess(search);

                if(check == null) {

                    CreateTranKeyVO ctk = new CreateTranKeyVO();

                    ctk.setSeqType("ROOT");
                    ctk.setSeqLen(7);

//                    String rootcode = (String) createTranKeyHelper.createProcess(ctk);

//                    logs.append(NL + "[" + TAG + "] rootcode [" + rootcode + "]");

                    val = new RootDTO();

//                    val.setRootcode(rootcode);
                    val.setRootid(dto.getRootid());
                    val.setRootpw(ase.encrypt(dto.getRootpw()));
                    val.setRootname(dto.getRootname());
                    val.setAuthcode(dto.getAuthcode());

                    processCode = "2001";

                } else {
                    returnCode = "4001";
                }

            } else {

                RootVO search = new RootVO();

                search.setRootcode(dto.getRootcode());

//                check = (RootVO) rootDao.checkProcess(search);

                if(check == null) {
                    returnCode = "5001";
                } else {

                    val = new RootDTO();

                    val.setRootpw(dto.getRootcode());

                    if(!"".equals(dto.getRootpw())) {
                        val.setRootpw(ase.encrypt(dto.getRootpw()));
                    }

                    if(!"".equals(dto.getAuthcode())) {
                        val.setAuthcode(dto.getAuthcode());
                    }

                    processCode = "2002";
                }
            }

            if("2001".equals(processCode) || "2002".equals(processCode)) {

                transactionHelper.start();

//                returnCode = (String) rootDao.saveProcess(val);

                if("0000".equals(returnCode)) {
                    transactionHelper.commit();
                }

                transactionHelper.end();

            }

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] saveProcess Error | ", e);
            returnCode = "9999";
            if("2001".equals(processCode) || "2002".equals(processCode)) {
                transactionHelper.end();
            }
        } finally {
            logs.append(NL + "[" + TAG + "] saveProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return returnCode;
    }

    private static final int BYTE     = 1024;
    private static final int KILOBYTE = 1024;

    private static final int MEMORY_THRESHOLD = BYTE * KILOBYTE * 5;
    private static final int MAX_FILE_SIZE    = BYTE * KILOBYTE * 40;
    private static final int MAX_REQUEST_SIZE = BYTE * KILOBYTE * 50;

    private static final String FILE_STROAGE = "excel";

    @Override
    public Object batchProcess(HttpServletRequest request) throws Exception {
        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] batchProcess START");

            DiskFileItemFactory disk = new DiskFileItemFactory();
            disk.setSizeThreshold(MEMORY_THRESHOLD);

            disk.setRepository(new File("D:\\workspaces\\was\\imweb_ers\\WebContent\\data\\upload"));

            ServletFileUpload upload = new ServletFileUpload(disk);
            upload.setFileSizeMax(MAX_FILE_SIZE);
            upload.setSizeMax(MAX_REQUEST_SIZE);

            // 서버에 파일 저장 경로

            String uploadPath = request.getServletContext().getRealPath("") + File.separator + FILE_STROAGE;

            File uploadStorage = new File(uploadPath);
            if(!uploadStorage.isDirectory()) {
                uploadStorage.mkdir();
            }

            List<FileItem> formItems = upload.parseRequest(request);

            String fileName = "";

            int sidx = 0;

            String ext = "";

            if(formItems != null && formItems.size() > 0) {

                for(FileItem item : formItems) {
                    // form의 file type만 가져온다.
                    if(!item.isFormField()) {
                        logs.append(NL + "[" + TAG + "] FileItem [" + item + "]");
                        String origFileName = new File(item.getName()).getName();
                        String saveFileName = getFileName(origFileName);
                        String filePath = uploadPath + File.separator + saveFileName;
                        File storeFile = new File(filePath);
                        // file 저장
                        item.write(storeFile);
                        request.setAttribute("result", "Upload has been done successfully!");

                        fileName = storeFile.getAbsolutePath();

                        sidx = saveFileName.lastIndexOf(".");

                        ext = saveFileName.substring(sidx + 1);

                        logs.append(NL + "[" + TAG + "] ext [" + ext + "]");
                        logs.append(NL + "[" + TAG + "] saveFileName [" + saveFileName + "]");
                        logs.append(NL + "[" + TAG + "] getAbsolutePath [" + storeFile.getAbsolutePath() + "]");
                    }
                }
            }

            ResultExcelParserVO item = null;

            if("xls".equals(ext)) {
                item = (ResultExcelParserVO) setXls(fileName);
                if(item != null) {
                    processCode = "2001";
                }
            } else if("xlsx".equals(ext)) {
                item = (ResultExcelParserVO) setXlsx(fileName);
                if(item != null) {
                    processCode = "2002";
                }
            }

            if("2001".equals(processCode) || "2002".equals(processCode)) {

                List<StoreVO> items = item.getItems();

                int cnt = 0;

                transactionHelper.start();

                for(StoreVO row : items) {

                    logs.append(NL + "[" + TAG + "] row [" + row + "]");

                    storeInfoDao.saveProcess(row);

                    cnt++;

                    if(item.getItems().size() == cnt) {
                        returnCode = "0000";
                        transactionHelper.commit();
                    } else {
                        returnCode = "9999";
                    }
                }

                transactionHelper.end();

                logs.append(NL + "[" + TAG + "] cnt [" + cnt + "]");
            }

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] batchProcess Error | ", e);
            returnCode = "9999";
            if("2001".equals(processCode) || "2002".equals(processCode)) {
                transactionHelper.end();
            }
        } finally {
            logs.append(NL + "[" + TAG + "] batchProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return returnCode;
    }

    private static String getFileName(String fileName) {
        String uniqueFileName = "";
        String uuidName = StringUtils.remove(UUID.randomUUID().toString(), '-').toUpperCase();
        int fileIndex = StringUtils.lastIndexOf(fileName, '.');
        if(fileIndex != -1) {
            String extension = StringUtils.substring(fileName, fileIndex + 1).toLowerCase();
            uniqueFileName = uuidName + "." + extension;
        } else {
            uniqueFileName = uuidName;
        }
        return uniqueFileName;
    }

    @Override
    public Object excelProcess(Object param) throws Exception {
        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] excelProcess START");

            SearchDTO search = new SearchDTO();

            SearchDTO dto = new SearchDTO();

            dto = (SearchDTO) param;

            search.setRootcode(dto.getRootcode());

//            result = rootDao.readProcess(search);

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] excelProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] excelProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    private Object setXls(String file) throws Exception {

        StringBuilder rows = new StringBuilder();

        ResultExcelParserVO result = null;

        try {
            rows = new StringBuilder();
            rows.append(NL + "[" + TAG + "] setXls START");

            HSSFWorkbook wb = null;
            HSSFSheet st = null;
            HSSFRow row = null;
            HSSFCell cell = null;
            int sRowIndex = 0;
            int eRowIndex = 0;
            int sColIndex = 0;
            int eColIndex = 0;
            File f = new File(file);
            FileInputStream fis = new FileInputStream(f);
            wb = new HSSFWorkbook(fis);

            if(wb != null) {
                st = wb.getSheetAt(0);
                if(st != null) {
                    sRowIndex = st.getFirstRowNum() + 1;
                    eRowIndex = st.getLastRowNum();
                    logs.append(NL + "[" + TAG + "] sRowIndex [" + sRowIndex + "]");
                    logs.append(NL + "[" + TAG + "] eRowIndex [" + eRowIndex + "]");
                    if(st.getRow(sRowIndex) != null) {
                        eColIndex = st.getRow(sRowIndex).getLastCellNum();
                        logs.append(NL + "[" + TAG + "] eColIndex [" + eColIndex + "]");
                        if(eColIndex == 2) {

                            result = new ResultExcelParserVO();

                            List success = new ArrayList();
                            List error = new ArrayList();

                            StoreVO parser = null;

                            for(int i = sRowIndex; i <= eRowIndex; i++) {

                                row = st.getRow(i);

                                parser = new StoreVO();

                                for(int j = sColIndex; j < eColIndex; j++) {
                                    cell = row.getCell(j);
                                    if(cell != null) {
                                        String sValue = "";
                                        if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                                            sValue = cell.getStringCellValue().trim();
                                            if(sValue.isEmpty() == false) {
                                                if(j == 0) {
//                                                    logs.append(NL + "[" + j + "] sValue [" + sValue + "]");
                                                    parser.setStorename(sValue);
                                                }
                                                if(j == 1) {
//                                                  logs.append(NL + "[" + j + "] sValue [" + sValue + "]");
//                                                  logs.append(NL + "[" + j + "] sValue [" + ase.encrypt(sValue) + "]");
                                                  parser.setStorecode(sValue);
                                                  parser.setStoreid(sValue);

                                                  String pass = sValue.toLowerCase() + "time";

//                                                  logs.append(NL + "[" + j + "] pass [" + pass + "]");
//                                                  logs.append(NL + "[" + j + "] pass [" + ase.encrypt(pass) + "]");

                                                  parser.setStorepw(ase.encrypt(pass));
                                              }
                                            }
                                        }
                                    }
                                }
                                success.add(parser);
                            }

                            result.setSuccessCnt(success.size());
                            result.setErrorCnt(error.size());
                            result.setItems(success);
                        }
                    }
                }
            }

//            logs.append(NL + "[" + TAG + "] ResultExcelParserVO [" + result + "]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] setXls Error | ", e);
            returnCode = "9999";
        } finally {
            rows.append(NL + "[" + TAG + "] setXls END");
            if(isDevMode()) {
                log.info(rows);
                rows.setLength(0);
            } else {
                rows.setLength(0);
            }
        }

        return result;
    }

    private Object setXlsx(String file) throws Exception {

        StringBuilder rows = new StringBuilder();

        ResultExcelParserVO result = null;

        try {
            rows = new StringBuilder();
            rows.append(NL + "[" + TAG + "] setXlsx START");

            XSSFWorkbook wb = null;
            XSSFSheet st = null;
            XSSFRow row = null;
            XSSFCell cell = null;
            int sRowIndex = 0;
            int eRowIndex = 0;
            int sColIndex = 0;
            int eColIndex = 0;
            File f = new File(file);
            FileInputStream fis = new FileInputStream(f);
            wb = new XSSFWorkbook(fis);

            if(wb != null) {
                st = wb.getSheetAt(0);
                if(st != null) {
                    sRowIndex = st.getFirstRowNum() + 1;
                    eRowIndex = st.getLastRowNum();
                    logs.append(NL + "[" + TAG + "] sRowIndex [" + sRowIndex + "]");
                    logs.append(NL + "[" + TAG + "] eRowIndex [" + eRowIndex + "]");
                    if(st.getRow(sRowIndex) != null) {
                        eColIndex = st.getRow(sRowIndex).getLastCellNum();
                        logs.append(NL + "[" + TAG + "] eColIndex [" + eColIndex + "]");
                        if(eColIndex == 2) {

                            result = new ResultExcelParserVO();

                            List success = new ArrayList();
                            List error = new ArrayList();

                            StoreVO parser = null;

                            for(int i = sRowIndex; i <= eRowIndex; i++) {

                                row = st.getRow(i);

                                parser = new StoreVO();

                                for(int j = sColIndex; j < eColIndex; j++) {
                                    cell = row.getCell(j);
                                    if(cell != null) {
                                        String sValue = "";
                                        if(cell.getCellType() == XSSFCell.CELL_TYPE_STRING) {
                                            sValue = cell.getStringCellValue().trim();
                                            if(sValue.isEmpty() == false) {
                                                if(j == 0) {
                                                    logs.append(NL + "[" + j + "] sValue [" + sValue + "]");
                                                    parser.setStorename(sValue);
                                                }
                                                if(j == 1) {
                                                    logs.append(NL + "[" + j + "] sValue [" + sValue + "]");
                                                    logs.append(NL + "[" + j + "] sValue [" + ase.encrypt(sValue) + "]");
                                                    parser.setStorecode(sValue);
                                                    parser.setStoreid(sValue);

                                                    String pass = sValue.toLowerCase() + "time";

                                                    logs.append(NL + "[" + j + "] pass [" + pass + "]");
                                                    logs.append(NL + "[" + j + "] pass [" + ase.encrypt(pass) + "]");

                                                    parser.setStorepw(ase.encrypt(pass));
                                                }
                                            }
                                        }
                                    }
                                }
                                success.add(parser);
                            }

                            result.setSuccessCnt(success.size());
                            result.setErrorCnt(error.size());
                            result.setItems(success);
                        }
                    }
                }
            }

            logs.append(NL + "[" + TAG + "] ResultExcelParserVO [" + result + "]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] setXlsx Error | ", e);
            returnCode = "9999";
        } finally {
            rows.append(NL + "[" + TAG + "] setXlsx END");
            if(isDevMode()) {
                log.info(rows);
                rows.setLength(0);
            } else {
                rows.setLength(0);
            }
        }

        return result;
    }
}