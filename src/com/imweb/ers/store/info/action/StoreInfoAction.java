package com.imweb.ers.store.info.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.common.module.base.BaseAction;
import com.common.module.conf.Constants;
import com.common.module.custom.pager.PageNavigation;
import com.common.module.json.data.CommonJsonData;
import com.common.module.session.vo.UserSessionVO;
import com.imweb.ers.base.root.dto.RootDTO;
import com.imweb.ers.base.root.vo.RootVO;
import com.imweb.ers.common.dto.SearchDTO;
import com.imweb.ers.common.vo.SearchVO;
import com.imweb.ers.store.info.service.StoreInfoService;

public class StoreInfoAction extends BaseAction {

    private static boolean listInstanceRunning     = false;
    private static boolean readInstanceRunning     = false;
    private static boolean saveFormInstanceRunning = false;
    private static boolean saveDoneInstanceRunning = false;
    private static boolean batchInstanceRunning    = false;
    private static boolean excelInstanceRunning    = false;

    private StoreInfoService storeInfoService;

    public void setStoreInfoService(StoreInfoService storeInfoService) {
        this.storeInfoService = storeInfoService;
    }

    public synchronized ActionForward list(ActionMapping mapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logs = new StringBuilder();

        while (listInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] list Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] list INTERRUPTED!!! error | ", e);
            }
        }

        listInstanceRunning = true;

        SearchDTO param = new SearchDTO();

        try {
            logs.append(NL + "[" + TAG + "] list START");

            fp.formRequestToVo(request, param);

            PageNavigation pager = new PageNavigation("10", param.getRowno());

            SearchVO item = (SearchVO) storeInfoService.listProcess(param);

            pager.setTotalRecords(item.getTotalRecords());

            if(isDevMode()) {
                logs.append(NL).append("================ * SearchVO * START ================");
                logs.append(NL).append(item.getTotalRecords());
                logs.append(NL).append(pager.getTotalRecords());
                logs.append(NL).append("================ * SearchVO * END   ================");
            }

            request.setAttribute(Constants.OBJ_ITEM, item);
            request.setAttribute(Constants.OBJ_ITEMS, item.getItems());
            request.setAttribute(Constants.OBJ_PAGER, pager);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] list error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] list END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        listInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_LIST_KEY);
    }

    public synchronized ActionForward read(ActionMapping mapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logs = new StringBuilder();

        while (readInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] view Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] view INTERRUPTED!!! error | ", e);
            }
        }

        readInstanceRunning = true;

        SearchVO param = new SearchVO();

        try {
            logs.append(NL + "[" + TAG + "] view START");

            fp.formRequestToVo(request, param);

//            RootVO item = (RootVO) rootService.readProcess(param);

            if(isDevMode()) {
                logs.append(NL).append("================ * returnCode * START ================");

                logs.append(NL).append("================ * returnCode * END   ================");
            }

//            req.setAttribute(Constants.OBJ_ITEM, item);

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] view error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] view END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        readInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_READ_KEY);
    }

    public synchronized ActionForward saveForm(ActionMapping mapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logs = new StringBuilder();

        while (saveFormInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] saveForm Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] saveForm INTERRUPTED!!! error | ", e);
            }
        }

        saveFormInstanceRunning = true;

        try {
            logs.append(NL + "[" + TAG + "] saveForm START");

            RootVO vo = new RootVO();

            request.setAttribute("view", vo);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] saveForm error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] saveForm END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        saveFormInstanceRunning = false;

        notifyAll();

        return mapping.findForward(Constants.ACTION_FORWARD_SAVEFORM_KEY);
    }

    public synchronized void saveDone(ActionMapping mapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logs = new StringBuilder();

        while (saveDoneInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] saveDone Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] saveDone INTERRUPTED!!! error | ", e);
            }
        }

        saveDoneInstanceRunning = true;

        RootDTO param = new RootDTO();

        try {
            logs.append(NL + "[" + TAG + "] saveDone START");

            fp.formRequestToVo(request, param);

//            returnCode = (String) rootService.saveProcess(param);

            if(isDevMode()) {
                logs.append(NL).append("================ * returnCode * START ================");

                logs.append(NL).append("================ * returnCode * END   ================");
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] saveDone error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] saveDone END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        saveDoneInstanceRunning = false;

        notifyAll();

        response.setContentType("application/json; charset=UTF-8");
//        res.getWriter().append(gson.toJson(data));
    }

    public synchronized ActionForward batchForm(ActionMapping mapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) throws Exception {

        logs = new StringBuilder();

        try {
            logs.append(NL + "[" + TAG + "] batchForm START");

            RootVO vo = new RootVO();

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] batchForm error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] batchForm END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return mapping.findForward(Constants.ACTION_FORWARD_BATCHFORM_KEY);
    }

    public synchronized void batchProcess(ActionMapping mapping, ActionForm actionform, HttpServletRequest req, HttpServletResponse res) throws Exception {

        logs = new StringBuilder();

        try {
            logs.append(NL + "[" + TAG + "] batchProcess START");

            storeInfoService.batchProcess(req);

            CommonJsonData data = new CommonJsonData();

            data.setSuccess("0000");

            logs.append(NL + "[" + TAG + "] restful [" + NL + gson.toJson(data) + NL + "]");

            res.setContentType("application/json; charset=UTF-8");
            res.getWriter().append(gson.toJson(data));

        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] batchProcess error | ", e);
            throw e;
        } finally {
            logs.append(NL + "[" + TAG + "] batchProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

    }
}