package com.common.module.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangeUtil {

    protected final static String NL = System.getProperty("line.separator", "\n");

    public static void main(String[] args) throws Exception {
        StringBuilder logs = new StringBuilder();
        ChangeUtil cu = new ChangeUtil();
//        logs.append("랜덤 문자열 생성[").append(cu.createRandomString(10)).append("]");
        String str = "arkia@naver.co.kr";
        boolean isPatterned;
        StringBuilder sb = new StringBuilder();
//        sb.append("^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$");
        sb.append("(^[_a-z0-9-])([a-z0-9-]+)(@)([a-z0-9])([a-z0-9-]+)(.[a-z]+.[a-z]+)");
        Pattern p = Pattern.compile(sb.toString());
        Matcher m = p.matcher(str);
        isPatterned = m.find();
        logs.append("isPatterned[").append(isPatterned).append("]").append(NL);
        logs.append("m.groupCount[").append(m.groupCount()).append("]").append(NL);

        String var = "";
        if (isPatterned) {
            for (int i = 0; i <= m.groupCount(); i++) {
                String domain = m.group(i);
                if (i == 2 || i == 5) {
                    var += domain.replaceAll("[^\\s]", "*");
                } else {
                    if (i != 0) {
                        var += domain;
                    }
                }
                logs.append("group[").append(i).append("]").append(domain).append(NL);
            }
        }
        logs.append("str[").append(var).append("]");
        System.out.println(logs.toString());
        System.out.println("Email[" + cu.changeEmailStar(str) + "]");
    }

    public ChangeUtil() {
    }

    public String changeEmailStar(String str) {
        boolean isPatterned;
        StringBuilder sb = new StringBuilder();
        Pattern p = Pattern.compile("(^[_a-z0-9-])([a-z0-9-]+)(@)([a-z0-9])([a-z0-9-]+)(.[a-z]+.[a-z]+)");
        try {
            Matcher m = p.matcher(str);
            isPatterned = m.find();
            if (isPatterned) {
                for (int i = 0; i <= m.groupCount(); i++) {
                    String var = m.group(i);
                    if (i == 2 || i == 5) {
                        sb.append(var.replaceAll("[^\\s]", "*"));
                    } else {
                        if (i != 0) {
                            sb.append(var);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return sb.toString();
    }
}