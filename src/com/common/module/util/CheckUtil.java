package com.common.module.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckUtil {

    public CheckUtil() {
    }

    public static void main(String[] args) {
        CheckUtil cu = new CheckUtil();
        String name = "Kim Dae Y-ong";
        String id = "Kim_Dae.Y-on g".toLowerCase().trim().replaceAll(" ", "");
        boolean isCheckName = cu.checkUserName(name);
        System.out.println("name [" + name + "] isCheckName [" + isCheckName + "]");

        boolean isCheckID = cu.checkUserID(id);
        System.out.println("id [" + id + "] isCheckID [" + isCheckID + "]");
    }

    public boolean checkUserID(String text) {
        boolean isCheckID = false;

        Pattern p = null;
        Matcher m = null;

        StringBuilder sb = null;

        sb = new StringBuilder();
        sb.append("^[a-z0-9_.]{5,20}$");

        p = Pattern.compile(sb.toString());
        m = p.matcher(text);

        while (m.find()) {
            isCheckID = true;
        }

        return isCheckID;
    }

    public boolean checkPassword(String str) {
        boolean flag = false;
        try {
            flag = Pattern.matches("^((?=.*\\w)(?=.*\\d)).{10,16}$", str);
        } catch (Exception e) {

        } finally {

        }
        return flag;
    }

    public boolean checkUserName(String text) {
        boolean isCheckName = false;

        Pattern p = null;
        Matcher m = null;

        StringBuilder sb = null;

        sb = new StringBuilder();
        sb.append("^[ㄱ-ㅇ가-힣]{2,6}$");

        p = Pattern.compile(sb.toString());
        m = p.matcher(text);

        /**
         * 한글 이름 체크
         */

        while (m.find()) {
            String str = m.group().trim();
            isCheckName = true;
        }

        if(isCheckName) {
            return isCheckName;
        } else {
            sb = new StringBuilder();
            sb.append("^[a-zA-Z-]+$|^[a-zA-Z-]+[\\s]{1}[a-zA-Z-]+$|^[a-zA-Z-]+[\\s]{1}[a-zA-Z-]+[\\s]{1}[a-zA-Z-]+$");
            p = Pattern.compile(sb.toString());
            m = p.matcher(text);

            /**
             * 영문 이름 체크
             */

            while (m.find()) {
                String str = m.group().trim();
                isCheckName = true;
            }
        }

        return isCheckName;
    }

    public boolean checkBirthday(String str) {
        boolean flag = false;
        try {
            flag = Pattern.matches("^([1-9])(?:\\d{3})(0(?:[1-9])|1(?:[0-2]))(0(?:[0-9])|1(?:[0-9])|2(?:[0-9])|3(?:[0-1]))$", str);
        } catch (Exception e) {

        } finally {

        }
        return flag;
    }

    public boolean checkEmail(String str) {
        boolean flag = false;
        try {
            flag = Pattern.matches("^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$", str);
        } catch (Exception e) {

        } finally {

        }
        return flag;
    }

    public boolean checkPhoneNumber(String str) {
        boolean flag = false;
        try {
            flag = Pattern.matches("^0(?:2|31|32|33|41|42|43|44|51|52|53|54|55|61|62|63|64)(?:\\d{3}|\\d{4})\\d{4}$", str);
        } catch (Exception e) {

        } finally {

        }
        return flag;
    }

    public boolean checkMobileNumber(String str) {
        boolean flag = false;
        try {
            flag = Pattern.matches("^01(?:0|1|[6-9])(?:\\d{3}|\\d{4})\\d{4}$", str);
        } catch (Exception e) {

        } finally {

        }
        return flag;
    }

    public boolean checkVersion(String str) {
        boolean flag = false;
        try {
            flag = Pattern.matches("^(?:[1-9]|[1-9]{1}[0-9]{0,3})([\\.])(?:[0-9]|[1-9]{1}[0-9]{0,3})([\\.])(?:[0-9]|[1-9]{1}[0-9]{0,3})([\\.])(?:[0-9]{2}(0(?:[1-9])|1(?:[0-2]))(0(?:[1-9])|1(?:[0-9])|2(?:[0-9])|3(?:[0-1])))$", str);
        } catch (Exception e) {

        } finally {

        }
        return flag;
    }
}