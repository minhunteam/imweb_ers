package com.common.module.util;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Properties;

public class PropertiesUtil {

    private static Properties props;

    protected Locale defaultLocale = Locale.getDefault();

    /**
     * 생성자
     * 
     * @param propertyFile 설정정보 파일
     */
    public PropertiesUtil(String propertyFile) {
        InputStream is = getClass().getResourceAsStream(propertyFile);
        props = new Properties();
        try {
            props.load(is);
        } catch (Exception e) {
            System.err.println("Can't read the properties file. Make sure " + propertyFile + " is in the CLASSPATH");
        }
    }

    public String get(String key) throws UnsupportedEncodingException {
        String str = props.getProperty(key);
        return str != null && !str.equals("") ? new String(str.getBytes("8859_1"), "UTF-8") : str;
    }
}