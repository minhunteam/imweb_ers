package com.common.module.util;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

import com.common.module.base.BaseUtil;
import com.common.module.conf.Constants;

public class ConfigUtil extends BaseUtil {

    private static Properties props;
    protected Locale          defaultLocale = Locale.getDefault();
    protected HashMap         formats       = new HashMap();

    /**
     * 생성자 - 기본 설정파일은 Constants에서 설정
     */
    public ConfigUtil() {
        InputStream is = getClass().getResourceAsStream(Constants.CONFIG_PROPERTIES_FILE_NAME);
        props = new Properties();
        try {
            props.load(is);
        } catch (Exception e) {
            System.err.println("Can't read the properties file. Make sure " + Constants.CONFIG_PROPERTIES_FILE_NAME + " is in the CLASSPATH");
        }
    }

    /**
     * 생성자
     * 
     * @param propertyFile 설정정보 파일
     */
    public ConfigUtil(String propertyFile) {
        InputStream is = getClass().getResourceAsStream(propertyFile);
        props = new Properties();
        try {
            props.load(is);
        } catch (Exception e) {
            System.err.println("Can't read the properties file. Make sure " + propertyFile + " is in the CLASSPATH");
        }
    }

    public static void initialize(Object obj) throws Exception {
        if(obj == null) {
            throw new Exception("null parameter");
        }
        if(obj instanceof Properties) {
            Properties temp = (Properties) obj;
            String name;
            String value;
            for(Enumeration enum0 = temp.keys(); enum0.hasMoreElements();) {
                name = (String) enum0.nextElement();
                value = (String) temp.getProperty(name);
                props.setProperty(name, value);
            }
        } else {
            throw new Exception("wrongly used, argument should be 'java.util.Properties'");
        }
    }

    /**
     * 해당 설정정보 반환
     * 
     * @param name         해당 설정명
     * @param defaultValue 해당 설정값이 존재하지 않으면, 반환 값
     * @return 설정값
     */
    public String get(String name, String defaultValue) {
        return props.getProperty(name, defaultValue);
    }

    /**
     * 해당 설정정보 반환
     * 
     * @param name 해당 설정명
     * @return 해당 설정값
     */
    public String get(String key) throws UnsupportedEncodingException {
        String str = props.getProperty(key);
        return str != null && !str.equals("") ? new String(str.getBytes("8859_1"), "EUC-KR") : str;
    }

    public String getProperty(String key) {
        return props.getProperty(key);
    }

    public String getProperty(String key, Object[] args) {
        MessageFormat format = null;
        synchronized (formats) {
            format = (MessageFormat) formats.get(key);
            if(format == null) {
                String formatString = props.getProperty(key);
                format = new MessageFormat(escape(formatString));
                format.setLocale(defaultLocale);
                formats.put(key, format);
            }
        }
        return format.format(args);
    }

    public String getProperty(String key, Object arg0) {
        return getProperty(key, new Object[] { arg0 });
    }

    public String getProperty(String key, Object arg0, Object arg1) {
        return getProperty(key, new Object[] { arg0, arg1 });
    }

    public String getProperty(String key, Object arg0, Object arg1, Object arg2) {
        return getProperty(key, new Object[] { arg0, arg1, arg2 });
    }

    public String getProperty(String key, Object arg0, Object arg1, Object arg2, Object arg3) {
        return getProperty(key, new Object[] { arg0, arg1, arg2, arg3 });
    }

    /**
     * 기본실행 메소드 - itv.properties 에서 데이터 가져온다.
     * 
     * @param args
     */
    public static void main(String[] args) {
    }

    /**
     * @return Returns the props.
     */
    public Properties getProperties() {
        return props;
    }

    /**
     * @param props The props to set.
     */
    public void setProperties(Properties props) {
        ConfigUtil.props = props;
    }

    public void debugString() {
        if(props.size() > 0) {
            String name;
            for(Enumeration enum0 = props.keys(); enum0.hasMoreElements();) {
                name = (String) enum0.nextElement();
                System.out.println(name + "=" + (String) props.getProperty(name));
            }
        }
    }

    /**
     * Escape any single quote characters that are included in the specified message
     * string.
     * 
     * @param string The string to be escaped
     */
    protected String escape(String string) {
        if((string == null) || (string.indexOf('\'') < 0)) {
            return string;
        }
        int n = string.length();
        StringBuffer sb = new StringBuffer(n);
        for(int i = 0; i < n; i++) {
            char ch = string.charAt(i);
            if(ch == '\'') {
                sb.append('\'');
            }
            sb.append(ch);
        }
        return sb.toString();
    }
}