package com.common.module.util;

import java.util.Random;

public class CreateRandomTextUtil {

    public static void main(String[] args) throws Exception {
        StringBuilder logs = new StringBuilder();
        CreateRandomTextUtil cu = new CreateRandomTextUtil();
        logs.append("랜덤 문자열[").append(cu.createRandomString(32)).append("]");
        System.out.println(logs.toString());
    }

    public CreateRandomTextUtil() {
    }

    public String createRandomString(int num) {
        StringBuilder sb = new StringBuilder();
        try {
            Random rnd = new Random();
            for (int i = 0; i < num; i++) {
                int rIndex = rnd.nextInt(2);
                switch (rIndex) {
                case 0:
                    // a-z
                    sb.append((char) ((int) (rnd.nextInt(26)) + 97));
                    break;
                case 1:
                    // 0-9
                    sb.append((rnd.nextInt(10)));
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}