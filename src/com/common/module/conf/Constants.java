package com.common.module.conf;

import java.util.HashMap;
import java.util.TreeMap;

import com.common.module.util.ConfigUtil;

/**
 * 어플리케이션에서 사용되는 상수값을 정의하는 인터페이스
 * 
 * @author hanuli
 */
public class Constants {

    /**
     * 상수데이터 초기화여부
     */
    public static boolean    isInit       = false; // 초기화 확인 값
    /**
     * 상수데이터를 담고 있는 HashMap 정보
     */
    public static HashMap    configs      = null;
    public static ConfigUtil configs_prop = null;
    public static HashMap    code         = null;

    /**
     * The name of the configuration hashmap stored in application scope.
     */
    public static final String CONFIG = "appConfig";

    /**
     * 상수데이터 초기화
     */
    public static void init() {
        isInit = true;
    }

    /**
     * 상수데이터 정보 리턴
     * 
     * @return
     */
    public static HashMap getConfigs() {
        return configs;
    }

    /**
     * 상수데이터중 해당 파라메터의 정보 리턴
     * 
     * @param key
     * @return
     */
    public static TreeMap getConfigs(String key) {
        if(configs.containsKey(key)) {

            return (TreeMap) configs.get(key);
        } else {
            return null;
        }
    }

    /**
     * 상수데이터중 해당 파라메터의 정보 리턴
     * 
     * @param key
     * @return
     */
    public static String getKey(String key) {
        if(configs.containsKey(key)) {
            return (String) configs.get(key);
        } else {
            return null;
        }
    }

    /**
     * 상수데이터 설정
     * 
     * @param map
     */
    public static void setConfigs(HashMap map) {
        configs = map;
    }

    // 컨텍스트정보
    public static String CONTEXT_NAME = "";
    public static String CONTEXT_PATH = "";

    public final static String USER_SESSION   = "userSession";
    public final static String EDITOR_SESSION = "editorSession";

    // action forwarding initialize

    public static final String ACTION_FORWARD_LIST_KEY      = "list";
    public static final String ACTION_FORWARD_READ_KEY      = "read";
    public static final String ACTION_FORWARD_DETAIL_KEY    = "detail";
    public static final String ACTION_FORWARD_SAVEFORM_KEY  = "saveForm";
    public static final String ACTION_FORWARD_SAVEDONE_KEY  = "saveDone";
    public static final String ACTION_FORWARD_BATCHFORM_KEY = "batchForm";
    public static final String ACTION_FORWARD_BATCHDONE_KEY = "batchDone";
    public static final String ACTION_FORWARD_BRANCH_KEY    = "branch";
    public static final String ACTION_FORWARD_SUCCESS_KEY   = "success";
    public static final String ACTION_FORWARD_FAILURE_KEY   = "failure";

    // default parameter
    public static final String OBJ_ITEM  = "item";
    public static final String OBJ_ITEMS = "items";
    public static final String OBJ_PAGER = "pager";

    // 설정파일정보
    public static final String CONFIG_PROPERTIES_FILE_NAME = "/order.properties";
}