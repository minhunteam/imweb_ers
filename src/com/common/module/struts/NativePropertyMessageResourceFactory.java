package com.common.module.struts;

import org.apache.struts.util.MessageResources;
import org.apache.struts.util.PropertyMessageResourcesFactory;

public class NativePropertyMessageResourceFactory extends PropertyMessageResourcesFactory {

    private static final long serialVersionUID = 3105397720032474727L;

    public MessageResources createResources(String config) {
        return new NativePropertyMessageResources(this, config, this.returnNull);
    }
}