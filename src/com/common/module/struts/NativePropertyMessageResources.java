package com.common.module.struts;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts.util.MessageResourcesFactory;
import org.apache.struts.util.PropertyMessageResources;

public class NativePropertyMessageResources extends PropertyMessageResources {

    private static final long serialVersionUID = -7465491355465508400L;

    public NativePropertyMessageResources(MessageResourcesFactory factory, String config) {
        super(factory, config);
    }

    public NativePropertyMessageResources(MessageResourcesFactory factory, String config, boolean returnNull) {
        super(factory, config, returnNull);
    }

    public String getMessage(Locale locale, String key) {
        return super.getMessage(locale, key);
    }

    protected synchronized void loadLocale(String localeKey) {
        if(log.isTraceEnabled()) {
            log.trace("loadLocale(" + localeKey + ")");
        }

        String mLanguage = null;

        for(Locale row : Locale.getAvailableLocales()) {
            Locale mLocale = new Locale(row.getLanguage(), row.getCountry());
            if(mLocale.toString().equals(localeKey)) {
                mLanguage = row.getLanguage();
                break;
            }
        }

        if(locales.get(mLanguage) != null) {
            return;
        }

        locales.put(mLanguage, mLanguage);

        String name = config.replace('.', '/');

        if(mLanguage.length() > 0) {
            name += "_" + mLanguage;
        }

        name += ".properties";

        try {

            InputStream is = null;

            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

            if(classLoader == null) {
                classLoader = this.getClass().getClassLoader();
            }

            is = classLoader.getResourceAsStream(name);

            // _ko 파일이 없을경우 원파일을 다시 읽는다.
            if(is == null) {
                is = classLoader.getResourceAsStream(config.replace('.', '/') + ".properties");
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(is));

            while (true) {
                String val = in.readLine();
                if(val == null) {
                    break;
                } else if(val.startsWith("#")) {
                    continue;
                } else if(val.length() == 0) {
                    continue;
                }
                StringTokenizer st = new StringTokenizer(val, "=");
                String key = st.nextToken();
                String value = st.nextToken();
                String text = StringEscapeUtils.unescapeJava(value);
                synchronized (messages) {
                    if(log.isTraceEnabled()) {
                        log.trace("  Saving message key '" + messageKey(mLanguage, key));
                    }
                    messages.put(messageKey(mLanguage, key), text);
                }
            }

            in.close();

            is.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}