package com.common.module.transaction;

import com.common.module.base.BaseDaoSupport;
import com.common.module.logger.LoggerManager;

public class TransactionHelperImpl implements TransactionHelper {

    protected final static LoggerManager log = new LoggerManager();

    protected final static String NL = System.getProperty("line.separator", "\n");

    private BaseDaoSupport dao;

    public void setDao(BaseDaoSupport dao) {
        this.dao = dao;
    }

    public void start() throws Exception {
        try {
            dao.startTransaction();
        } catch (Exception e) {
            log.error("Failed to execute startTransaction()", e);
            throw new RuntimeException(e);
        }
    }

    public void end() throws Exception {
        try {
            dao.endTransaction();
        } catch (Exception e) {
            log.error("Failed to execute endTransaction()", e);
            throw new RuntimeException(e);
        }
    }

    public void commit() throws Exception {
        try {
            dao.commitTransaction();
        } catch (Exception e) {
            log.error("Failed to execute commitTransaction()", e);
            throw new RuntimeException(e);
        }
    }

    public void startBatch() throws Exception {
        try {
            dao.startBatch();
        } catch (Exception e) {
            log.error("Failed to execute startBatch()", e);
            throw new RuntimeException(e);
        }
    }

    public int executeBatch() throws Exception {
        try {
            return dao.executeBatch();
        } catch (Exception e) {
            log.error("Failed to execute executeBatch()", e);
            throw new RuntimeException(e);
        }
    }
}