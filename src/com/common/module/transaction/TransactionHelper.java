package com.common.module.transaction;

public interface TransactionHelper {
    public void start() throws Exception;

    public void end() throws Exception;

    public void commit() throws Exception;

    public void startBatch() throws Exception;

    public int executeBatch() throws Exception;
}