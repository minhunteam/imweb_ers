package com.common.module.base;

public interface BaseDaoSupport {
    public Object select(String queryName) throws Exception;

    public Object select(String queryName, Object param) throws Exception;

    public Object list(String queryName) throws Exception;

    public Object list(String queryName, Object param) throws Exception;

    public void call(String queryName, Object param) throws Exception;

    public int save(String queryUpdate, String queryInsert, Object param) throws Exception;

    public int add(Object param, String queryInsert) throws Exception;

    public Object add(String queryInsert, Object param) throws Exception;

    public int mod(String queryName, Object param) throws Exception;

    public int del(String queryName, Object param) throws Exception;

    public void startTransaction() throws Exception;

    public void endTransaction() throws Exception;

    public void commitTransaction() throws Exception;

    public void startBatch() throws Exception;

    public int executeBatch() throws Exception;
}