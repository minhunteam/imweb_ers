package com.common.module.base;

import java.io.Serializable;
import java.net.InetAddress;

import com.common.module.logger.LoggerManager;

public class BaseDao implements Serializable {

    private static final long serialVersionUID = -7762951846977856371L;

    protected final String TAG = this.getClass().getSimpleName();

    protected final static String NL = System.getProperty("line.separator", "\n");

    protected final static LoggerManager log = new LoggerManager();

    protected StringBuilder logs = new StringBuilder();

    protected String returnCode = "";

    protected BaseDaoSupport dao;

    public void setDao(BaseDaoSupport dao) {
        this.dao = dao;
    }

    public String getIpAddress() throws Exception {
        return InetAddress.getLocalHost().getHostAddress();
    }

    public boolean isDevMode() throws Exception {
        boolean flag = false;
        try {
            if("192.168.0.250".equals(getIpAddress()) || "192.168.1.2".equals(getIpAddress()) || "1.214.89.169".equals(getIpAddress()) || "1.214.89.173".equals(getIpAddress())) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
}