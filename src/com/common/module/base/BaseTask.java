package com.common.module.base;

import java.io.Serializable;
import java.net.InetAddress;

import javax.servlet.http.HttpServletRequest;

import com.common.module.logger.LoggerManager;

public class BaseTask implements Serializable {

    private static final long serialVersionUID = 1636781664912720763L;

    protected final String TAG = this.getClass().getSimpleName();

    protected final static String NL = System.getProperty("line.separator", "\n");

    protected final static LoggerManager log  = new LoggerManager();

    protected StringBuilder logs = new StringBuilder();

    public String getIpAddress() throws Exception {
        return InetAddress.getLocalHost().getHostAddress();
    }

    public boolean isDevMode() throws Exception {
        boolean flag = false;
        try {
            if("192.168.0.250".equals(getIpAddress()) || "1.214.89.169".equals(getIpAddress()) || "1.214.89.173".equals(getIpAddress())) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] devMode error | ", e);
            throw e;
        }
        return flag;
    }

    public String getClientIp(HttpServletRequest req) throws Exception {
        String clientIp = "";
        try {
            clientIp = req.getRemoteAddr();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] getClientIp error | ", e);
            throw e;
        }
        return clientIp;
    }
}