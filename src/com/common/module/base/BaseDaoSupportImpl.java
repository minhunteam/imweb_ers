package com.common.module.base;

import java.io.Serializable;
import java.net.InetAddress;
import java.sql.SQLException;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.ibatis.sqlmap.engine.impl.SqlMapClientImpl;
import com.ibatis.sqlmap.engine.mapping.parameter.ParameterMap;
import com.ibatis.sqlmap.engine.mapping.parameter.ParameterMapping;
import com.ibatis.sqlmap.engine.mapping.statement.MappedStatement;
import com.ibatis.sqlmap.engine.scope.SessionScope;
import com.ibatis.sqlmap.engine.scope.StatementScope;
import com.common.module.logger.LoggerManager;

public class BaseDaoSupportImpl extends SqlMapClientDaoSupport implements BaseDaoSupport, Serializable {

    private static final long serialVersionUID = -155990168202434993L;

    private final static LoggerManager log = new LoggerManager();

    private final static String NL = System.getProperty("line.separator", "\n");

    private String getIpAddress() throws Exception {
        return InetAddress.getLocalHost().getHostAddress();
    }

    private boolean isSuccessed() throws Exception {
        if("192.168.0.250".equals(getIpAddress()) || "192.168.1.2".equals(getIpAddress()) || "1.214.89.169".equals(getIpAddress()) || "1.214.89.173".equals(getIpAddress())) {
            return true;
        }
        return false;
    }

    private boolean isErrored() throws Exception {
        if("192.168.0.250".equals(getIpAddress()) || "192.168.1.2".equals(getIpAddress()) || "1.214.89.169".equals(getIpAddress()) || "1.214.89.173".equals(getIpAddress())) {
            return true;
        }
        return false;
    }

    public Object select(String queryName) throws SQLException {
        Object object = new Object();
        try {
            object = getSqlMapClient().queryForObject(queryName);
            if(isSuccessed()) {
                getSqlQuery(queryName, "");
            }
        } catch (Exception e) {
            log.error("Failed to execute select(String queryName)", e);
            try {
                if(isErrored()) {
                    getSqlQuery(queryName, "");
                }
            } catch (Exception i) {
                i.printStackTrace();
            }
        }
        return object;
    }

    public Object select(String queryName, Object param) throws SQLException {
        Object object = new Object();
        try {
            object = getSqlMapClient().queryForObject(queryName, param);
            if(isSuccessed()) {
                getSqlQuery(queryName, param);
            }
        } catch (Exception e) {
            log.error("Failed to execute select(String queryName, Object param)", e);
            try {
                if(isErrored()) {
                    getSqlQuery(queryName, param);
                }
            } catch (Exception i) {
                i.printStackTrace();
            }

        }
        return object;
    }

    public Object list(String queryName) throws SQLException {
        Object object = new Object();
        try {
            object = getSqlMapClient().queryForList(queryName);
            if(isSuccessed()) {
                getSqlQuery(queryName, "");
            }
        } catch (Exception e) {
            log.error("Failed to execute list(String queryName)", e);
            try {
                if(isErrored()) {
                    getSqlQuery(queryName, "");
                }
            } catch (Exception i) {
                i.printStackTrace();
            }
        }
        return object;
    }

    public Object list(String queryName, Object param) throws SQLException {
        Object object = new Object();
        try {
            object = getSqlMapClient().queryForList(queryName, param);
            if(isSuccessed()) {
                getSqlQuery(queryName, param);
            }
        } catch (Exception e) {
            log.error("Failed to execute list(String queryName, Object param)", e);
            try {
                if(isErrored()) {
                    getSqlQuery(queryName, param);
                }
            } catch (Exception i) {
                i.printStackTrace();
            }
        }
        return object;
    }

    public void call(String queryName, Object param) throws SQLException {
        try {
            getSqlMapClient().queryForObject(queryName, param);
            if(isSuccessed()) {
                getSqlQuery(queryName, param);
            }
        } catch (Exception e) {
            log.error("Failed to execute call(String queryName, Object param)", e);
            try {
                if(isErrored()) {
                    getSqlQuery(queryName, param);
                }
            } catch (Exception i) {
                i.printStackTrace();
            }
        }
    }

    public Object add(String queryName, Object param) throws SQLException {
        Object object = new Object();
        try {
            object = getSqlMapClient().insert(queryName, param);
            if(isSuccessed()) {
                getSqlQuery(queryName, param);
            }
        } catch (Exception e) {
            log.error("Failed to execute add(String queryName, Object param)", e);
            try {
                if(isErrored()) {
                    getSqlQuery(queryName, param);
                }
            } catch (Exception i) {
                i.printStackTrace();
            }
        }
        return object;
    }

    public int add(Object param, String queryName) throws SQLException {
        int n = 0;
        String str = "";
        try {

            str = (String) getSqlMapClient().insert(queryName, param);
            if("".equals(str)) {
                n = 0;
            } else {
                n++;
            }
            if(isSuccessed()) {
                getSqlQuery(queryName, param);
            }
        } catch (Exception e) {
            n = -1;
            log.error("Failed to execute add(Object param, String queryName)", e);
            try {
                if(isErrored()) {
                    getSqlQuery(queryName, param);
                }
            } catch (Exception i) {
                i.printStackTrace();
            }
        }
        return n;
    }

    public int mod(String queryName, Object param) throws SQLException {
        int n = 0;
        try {
            n = getSqlMapClient().update(queryName, param);
            if(isSuccessed()) {
                getSqlQuery(queryName, param);
            }
        } catch (Exception e) {
            n = -1;
            log.error("Failed to execute mod(String queryName, Object param)", e);
            try {
                if(isErrored()) {
                    getSqlQuery(queryName, param);
                }
            } catch (Exception i) {
                i.printStackTrace();
            }
        }
        return n;
    }

    public int del(String queryName, Object param) throws SQLException {
        int n = 0;
        try {
            n = getSqlMapClient().delete(queryName, param);
            if(isSuccessed()) {
                getSqlQuery(queryName, param);
            }
        } catch (Exception e) {
            n = -1;
            log.error("Failed to execute del(String queryName, Object param)", e);
            try {
                if(isErrored()) {
                    getSqlQuery(queryName, param);
                }
            } catch (Exception i) {
                i.printStackTrace();
            }
        }
        return n;
    }

    public int save(String queryUpdate, String queryInsert, Object param) throws SQLException {
        int n = 0;
        String queryName = "";
        try {
            queryName = queryUpdate;
            n = mod(queryName, param);
            if(n == 0) {
                queryName = queryInsert;
                n = add(param, queryName);
            }
        } catch (Exception e) {
            n = -1;
            log.error("Failed to execute save(String queryUpdate, String queryInsert, Object param)", e);
            try {
                if(isErrored()) {
                    getSqlQuery(queryName, param);
                }
            } catch (Exception i) {
                i.printStackTrace();
            }
        }
        return n;
    }

    public void startTransaction() throws Exception {
        try {
            getSqlMapClient().startTransaction();
            getSqlMapClient().getCurrentConnection().setAutoCommit(false);
        } catch (Exception e) {
            log.error("Failed to execute startTransaction()", e);
            throw new RuntimeException(e);
        }
    }

    public void endTransaction() throws Exception {
        try {
            getSqlMapClient().endTransaction();
        } catch (Exception e) {
            log.error("Failed to execute endTransaction()", e);
            throw new RuntimeException(e);
        }
    }

    public void commitTransaction() throws Exception {
        try {
            getSqlMapClient().commitTransaction();
        } catch (Exception e) {
            log.error("Failed to execute commitTransaction()", e);
            throw new RuntimeException(e);
        }
    }

    public void startBatch() throws Exception {
        try {
            getSqlMapClient().startBatch();
        } catch (Exception e) {
            log.error("Failed to execute startBatch()", e);
            throw new RuntimeException(e);
        }
    }

    public int executeBatch() throws Exception {
        try {
            return getSqlMapClient().executeBatch();
        } catch (Exception e) {
            log.error("Failed to execute executeBatch()", e);
            throw new RuntimeException(e);
        }
    }

    public void getSqlQuery(String sqlId, Object parameterObject) {
        try {
            MappedStatement mappedStatement;
            SqlMapClientImpl sqlMapClient;
            StatementScope statementScope;
            SessionScope sessionScope;
            sqlMapClient = (SqlMapClientImpl) getSqlMapClientTemplate().getSqlMapClient();
            mappedStatement = sqlMapClient.getMappedStatement(sqlId);
            sessionScope = new SessionScope();
            statementScope = new StatementScope(sessionScope);
            mappedStatement.initRequest(statementScope);
            ParameterMap parameterMap = mappedStatement.getSql().getParameterMap(statementScope, parameterObject);
            Object[] bindParameter = parameterMap.getParameterObjectValues(statementScope, parameterObject);
            ParameterMapping[] parameterMappinge = parameterMap.getParameterMappings();
            String preparedStatement = mappedStatement.getSql().getSql(statementScope, parameterObject);
            for(int i = 0; i < bindParameter.length; i++) {
                if(parameterMappinge[i].getJdbcType() == 0) {
                    preparedStatement = preparedStatement.replaceFirst("[?]", "'" + bindParameter[i].toString() + "'");
                } else {
                    if(bindParameter[i] != null) {
                        preparedStatement = preparedStatement.replaceFirst("[?]", bindParameter[i].toString());
                    }
                }
            }

            log.info(NL + singleSpaceReturn(preparedStatement) + NL);

        } catch (Exception e) {
            log.error("Failed to execute getSqlQuery()", e);
        }
    }

    public static String singleSpaceReturn(String text) {
        char[] toCharArray = text.toCharArray();
        int index = 1;
        for(int i = 1; i < toCharArray.length; i++) {
            toCharArray[index] = toCharArray[i];
            if(toCharArray[index] != ' ') {
                index++;
            } else if(toCharArray[index - 1] != ' ') {
                index++;
            }
        }
        return new String(toCharArray, 0, index).trim();
    }
}