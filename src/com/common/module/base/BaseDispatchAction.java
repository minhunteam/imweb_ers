package com.common.module.base;

import java.io.Serializable;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.actions.DispatchAction;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.common.module.logger.LoggerManager;
import com.common.module.menu.service.CommonMenuService;
import com.common.module.parser.FormParser;

public class BaseDispatchAction extends DispatchAction implements Serializable {

    private static final long serialVersionUID = 3001835873586717150L;

    public final String TAG = this.getClass().getSimpleName();

    public final static String NL = System.getProperty("line.separator", "\n");

    public static StringBuilder logs = new StringBuilder();

    public final static LoggerManager log = new LoggerManager();

    public static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    protected FormParser fp = new FormParser();

    protected String returnCode = "";

    protected String rowno = "1";

    protected String topMenuNo = "";

    protected String totalRecords = "0";

    private CommonMenuService commonMenuService;

    protected String getIpAddress() throws Exception {
        return InetAddress.getLocalHost().getHostAddress();
    }

    protected void RequestHeaders(HttpServletRequest req) throws Exception {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(NL).append("[" + TAG + "] ============================== Header 상태체크 시작 ==============================");

            System.out.println("getServlet()[" + req + "]");
            System.out.println("HttpServletRequest()[" + req.getServletContext() + "]");
            System.out.println("getServlet()[" + getServlet() + "]");

            ServletContext sc = req.getServletContext();

            System.out.println("ServletContext()[" + sc + "]");

            WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(sc);

            commonMenuService = (CommonMenuService) wac.getBean("commonMenuService");

            sb.append(NL).append("HeaderInterceptor START");
            sb.append(NL).append("================ * Header Information * START ===============");

            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            HttpSession session = req.getSession(true);
            session.setMaxInactiveInterval(60 * 60);

            sb.append(NL).append("session getId[" + session.getId() + "]");
            sb.append(NL).append("session isNew[" + session.isNew() + "]");
            sb.append(NL).append("session getCreationTime[" + fmt.format(session.getCreationTime()) + "]");
            sb.append(NL).append("session LastAccessedTime[" + fmt.format(session.getLastAccessedTime()) + "]");
            sb.append(NL).append("session MaxInactiveInterval[" + session.getMaxInactiveInterval() + "]");

            List<String> headers = Collections.<String>list(req.getHeaderNames());

            for(String value : headers) {
                sb.append(NL).append("[" + value + "=" + req.getHeader(value) + "]");
            }

            sb.append(NL).append("================ * Header Information *   END ===============");
            sb.append(NL).append("================ * Client Information * START ===============");

            String ip = req.getHeader("X-Forwarded-For");

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = req.getHeader("Proxy-Client-IP");
            }

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = req.getHeader("WL-Proxy-Client-IP");
            }

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = req.getHeader("HTTP_CLIENT_IP");
            }

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = req.getHeader("HTTP_X_FORWARDED_FOR");
            }

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = req.getRemoteAddr();
            }

            sb.append(NL).append("Client IP Address [" + ip + "]");
            sb.append(NL).append("Locale            [" + req.getLocale() + "]");
            sb.append(NL).append("Method            [" + req.getMethod() + "]");
            sb.append(NL).append("================ * Client Information *   END ===============");
            sb.append(NL).append("================ * Parameter Information * START ===============");

            StringBuilder tokens = new StringBuilder();

            String uri = req.getRequestURI();
            String url = req.getRequestURL().toString();

            if("POST".equals(req.getMethod())) {
                List<String> items = Collections.<String>list(req.getParameterNames());
                sb.append(NL).append("param [");
                for(int i = 0; i < items.size(); i++) {
                    String value = items.get(i);
                    sb.append(NL).append(value + "=" + req.getParameter(value));
                    if(i == items.size() - 1) {
                        tokens.append(value).append("=").append(req.getParameter(value));
                    } else {
                        tokens.append(value).append("=").append(req.getParameter(value)).append("&");
                    }
                }
                sb.append(NL).append("]");
                sb.append(NL).append("param[" + tokens + "]");
            } else if("GET".equals(req.getMethod())) {
                String param = req.getQueryString();
                sb.append(NL).append("param[" + param + "]");
                tokens.append(param);
            }

            sb.append(NL).append("================ * Parameter Information *   END ===============");
            sb.append(NL).append("================ * Url Information * START ===============");

            sb.append(NL).append("URL[" + url + "?" + tokens + "]");

            sb.append(NL).append("================ * Url Information *   END ===============");

            sb.append(NL).append("================ * Menu Information * START ===============");

            commonMenuService.topMenuList("");
            commonMenuService.subMenuList("");

            sb.append(NL).append("================ * Menu Information *   END ===============");

        } catch (Exception e) {
            e.printStackTrace();
            log.error("[" + TAG + "]", e);
        } finally {
            sb.append(NL).append("[" + TAG + "] ============================== Header 상태체크 종료 ==============================");
            if(isDevMode()) {
                log.info(sb);
                sb.setLength(0);
            } else {
                sb.setLength(0);
            }
        }
    }

    protected boolean isDevMode() throws Exception {
        boolean flag = false;
        try {
            if("192.168.0.250".equals(getIpAddress()) || "192.168.1.2".equals(getIpAddress()) || "1.214.89.169".equals(getIpAddress()) || "1.214.89.173".equals(getIpAddress()) || "211.177.246.234".equals(getIpAddress())) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[" + TAG + "]", e);
        }
        return flag;
    }
}