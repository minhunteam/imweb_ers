package com.common.module.base;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class BaseDTO implements Serializable {

    private int    limit          = 0;
    private String rowno          = "1";
    private String rownum         = "10";
    private String route          = "";
    private String createdate     = "";
    private String updateid       = "";
    private String lastupdatetime = "";

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getRowno() {
        return rowno;
    }

    public void setRowno(String rowno) {
        this.rowno = rowno;
    }

    public void setRowno(int rowno) {
        this.rowno = Integer.toString(rowno);
    }

    public String getRownum() {
        return rownum;
    }

    public void setRownum(String rownum) {
        this.rownum = rownum;
    }

    public void setRownum(int rownum) {
        this.rownum = Integer.toString(rownum);
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getCreatedate() {
        return createdate;
    }

    public void setCreatedate(String createdate) {
        this.createdate = createdate;
    }

    public String getUpdateid() {
        return updateid;
    }

    public void setUpdateid(String updateid) {
        this.updateid = updateid;
    }

    public String getLastupdatetime() {
        return lastupdatetime;
    }

    public void setLastupdatetime(String lastupdatetime) {
        this.lastupdatetime = lastupdatetime;
    }

    public BaseDTO() {
    }

    public BaseDTO(Object obj) {
        try {
            set(obj);
        } catch (Exception e) {
        }
    }

    /**
     * bean 의 field value 를 리턴한다
     * 
     * @param field
     * @return
     */
    public final Object get(String field) {
        if(field == null || field.trim().length() < 1)
            return null;
        try {
            String methodName = "get" + field.substring(0, 1).toUpperCase() + field.substring(1);
            Method targetMethod = this.getClass().getDeclaredMethod(methodName, (Class[]) null);
            return targetMethod.invoke(this, (Object[]) null);
        } catch (Exception ex) {
            return null;
        }
    }

    public final void set(String field, String value) {
        if(field == null || field.trim().length() < 1)
            return;
        try {
            String methodName = "set" + field.substring(0, 1).toUpperCase() + field.substring(1);
            Class[] methodArgs = new Class[1];
            methodArgs[0] = String.class;
            Method targetMethod = null;
            String[] args = new String[1];
            args[0] = value;// Locale.toWeb(value);
            targetMethod = this.getClass().getDeclaredMethod(methodName, methodArgs);
            targetMethod.invoke(this, (Object[]) args);
        } catch (Exception ex) {
        }
    }

    /**
     * 해당 Bean 의 String 형 반환에 사용
     * 
     * @return String 으로 변환된 bean
     */
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    /**
     * 세터로 사용될 메소드모음셋 클레스를 인자로 받아 그 클레스에 있는 모든 메소드 중 메소드 명이 set으로 시작하는 것들 중에서 파라미터가
     * 한개인 메소드를 세터로 간주하여 이 세터들만 HashMap에 담아둔다.
     * 
     * @param cls
     * @return
     * @throws Exception
     */
    public static Map getSetter(Class cls) throws Exception {
        Map<String, Method> set = new HashMap<String, Method>();

        try {
            Method methods[] = cls.getMethods();
            set = new HashMap<String, Method>();
            String key;
            for(int m = 0; m < methods.length; m++) {
                // set으로 시작하고 파라미터가 하나이면 세터로 간주한다.
                if(methods[m].getName().startsWith("set") && methods[m].getName().length() > 3 && methods[m].getParameterTypes().length == 1) {

                    // 파라미터에 기본데이터형이 있으면 안된다.
                    // 모두 클레스타입이어야한다.
                    Class params[] = methods[m].getParameterTypes();
                    int paramCnt = params.length;
                    boolean bSet = true;
                    for(int p = 0; p < paramCnt; p++) {
                        // .이 없다는것은 클레스타입이 아니고 기본데이터타입이다.
                        // int, double, long, short, char 등...
                        if(params[p].getName().indexOf(".") < 0) {
                            // 기본데이터타입은 세터에 추가하지 않는다.
                            bSet = false;
                            break;
                        }
                    }

                    if(bSet) {
                        key = (methods[m].getName()).substring(3).toLowerCase();
                        if(set.get(key) == null) {
                            set.put(key, methods[m]);
                        }
                    }
                }
            }

        } catch (Exception ex) {
        }

        return set;
    }

    /**
     * 타 bean 과의 비교
     * 
     * @param o 비교대상 bean
     * @return 비교결과
     */
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    /**
     * @param o
     * @return
     */
    public int hashCode(Object o) {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    // reflaction
    protected boolean isBlank(String s) {
        return ((s == null) || ("".equals(s)));
    }

    public Map describe() throws Exception {
        try {
            return BeanUtils.describe(this);
        } catch (Throwable t) {
            throw new Exception(t);
        }
    }

    public void set(Object o) throws Exception {
        try {
            if(o instanceof Map) {
                BeanUtils.populate(this, (Map) o);
            } else
                BeanUtils.populate(this, BeanUtils.describe(o));
        } catch (Throwable t) {
            throw new Exception(t);
        }
    }

    public void populate(Object o) throws Exception {
        try {
            BeanUtils.populate(o, this.describe());
        } catch (Throwable t) {
            throw new Exception(t);
        }
    }
}