package com.common.module.base;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class BaseJsonData implements Serializable {

    private String success;
    private String message;
    private String total;
    private String scnt;
    private String ecnt;
    private String size;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public void setTotal(int total) {
        this.total = Integer.toString(total);
    }

    public String getScnt() {
        return scnt;
    }

    public void setScnt(String scnt) {
        this.scnt = scnt;
    }

    public void setScnt(int scnt) {
        this.scnt = Integer.toString(scnt);
    }

    public String getEcnt() {
        return ecnt;
    }

    public void setEcnt(String ecnt) {
        this.ecnt = ecnt;
    }

    public void setEcnt(int ecnt) {
        this.ecnt = Integer.toString(ecnt);
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setSize(int size) {
        this.size = Integer.toString(size);
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}