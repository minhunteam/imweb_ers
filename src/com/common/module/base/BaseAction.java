package com.common.module.base;

import java.io.Serializable;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.common.module.conf.Constants;
import com.common.module.logger.LoggerManager;
import com.common.module.parser.FormParser;
import com.common.module.session.vo.UserSessionVO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class BaseAction extends DispatchAction implements Serializable {

    private static final long serialVersionUID = 7546672134541473934L;

    public final String TAG = this.getClass().getSimpleName();

    public final static String NL = System.getProperty("line.separator", "\n");

    public static StringBuilder logs = new StringBuilder();

    public final static LoggerManager log = new LoggerManager();

    public static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    protected FormParser fp = new FormParser();

    protected String returnCode = "";

    protected String rowno = "1";

    protected String topMenuNo = "";

    protected String totalRecords = "0";

//    private CommonMenuService commonMenuService;

    protected String getIpAddress() throws Exception {
        return InetAddress.getLocalHost().getHostAddress();
    }

    /**
     * 프론트 Actions 에서 공통으로 처리해야 할 Method 의 정의 dispatch action 의 Execute 를 재정의함
     */

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
//        this.RequestHeaders(req);
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(NL).append("[" + TAG + "] ============================== Header 상태체크 시작 ==============================");

            System.out.println("getServlet().getServletContext()[" + getServlet().getServletContext() + "]");

//            WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(getServlet().getServletContext());

//            commonMenuService = (CommonMenuService) wac.getBean("commonMenuService");

            sb.append(NL).append("HeaderInterceptor START");
            sb.append(NL).append("================ * Header Information * START ===============");

            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            HttpSession session = request.getSession(true);
            session.setMaxInactiveInterval(60 * 60);

            sb.append(NL).append("session getId[" + session.getId() + "]");
            sb.append(NL).append("session isNew[" + session.isNew() + "]");
            sb.append(NL).append("session getCreationTime[" + fmt.format(session.getCreationTime()) + "]");
            sb.append(NL).append("session LastAccessedTime[" + fmt.format(session.getLastAccessedTime()) + "]");
            sb.append(NL).append("session MaxInactiveInterval[" + session.getMaxInactiveInterval() + "]");

            List<String> headers = Collections.<String>list(request.getHeaderNames());

            for(String value : headers) {
                sb.append(NL).append("[" + value + "=" + request.getHeader(value) + "]");
            }

            sb.append(NL).append("================ * Header Information *   END ===============");
            sb.append(NL).append("================ * Client Information * START ===============");

            String ip = request.getHeader("X-Forwarded-For");

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
            }

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("WL-Proxy-Client-IP");
            }

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_CLIENT_IP");
            }

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getHeader("HTTP_X_FORWARDED_FOR");
            }

            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                ip = request.getRemoteAddr();
            }

            sb.append(NL).append("Client IP Address [" + ip + "]");
            sb.append(NL).append("Locale            [" + request.getLocale() + "]");
            sb.append(NL).append("Method            [" + request.getMethod() + "]");
            sb.append(NL).append("================ * Client Information *   END ===============");
            sb.append(NL).append("================ * Parameter Information * START ===============");

            StringBuilder tokens = new StringBuilder();

            String uri = request.getRequestURI();
            String url = request.getRequestURL().toString();

            if("POST".equals(request.getMethod())) {
                List<String> items = Collections.<String>list(request.getParameterNames());
                sb.append(NL).append("param [");
                for(int i = 0; i < items.size(); i++) {
                    String value = items.get(i);
                    sb.append(NL).append(value + "=" + request.getParameter(value));
                    if(i == items.size() - 1) {
                        tokens.append(value).append("=").append(request.getParameter(value));
                    } else {
                        tokens.append(value).append("=").append(request.getParameter(value)).append("&");
                    }
                }
                sb.append(NL).append("]");
                sb.append(NL).append("param[" + tokens + "]");
            } else if("GET".equals(request.getMethod())) {
                String param = request.getQueryString();
                sb.append(NL).append("param[" + param + "]");
                tokens.append(param);
            }

            sb.append(NL).append("================ * Parameter Information *   END ===============");
            sb.append(NL).append("================ * Url Information * START ===============");

            sb.append(NL).append("ServletPath[" + request.getServletPath() + "]");
            sb.append(NL).append("URL[" + url + "?" + tokens + "]");

            sb.append(NL).append("================ * Url Information *   END ===============");

            sb.append(NL).append("================ * Menu Information * START ===============");

            sb.append(NL).append("================ * Menu Information *   END ===============");

            String prefix = mapping.getModuleConfig().getPrefix();
            String loginPath = mapping.getModuleConfig().findForwardConfig("LoginForm").getPath();

            sb.append(NL).append("isLoginChecked[" + isLoginChecked(request) + "]");

            if(new String(prefix + loginPath).indexOf(request.getServletPath()) == 0 || "/connect.do".indexOf(request.getServletPath()) == 0) {
                return super.execute(mapping, form, request, response);
            } else if(isLoginChecked(request)) {
                return mapping.findForward("LoginForm");
            }

            sb.append(NL).append("session prefix[" + prefix + "]");
            sb.append(NL).append("session prefix[" + loginPath + "]");

        } catch (Exception e) {
            e.printStackTrace();
            log.error("[" + TAG + "]", e);
        } finally {
            sb.append(NL).append("[" + TAG + "] ============================== Header 상태체크 종료 ==============================");
            if(isDevMode()) {
                log.info(sb);
                sb.setLength(0);
            } else {
                sb.setLength(0);
            }
        }

        return super.execute(mapping, form, request, response);
    }

    protected boolean isLoginChecked(HttpServletRequest request) throws Exception {

        StringBuilder sb = new StringBuilder();

        boolean status = false;

        try {
            sb.append(NL).append("[" + TAG + "] ============================== isLoginChecked 상태체크 시작 ==============================");
            HttpSession session = request.getSession(true);
            session.setMaxInactiveInterval(60 * 60);

            UserSessionVO user = new UserSessionVO();

            user = (UserSessionVO) session.getAttribute(Constants.USER_SESSION);

            if(user == null) {
                status = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[" + TAG + "]", e);
        } finally {
            sb.append(NL).append("[" + TAG + "] ============================== isLoginChecked 상태체크 종료 ==============================");
            if(isDevMode()) {
                log.info(sb);
                sb.setLength(0);
            } else {
                sb.setLength(0);
            }
        }
        return status;
    }

    protected boolean isDevMode() throws Exception {
        boolean flag = false;
        try {
            if("192.168.0.250".equals(getIpAddress()) || "192.168.1.2".equals(getIpAddress()) || "1.214.89.169".equals(getIpAddress()) || "1.214.89.173".equals(getIpAddress()) || "211.177.246.234".equals(getIpAddress())) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[" + TAG + "]", e);
        }
        return flag;
    }
}