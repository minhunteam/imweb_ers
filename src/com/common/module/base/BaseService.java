package com.common.module.base;

import java.io.Serializable;
import java.net.InetAddress;

import javax.servlet.http.HttpServletRequest;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberType;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.common.module.cipher.ASECipher;
import com.common.module.logger.LoggerManager;
import com.common.module.util.CheckUtil;

public class BaseService implements Serializable {

    private static final long serialVersionUID = -5579916101947556878L;

    public String TAG = this.getClass().getSimpleName();

    public final static String NL = System.getProperty("line.separator", "\n");

    public final static LoggerManager log = new LoggerManager();

    public static StringBuilder logs = new StringBuilder();

    public CheckUtil cu = new CheckUtil();

    private PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

    public ASECipher ase = new ASECipher();

    public String returnCode = "";

    public String processCode = "";

    public String getIpAddress() throws Exception {
        return InetAddress.getLocalHost().getHostAddress();
    }

    public static boolean isEmpty(CharSequence str) {
        if(str == null || str.length() == 0)
            return true;
        else
            return false;
    }

    public static boolean isNotEmpty(CharSequence str) {
        if(str == null || str.length() == 0)
            return false;
        else
            return true;
    }

    public static String getMaxPage(String totalRecords, String pageMax) {
        int maxPages = 1;
        if(isEmpty(totalRecords)) {
            maxPages = 1;
        } else {
            maxPages = (int) Math.ceil((Integer.parseInt(totalRecords) * 1.0) / (Integer.parseInt(pageMax) * 1.0));
        }
        return Integer.toString(maxPages);
    }

    public boolean isDevMode() throws Exception {
        boolean flag = false;
        try {
            if("192.168.0.250".equals(getIpAddress()) || "192.168.1.2".equals(getIpAddress()) || "1.214.89.169".equals(getIpAddress()) || "1.214.89.173".equals(getIpAddress())) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] isDevMode error | ", e);
            throw e;
        }
        return flag;
    }

    public String getClientIp(HttpServletRequest req) throws Exception {
        String clientIp = "";
        try {
            clientIp = req.getRemoteAddr();
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] getClientIp error | ", e);
            throw e;
        }
        return clientIp;
    }

    public boolean isCheckPhoneNumber(String defaultAlpha, String phoneNumber) throws Exception {
        boolean isChecked = false;
        try {
            String mobile = isNationalFormatPhoneNumber(defaultAlpha, phoneNumber);

            boolean isNumbered = isPhoneNumberType(defaultAlpha, mobile);

            if(isNumbered) {
                PhoneNumber number = phoneUtil.parseAndKeepRawInput(mobile, defaultAlpha);
                return phoneUtil.isValidNumber(number);
            } else {
                return isChecked;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] isCheckPhoneNumber error | ", e);
            throw e;
        }
    }

    public boolean isPhoneNumberType(String defaultAlpha, String phoneNumber) throws Exception {
        try {
            boolean isNumbered = false;

            PhoneNumber number = phoneUtil.parseAndKeepRawInput(phoneNumber, defaultAlpha);

            PhoneNumberType numberType = phoneUtil.getNumberType(number);

            if(numberType == PhoneNumberType.MOBILE || numberType == PhoneNumberType.FIXED_LINE_OR_MOBILE) {
                isNumbered = true;
            }
            return isNumbered;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] isPhoneNumberType error | ", e);
            throw e;
        }
    }

    public String isNationalFormatPhoneNumber(String defaultAlpha, String phoneNumber) throws Exception {
        try {
            PhoneNumber number = phoneUtil.parseAndKeepRawInput(phoneNumber, defaultAlpha);
            return phoneUtil.format(number, PhoneNumberFormat.NATIONAL).replaceAll("[^0-9]", "");
        } catch (Exception e) {
            e.printStackTrace();
            log.error(NL + "[" + TAG + "] isNationalFormatPhoneNumber error | ", e);
            throw e;
        }
    }
}