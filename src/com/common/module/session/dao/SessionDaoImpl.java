package com.common.module.session.dao;

import com.common.module.base.BaseDao;

public class SessionDaoImpl extends BaseDao implements SessionDao {

    private static final long serialVersionUID = 4219042344996256991L;

    @Override
    public void checkProcess() throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] checkProcess Start");
            dao.select("ibatis.mysql.common.module.session.checkProcess");
        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] checkProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] checkProcess End");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            }
        }
    }
}