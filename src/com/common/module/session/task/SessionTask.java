package com.common.module.session.task;

import com.common.module.base.BaseTask;
import com.common.module.session.service.SessionService;

public class SessionTask extends BaseTask {

    private static final long serialVersionUID = -8220581543846064200L;

    private SessionService sessionService;

    public void setSessionService(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public void check() throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] check START");
            sessionService.checkProcess();
        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] check Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] check END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            }
        }
    }
}