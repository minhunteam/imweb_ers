package com.common.module.session.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.common.module.session.task.SessionTask;

public class SessionJob extends QuartzJobBean {

    private SessionTask sessionTask;

    public void setSessionTask(SessionTask sessionTask) {
        this.sessionTask = sessionTask;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        try {
            sessionTask.check();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}