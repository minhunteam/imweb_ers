package com.common.module.session.service;

import com.common.module.base.BaseService;
import com.common.module.session.dao.SessionDao;

public class SessionServiceImpl extends BaseService implements SessionService {

    private static final long serialVersionUID = 7620006092705676587L;

    private SessionDao sessionDao;

    public void setSessionDao(SessionDao sessionDao) {
        this.sessionDao = sessionDao;
    }

    @Override
    public void checkProcess() throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] checkProcess START");
            sessionDao.checkProcess();
        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] checkProcess Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] checkProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
    }
}