package com.common.module.session.vo;

import com.common.module.base.BaseVO;
import com.imweb.ers.base.root.vo.RootVO;
import com.imweb.ers.connect.dto.ConnectDTO;
import com.imweb.ers.store.info.vo.StoreVO;

public class UserSessionVO extends BaseVO {

    public static final String SUPERVISOR_ID  = "supervisor";
    public static final String SUPERVISOR_PWD = "supervisor";

    private String userno    = "";
    private String userid    = "";
    private String userpw    = "";
    private String username  = "";
    private String authcode  = "";
    private String route     = "";
    private String storecode = "";
    private BaseVO session   = null;

    public void initSuperVisor() {
        this.userno = "ROOT2019030100000001";
        this.userid = UserSessionVO.SUPERVISOR_ID;
        this.userpw = UserSessionVO.SUPERVISOR_PWD;
        this.username = "슈퍼관리자";
        this.authcode = "AT001";
        this.route = "R001";
    }

    public static UserSessionVO getInstance() {
        return new UserSessionVO();
    }

    public static UserSessionVO getInstance(BaseVO vo) {
        if(vo != null) {
            UserSessionVO user = new UserSessionVO();
            user.setSession(vo);
            return user;
        } else {
            return null;
        }
    }

    private void setSession(Object obj) {
        if(obj instanceof UserSessionVO) {
            this.userno = ((UserSessionVO) obj).getUserno();
            this.userid = ((UserSessionVO) obj).getUserid();
            this.userpw = ((UserSessionVO) obj).getUserpw();
            this.username = ((UserSessionVO) obj).getUsername();
            this.authcode = ((UserSessionVO) obj).getAuthcode();
            this.route = ((UserSessionVO) obj).getRoute();
        }
        if(obj instanceof RootVO) {
            this.userno = ((RootVO) obj).getRootcode();
            this.userid = ((RootVO) obj).getRootid();
            this.userpw = ((RootVO) obj).getRootpw();
            this.username = ((RootVO) obj).getRootname();
            this.authcode = ((RootVO) obj).getAuthcode();
            this.route = "R001";
        }
        if(obj instanceof StoreVO) {
            this.userno = ((StoreVO) obj).getStorecode();
            this.userid = ((StoreVO) obj).getStoreid();
            this.userpw = ((StoreVO) obj).getStorepw();
            this.username = ((StoreVO) obj).getStorename();
            this.storecode = ((StoreVO) obj).getStorecode();
            this.route = "R002";
        }
    }

    public boolean isLogin() {
        return userid != null || session != null;
    }

    public BaseVO getSession() {
        return session;
    }

    public void setUserSessionVo(BaseVO session) {
        this.session = session;
        this.setSession(session);
    }

    public String getUserno() {
        return userno;
    }

    public void setUserno(String userno) {
        this.userno = userno;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserpw() {
        return userpw;
    }

    public void setUserpw(String userpw) {
        this.userpw = userpw;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public String getStorecode() {
        return storecode;
    }

    public void setStorecode(String storecode) {
        this.storecode = storecode;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }
}