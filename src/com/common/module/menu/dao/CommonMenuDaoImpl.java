package com.common.module.menu.dao;

import com.common.module.base.BaseDao;

public class CommonMenuDaoImpl extends BaseDao implements CommonMenuDao {

    @Override
    public Object listProcess(Object param) throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] listProcess START");
            
            logs.append(NL).append("[").append(TAG).append("]").append("★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★★");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] listProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] listProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
        
        return "";
    }
}