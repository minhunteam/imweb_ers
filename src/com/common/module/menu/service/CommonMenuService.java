package com.common.module.menu.service;

public interface CommonMenuService {

    public void topMenuList(Object param) throws Exception;

    public void subMenuList(Object param) throws Exception;

}