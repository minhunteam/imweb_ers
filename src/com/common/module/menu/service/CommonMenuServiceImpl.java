package com.common.module.menu.service;

import com.common.module.base.BaseService;
import com.common.module.menu.dao.CommonMenuDao;

public class CommonMenuServiceImpl extends BaseService implements CommonMenuService {

    private CommonMenuDao commonMenuDao;

    public void setCommonMenuDao(CommonMenuDao commonMenuDao) {
        this.commonMenuDao = commonMenuDao;
    }

    @Override
    public void topMenuList(Object param) throws Exception {

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] topMenuList START");
            commonMenuDao.listProcess("");
        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] topMenuList Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] topMenuList END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
    }

    @Override
    public void subMenuList(Object param) throws Exception {

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] subMenuList START");
            commonMenuDao.listProcess("");
        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] subMenuList Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] subMenuList END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
    }

}