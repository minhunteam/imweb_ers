package com.common.module.parser;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.common.module.logger.LoggerManager;

public class FormParser {

    public final String TAG = this.getClass().getSimpleName();

    protected final static String NL = System.getProperty("line.separator", "\n");

    private static LoggerManager log = new LoggerManager();

    public void formRequestToVo(HttpServletRequest req, Object targetVo) {
        StringBuilder sb = new StringBuilder();
        try {

            sb.append(NL).append("[" + TAG + "] ============================== formRequestToVo 시작 ==============================");

            Field[] fields;
            boolean isFieldExist;
            fields = targetVo.getClass().getDeclaredFields();

            String query = req.getQueryString();

            if(query == null) {

                sb.append(NL).append("POST 방식 데이터 담기");

                List<String> items = Collections.<String>list(req.getParameterNames());

                sb.append(NL).append("POST SIZE [" + items.size() + "]");

                for(String parameterName : items) {
                    isFieldExist = isFieldExist(parameterName, fields);
                    if(isFieldExist) {
                        Class variableType = targetVo.getClass().getDeclaredField(parameterName).getType();
                        String variableTypeStr = variableType.getName();
                        String setMethodName = "set" + parameterName.substring(0, 1).toUpperCase() + parameterName.substring(1);
                        Method setMethod = null;
                        // 변수가 String인 경우
                        if(variableTypeStr.equals("java.lang.String")) {
                            String value = req.getParameter(parameterName);
                            sb.append(NL).append("param[" + parameterName + "=" + value + "]");
                            setMethod = targetVo.getClass().getMethod(setMethodName, new Class[] { variableType });
                            setMethod.invoke(targetVo, new Object[] { value });
                        } else if(variableTypeStr.equals("[Ljava.lang.String;")) {
                            String[] value = req.getParameterValues(parameterName);
                            sb.append(NL).append("param[" + parameterName + "=" + value + "]");
                            setMethod = targetVo.getClass().getMethod(setMethodName, new Class[] { variableType });
                            setMethod.invoke(targetVo, new Object[] { value });
                        }
                    }
                }

                fields = targetVo.getClass().getSuperclass().getDeclaredFields();

                for(String parameterName : items) {
                    isFieldExist = isFieldExist(parameterName, fields);
                    if(isFieldExist) {
                        Class variableType = targetVo.getClass().getSuperclass().getDeclaredField(parameterName).getType();
                        String variableTypeStr = variableType.getName();
                        String setMethodName = "set" + parameterName.substring(0, 1).toUpperCase() + parameterName.substring(1);
                        Method setMethod = null;
                        // 변수가 String인 경우
                        if(variableTypeStr.equals("java.lang.String")) {
                            String value = req.getParameter(parameterName);
                            sb.append(NL).append("param[" + parameterName + "=" + value + "]");
                            setMethod = targetVo.getClass().getMethod(setMethodName, new Class[] { variableType });
                            setMethod.invoke(targetVo, new Object[] { value });
                        } else if(variableTypeStr.equals("[Ljava.lang.String;")) {
                            String[] value = req.getParameterValues(parameterName);
                            sb.append(NL).append("param[" + parameterName + "=" + value + "]");
                            setMethod = targetVo.getClass().getMethod(setMethodName, new Class[] { variableType });
                            setMethod.invoke(targetVo, new Object[] { value });
                        }
                    }
                }
            } else {

                sb.append(NL).append("GET 방식 데이터 담기");

                String[] params;
                String[] values;
                String name;
                String value;

                params = URLDecoder.decode(query, "UTF-8").split("&");

                for(int i = 0; i < params.length; i++) {
                    values = params[i].split("=");
                    name = values[0];
                    if(values.length == 2) {
                        value = values[1].trim();
                    } else {
                        value = "";
                    }
                    isFieldExist = isFieldExist(name, fields);
                    if(isFieldExist) {
                        sb.append(NL).append("param[" + name + "=" + value + "]");
                        Class variableType = targetVo.getClass().getDeclaredField(name).getType();
                        String variableTypeStr = variableType.getName();
                        String setMethodName = "set" + name.substring(0, 1).toUpperCase() + name.substring(1);
                        Method setMethod = null;
                        // 변수가 String인 경우
                        if(variableTypeStr.equals("java.lang.String")) {
                            setMethod = targetVo.getClass().getMethod(setMethodName, new Class[] { variableType });
                            setMethod.invoke(targetVo, new Object[] { value });
                        } else if(variableTypeStr.equals("[Ljava.lang.String;")) {
                            setMethod = targetVo.getClass().getMethod(setMethodName, new Class[] { variableType });
                            setMethod.invoke(targetVo, new Object[] { value });
                        }
                    }
                }

                fields = targetVo.getClass().getSuperclass().getDeclaredFields();

                for(int i = 0; i < params.length; i++) {
                    values = params[i].split("=");
                    name = values[0];
                    if(values.length == 2) {
                        value = values[1].trim();
                    } else {
                        value = "";
                    }
                    isFieldExist = isFieldExist(name, fields);
                    if(isFieldExist) {
                        sb.append(NL).append("param[" + name + "=" + value + "]");
                        Class variableType = targetVo.getClass().getSuperclass().getDeclaredField(name).getType();
                        String variableTypeStr = variableType.getName();
                        String setMethodName = "set" + name.substring(0, 1).toUpperCase() + name.substring(1);
                        Method setMethod = null;
                        // 변수가 String인 경우
                        if(variableTypeStr.equals("java.lang.String")) {
                            setMethod = targetVo.getClass().getMethod(setMethodName, new Class[] { variableType });
                            setMethod.invoke(targetVo, new Object[] { value });
                        } else if(variableTypeStr.equals("[Ljava.lang.String;")) {
                            setMethod = targetVo.getClass().getMethod(setMethodName, new Class[] { variableType });
                            setMethod.invoke(targetVo, new Object[] { value });
                        }
                    }
                }
            }

            List<String> header = new ArrayList<String>();

            header.add("lang");

            fields = targetVo.getClass().getSuperclass().getDeclaredFields();

            if(header.size() > 0) {
                sb.append(NL).append("header[" + header + "]");
                for(String row : header) {
                    sb.append(NL).append("row[" + row + "]");
                    isFieldExist = isFieldExist(row, fields);
                    sb.append(NL).append("isFieldExist[" + isFieldExist + "]");
                    if(isFieldExist) {

                        List<String> headers = Collections.<String>list(req.getHeaderNames());

                        for(String value : headers) {
                            sb.append(NL).append("[" + value + "=" + req.getHeader(value) + "]");
                            if("accept-language".equals(value.toLowerCase())) {
                                sb.append(NL).append("param[" + row + "=" + req.getHeader(value) + "]");
                                String acceptLang = req.getHeader(value);
                                String[] arrayLang = acceptLang.split(",");
                                String lang = "KR";
                                if(arrayLang != null && arrayLang.length > 0) {
                                    String str = arrayLang[0];
                                    if(str.indexOf("ko") > -1) {
                                        lang = "KR";
                                    } else {
                                        lang = "EN";
                                    }
                                }
                                sb.append(NL).append("lang[" + lang + "]");
                                Class variableType = targetVo.getClass().getSuperclass().getDeclaredField(row).getType();
                                String variableTypeStr = variableType.getName();
                                String setMethodName = "set" + row.substring(0, 1).toUpperCase() + row.substring(1);
                                Method setMethod = null;
                                // 변수가 String인 경우
                                if(variableTypeStr.equals("java.lang.String")) {
                                    sb.append(NL).append("param[" + row + "=" + lang + "]");
                                    setMethod = targetVo.getClass().getMethod(setMethodName, new Class[] { variableType });
                                    setMethod.invoke(targetVo, new Object[] { lang });
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.error("[" + TAG + "]", e);
            e.printStackTrace();
        } finally {
            sb.append(NL).append("[" + TAG + "] ============================== formRequestToVo 종료 ==============================");
            log.info(sb.toString());
            sb.setLength(0);
        }
    }

    public static boolean isFieldExist(String parameterName, Field[] fields) {
        boolean result = false;

        try {

            int i = 0;

            while (i < fields.length) {
                Field field = fields[i];
                if(field.getName().equals(parameterName)) {
                    return true;
                }
                i++;
            }

        } catch (Exception e) {
            System.out.println(" isFieldExist Error : " + e.toString());
            e.printStackTrace();
        }
        return result;
    }
}