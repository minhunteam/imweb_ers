package com.common.module.create.helper;

import org.springframework.util.StopWatch;

import com.common.module.base.BaseDao;
import com.common.module.create.vo.CreateTranCodeVO;

public class CreateTranCodeHelperImpl extends BaseDao implements CreateTranCodeHelper {

    private static boolean createInstanceRunning = false;

    private CreateTranCodeVO proc = new CreateTranCodeVO();

    @Override
    public synchronized Object createProcess(Object param) throws Exception {

        logs = new StringBuilder();

        while (createInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] create Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] create INTERRUPTED!!! error | ", e);
            }
        }

        createInstanceRunning = true;

        try {
            logs.append(NL + "[" + TAG + "] createProcess Start");

            proc = new CreateTranCodeVO();

            proc = (CreateTranCodeVO) param;

            StopWatch watch = new StopWatch();

            watch.start();

            dao.call("ibatis.mysql.common.module.create.trancode.createProcess", proc);

            watch.stop();

            logs.append(NL + "[" + TAG + "] 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] createProcess Error | ", e);
            proc = new CreateTranCodeVO();
            proc.setRescode("9999");
        } finally {
            logs.append(NL + "[" + TAG + "] createProcess End");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            }
        }

        createInstanceRunning = false;

        notifyAll();

        return proc.getSeqNo();
    }
}