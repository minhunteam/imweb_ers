package com.common.module.create.helper;

import org.springframework.util.StopWatch;

import com.common.module.base.BaseDao;
import com.common.module.create.vo.CreateTranKeyVO;

public class CreateTranKeyHelperImpl extends BaseDao implements CreateTranKeyHelper {

    private static boolean createInstanceRunning = false;

    private CreateTranKeyVO proc = new CreateTranKeyVO();

    @Override
    public synchronized Object createProcess(Object param) throws Exception {

        logs = new StringBuilder();

        while (createInstanceRunning) {
            try {
                logs.append(NL + "[" + TAG + "] create Waiting....");
                wait();
            } catch (InterruptedException e) {
                log.error(NL + "[" + TAG + "] create INTERRUPTED!!! error | ", e);
            }
        }

        createInstanceRunning = true;

        try {
            logs.append(NL + "[" + TAG + "] createProcess Start");

            proc = new CreateTranKeyVO();
            proc = (CreateTranKeyVO) param;

            StopWatch watch = new StopWatch();

            watch.start();

            dao.call("ibatis.mysql.common.module.create.trankey.createProcess", proc);

            watch.stop();

            logs.append(NL + "[" + TAG + "] 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] createProcess Error | ", e);
            proc = new CreateTranKeyVO();
            proc.setRescode("9999");
        } finally {
            logs.append(NL + "[" + TAG + "] createProcess End");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            }
        }

        createInstanceRunning = false;

        notifyAll();

        return proc.getSeqNo();
    }
}