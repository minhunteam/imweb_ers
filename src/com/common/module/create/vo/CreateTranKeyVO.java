package com.common.module.create.vo;

import com.common.module.base.BaseVO;

public class CreateTranKeyVO extends BaseVO {

    private String seqType = "";
    private String seqCode = "";
    private String seqNum  = "";
    private String seqLen  = "";
    private String seqNo   = "";

    public String getSeqType() {
        return seqType;
    }

    public void setSeqType(String seqType) {
        this.seqType = seqType;
    }

    public String getSeqCode() {
        return seqCode;
    }

    public void setSeqCode(String seqCode) {
        this.seqCode = seqCode;
    }

    public String getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(String seqNum) {
        this.seqNum = seqNum;
    }

    public void setSeqNum(int seqNum) {
        this.seqNum = Integer.toString(seqNum);
    }

    public String getSeqLen() {
        return seqLen;
    }

    public void setSeqLen(String seqLen) {
        this.seqLen = seqLen;
    }

    public void setSeqLen(int seqLen) {
        this.seqLen = Integer.toString(seqLen);
    }

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }
}