package com.common.module.logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerManager {

    private static String className  = "";
    private static String methodName = "";

    public LoggerManager() {
    }

    public void trace(String message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isTraceEnabled()) {
            log.trace("::::::::::: [trace]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.trace(message);
            log.trace("::::::::::: [trace]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void trace(StringBuilder message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isTraceEnabled()) {
            log.trace("::::::::::: [trace]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.trace(message);
            log.trace("::::::::::: [trace]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void debug(String message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isDebugEnabled() || log.isTraceEnabled()) {
            log.debug("::::::::::: [debug]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.debug(message);
            log.debug("::::::::::: [debug]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void debug(StringBuilder message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isDebugEnabled() || log.isTraceEnabled()) {
            log.debug("::::::::::: [debug]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.debug(message);
            log.debug("::::::::::: [debug]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void info(String message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isInfoEnabled() || log.isDebugEnabled() || log.isTraceEnabled()) {
            log.info("::::::::::: [info]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.info(message);
            log.info("::::::::::: [info]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void info(StringBuilder message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isInfoEnabled() || log.isDebugEnabled() || log.isTraceEnabled()) {
            log.info("::::::::::: [info]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.info(message);
            log.info("::::::::::: [info]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void warn(String message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isInfoEnabled() || log.isDebugEnabled() || log.isTraceEnabled()) {
            log.warn("::::::::::: [warn]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.warn(message);
            log.warn("::::::::::: [warn]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void warn(StringBuilder message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isInfoEnabled() || log.isDebugEnabled() || log.isTraceEnabled()) {
            log.warn("::::::::::: [warn]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.warn(message);
            log.warn("::::::::::: [warn]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void error(String message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isInfoEnabled() || log.isDebugEnabled() || log.isTraceEnabled()) {
            log.error("::::::::::: [error]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.error(message);
            log.error("::::::::::: [error]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void error(StringBuilder message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isInfoEnabled() || log.isDebugEnabled() || log.isTraceEnabled()) {
            log.error("::::::::::: [error]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.error(message);
            log.error("::::::::::: [error]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void error(String message, Throwable t) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isInfoEnabled() || log.isDebugEnabled() || log.isTraceEnabled()) {
            log.error("::::::::::: [error]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.error(message, t);
            log.error("::::::::::: [error]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void error(StringBuilder message, Throwable t) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isInfoEnabled() || log.isDebugEnabled() || log.isTraceEnabled()) {
            log.error("::::::::::: [error]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.error(message, t);
            log.error("::::::::::: [error]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void fatal(String message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isInfoEnabled() || log.isDebugEnabled() || log.isTraceEnabled()) {
            log.fatal("::::::::::: [fatal]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.fatal(message);
            log.fatal("::::::::::: [fatal]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }

    public void fatal(StringBuilder message) {
        Exception e = new Exception();
        StackTraceElement element = e.getStackTrace()[1];
        className = element.getFileName().replace(".java", "");
        methodName = element.getMethodName();
        Logger log = LogManager.getLogger(className + ".class");
        if(log.isInfoEnabled() || log.isDebugEnabled() || log.isTraceEnabled()) {
            log.fatal("::::::::::: [fatal]" + "className[" + className + "] methodName[" + methodName + "()] 시작! :::::::::::");
            log.fatal(message);
            log.fatal("::::::::::: [fatal]" + "className[" + className + "] methodName[" + methodName + "()] 종료! :::::::::::");
        }
    }
}