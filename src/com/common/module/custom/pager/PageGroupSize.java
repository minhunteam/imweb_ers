package com.common.module.custom.pager;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class PageGroupSize extends BodyTagSupport {

    /**
     * pageGroupSize 태그 : 현재 섹터.
     */
    private static final long serialVersionUID = 4908532937956094472L;

    public int doAfterBody() throws JspTagException {
        PageNavigation paging = (PageNavigation) findAncestorWithClass(this, PageNavigation.class);
        if(paging == null) {
            throw new JspTagException("pageGroupSize는 PageNavigation안에 있어야 됩니다.");
        }
        String pageGroupSize = getBodyContent().getString().trim();
        paging.setPageGroupSize(pageGroupSize);
        return SKIP_BODY;
    }
}