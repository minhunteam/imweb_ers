package com.common.module.custom.pager;

import java.io.IOException;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;

import com.common.module.util.PropertiesUtil;

public class PageNavigation extends TagSupport {

    private static final long serialVersionUID = -2655449878116830914L;

    private final static String NL = System.getProperty("line.separator", "\n");

    private int    pageNo          = 1;                                         // 페이지번호
    private int    pageMax         = 10;                                        // 한 페이지에서 출력할 컨텐츠 갯수
    private String pageType        = "W";                                       // 페이지징 타입
    private int    totalPages      = 1;                                         // 전체페이지
    private int    totalRecords    = 1;                                         // 전체레코드수
    public boolean outermax        = false;                                     // 페이지사이즈변경여부
    public boolean forExcel        = false;                                     // EXCEL 출력여부
    private int    pageGroupSize   = 10;                                        // 그룹사이즈
    private int    halfPageSector  = Math.round(this.pageGroupSize / 2);
    private int    startNo         = 1;
    private int    endNo           = 10;
    private int    startPageNo     = 1;                                         // 시작페이지 번호
    private int    endPageNo       = 1;                                         // 마지막페이지 번호
    private int    pagecnt         = 1;                                         // 페이지 카운터
    private int    i               = 0;
    private String startPage       = "|<";                                      // 맨 처음 시작페이지
    private String endPage         = ">|";                                      // 맨 끝 페이지
    private String beforeGroup     = "<<";                                      // 이전 그룹 페이지
    private String nextGroup       = ">>";                                      // 다음 그룹 페이지
    private String beforePage      = "◀";                                       // 이전 페이지
    private String nextPage        = "▶";                                       // 다음 페이지
    private String pageCurrent     = "<li><span class=\"on\">:no:</span></li>"; // 페이지 구분자
    private String pageSeperator   = "|";                                       // 페이지 구분자
    private String pagerProperties = "/pager.properties";
    private String method          = "POST";                                    // 링크사용방식
    private String paramKey        = "action";
    private String paramValue      = "list";
    private String submitFormName  = "frm";
    private String cmdName         = "list";

    public PageNavigation() {
        try {
            this.pageMax = Integer.parseInt((new PropertiesUtil(pagerProperties)).get("pager.maxlength.default"));
        } catch (Exception e) {
        }
        initProperties();
    }

    public PageNavigation(String pageNo) {
        try {
            this.pageMax = Integer.parseInt((new PropertiesUtil(pagerProperties)).get("pager.maxlength.default"));;
        } catch (Exception e) {
        }
        if(pageNo != null && !"".equals(pageNo)) {
            this.setPageNo(Integer.parseInt(pageNo));
        }
        initProperties();
    }

    public PageNavigation(String pageMax, String pageNo) {
        if(pageMax != null && !"".equals(pageMax)) {
            this.setPageMax(Integer.parseInt(pageMax));
            this.outermax = true;
        }
        if(pageNo != null && !"".equals(pageNo)) {
            this.setPageNo(Integer.parseInt(pageNo));
        }
        initProperties();
    }

    public void initProperties() {
        try {
            this.pageGroupSize = Integer.parseInt((new PropertiesUtil(pagerProperties)).get("pager.groupsize.default"));;
        } catch (Exception e) {
        }
        this.setStartNo((this.pageNo - 1) * this.pageMax + 1);
        this.setEndNo(this.pageNo * this.pageMax);
        initIcon(pagerProperties);
    }

    public void initIcon(String iconProperties) {
        PropertiesUtil conf = new PropertiesUtil(iconProperties);
        try {
            this.startPage = conf.get("pager.icon.pageFirst");
            this.endPage = conf.get("pager.icon.pageLast");
            this.beforeGroup = conf.get("pager.icon.sectPrev");
            this.nextGroup = conf.get("pager.icon.sectNext");
            this.beforePage = conf.get("pager.icon.pagePrev");
            this.nextPage = conf.get("pager.icon.pageNext");
            this.pageCurrent = conf.get("pager.icon.pageCurrent");
            this.pageSeperator = conf.get("pager.icon.pageSeperator");
        } catch (Exception e) {
        }
    }

    /**
     * 태그 시작처리
     */
    public int doStartTag() {
        return EVAL_BODY_INCLUDE;
    }

    /**
     * 태그 종료처리
     */
    public int doEndTag() {
        if(this.getMethod().equals("GET")) {
            return doEndTag_GET();
        } else {
            return doEndTag_POST();
        }
    }

    public void pageInit() {
        totalPages = (int) Math.ceil((totalRecords * 1.0) / (pageMax * 1.0));
        if(totalPages > 0) {
            startPageNo = ((pageNo - 1) / pageGroupSize) * pageGroupSize + 1;
            if(startPageNo <= 0) {
                startPageNo = 1;
                endPageNo = pageGroupSize;
            } else {
                endPageNo = (((pageNo - 1) / pageGroupSize) * pageGroupSize) + pageGroupSize + 1;
                pagecnt = endPageNo;
            }
            if(endPageNo >= totalPages) {
                if(endPageNo == totalPages) {
                    endPageNo = totalPages;
                    pagecnt = totalPages - 1;
                } else {
                    endPageNo = totalPages;
                    pagecnt = totalPages;
                }
            } else {
                pagecnt = endPageNo - 1;
            }
        }
    }

    public int doEndTag_GET() {
        this.pageInit();
        return EVAL_PAGE;
    }

    public int doEndTag_POST() {
        this.pageInit();
        StringBuilder headPageNavi = new StringBuilder();
        StringBuilder bodyNavi = new StringBuilder();
        StringBuilder footPageNavi = new StringBuilder();
        StringBuilder returnNavi = new StringBuilder();
        String linkString = "<li><a href=\"javascript:goPage('!')\" onfocus=\"blur();\"><span class=\"off\">:no:</span></a></li>";
        String linkImgString = "";
        String buttonStr = "";
        if("W".equals(pageType)) {
            linkImgString = "<li><a href=\"javascript:goPage('!')\" onfocus=\"blur();\">:no:</a></li>";
        }

        if(totalPages > 0) {
            i = pageNo - pageGroupSize;
            if("W".equals(pageType)) {
                headPageNavi.append(this.parseLinkStr(linkImgString.replaceAll("!", Integer.toString(1)), startPage, pageNo > 1));
                headPageNavi.append(this.parseLinkStr(linkImgString.replaceAll("!", Integer.toString(i)), beforeGroup, i > 1));
                headPageNavi.append(this.parseLinkStr(linkImgString.replaceAll("!", Integer.toString(pageNo - 1)), beforePage, pageNo > 1));
            } else {
                if(pageNo > 1) {
                    linkImgString = "<input type=\"button\" value=\"이전페이지\" class=\"btn01\" onclick=\"javascript:goPage('!')\" style=\"cursor: pointer;cursor: hand;\"/>";
                    buttonStr = "<input type=\"button\" value=\"이전페이지\" class=\"btn01\" onclick=\"javascript:goPage('!')\"/>";
                } else {
                    linkImgString = "<input type=\"button\" value=\"이전페이지\" class=\"btn01\" />";
                    buttonStr = "<input type=\"button\" value=\"이전페이지\" class=\"btn01\" />";
                }
                headPageNavi.append(this.parseLinkStr(linkImgString.replaceAll("!", Integer.toString(pageNo - 1)), buttonStr, pageNo > 1));
            }
            for(int i = startPageNo; i <= pagecnt; i++) {
                bodyNavi.append(pageSeperator).append("&nbsp;");
                if(i == pageNo) {
                    if("M".equals(pageType)) {
                        pageCurrent = "<span style=\"display: none;\">:no:</span>"; // 페이지 구분자
                    }
                    bodyNavi.append(pageCurrent.replaceAll(":no:", Integer.toString(i)));
                } else {
                    bodyNavi.append(this.parseLinkStr(linkString.replaceAll("!", Integer.toString(i)), Integer.toString(i), true));
                }
            }
            if("W".equals(pageType)) {
                footPageNavi.append(this.parseLinkStr(linkImgString.replaceAll("!", Integer.toString(pageNo + 1)), nextPage, endPageNo > pageNo));
                footPageNavi.append(this.parseLinkStr(linkImgString.replaceAll("!", Integer.toString(pageNo + pageGroupSize)), nextGroup, (pageNo + pageGroupSize <= totalPages)));
                footPageNavi.append(this.parseLinkStr(linkImgString.replaceAll("!", Integer.toString(totalPages)), endPage, pageNo < totalPages));
            } else {
                if(endPageNo > pageNo) {
                    linkImgString = "<input type=\"button\" value=\"다음페이지\" class=\"btn01\" onclick=\"javascript:goPage('!')\" style=\"cursor: pointer;cursor: hand;\"/>";
                    buttonStr = "<input type=\"button\" value=\"다음페이지\" class=\"btn01\" onclick=\"javascript:goPage('!')\" style=\"cursor: pointer;cursor: hand;\"/>";
                } else {
                    linkImgString = "<input type=\"button\" value=\"다음페이지\" class=\"btn01\" />";
                    buttonStr = "<input type=\"button\" value=\"다음페이지\" class=\"btn01\" />";
                }
                footPageNavi.append(this.parseLinkStr(linkImgString.replaceAll("!", Integer.toString(pageNo + 1)), buttonStr, endPageNo > pageNo));
            }

            returnNavi.append(headPageNavi);
            returnNavi.append(bodyNavi);
            returnNavi.append(footPageNavi);
            returnNavi.append(NL + "\t <script language=\"javascript\" type=\"text/javascript\">");
            returnNavi.append(NL + "\t     goPage=function(num){");
            returnNavi.append(NL + "\t         var frm = document.getElementById(\"" + this.submitFormName + "\");");
            returnNavi.append(NL + "\t             frm.method.value = \"" + this.cmdName + "\";");
            returnNavi.append(NL + "\t             frm.rowno.value = num;");
            returnNavi.append(NL + "\t             frm." + this.paramKey + "=\"" + this.paramValue + ".do\";");
            returnNavi.append(NL + "\t             frm.submit();");
            returnNavi.append(NL + "\t     }");
            returnNavi.append(NL + "\t </script>");
        } else {
            returnNavi.append("");
        }
        JspWriter out = pageContext.getOut();
        try {
            out.print(returnNavi.toString());
        } catch (IOException e) {
            System.out.println("doEndTag_POST()" + e);
        }
        return EVAL_PAGE;
    }

    private String parseLinkStr(String tag, String icon, boolean link) {
        if(!icon.equals("")) {
            return link ? tag.replaceAll(":no:", icon) : icon;
        } else {
            return "";
        }
    }

    /**
     * 페이지번호 세팅
     * 
     * @param pageNo 페이지번호
     */
    public void setPageNo(String pageNo) {
        if(!StringUtils.isBlank(pageNo))
            this.pageNo = Integer.parseInt(pageNo);
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageNo() {
        return pageNo;
    }

    /**
     * 페이지당 출력물수 설정
     * 
     * @param pageMax 페이지당출력물수
     */
    public void setPageMax(String pageMax) {
        if(!StringUtils.isBlank(pageMax))
            this.pageMax = Integer.parseInt(pageMax);
    }

    public void setPageMax(int pageMax) {
        this.pageMax = pageMax;
    }

    public int getPageMax() {
        return pageMax;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = Integer.parseInt(totalPages);
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalRecords(String totalRecords) {
        if(!StringUtils.isBlank(totalRecords)) {
            this.totalRecords = Integer.parseInt(totalRecords) == 0 ? 1 : Integer.parseInt(totalRecords);
            if(this.forExcel) {
                this.pageMax = Integer.parseInt(totalRecords) > 65000 ? 65000 : Integer.parseInt(totalRecords);
                this.pageNo = 1;
                this.startNo = (this.pageNo - 1) * this.pageMax + 1;
                this.endNo = this.pageNo * this.pageMax;
            }
        }
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public boolean isForExcel() {
        return forExcel;
    }

    public void setForExcel(boolean forExcel) {
        this.forExcel = forExcel;
    }

    /**
     * 페이지그룹 설정
     * 
     * @param pageGroupSize 페이지그룹 설정
     */
    public void setPageGroupSize(String pageGroupSize) {
        this.pageGroupSize = Integer.parseInt(pageGroupSize);
        if(this.pageGroupSize <= 0) {
            this.pageGroupSize = 10;
        }
    }

    public void setPageGroupSize(int pageGroupSize) {
        this.pageGroupSize = pageGroupSize;
    }

    public int getPageGroupSize() {
        return pageGroupSize;
    }

    public void setStartNo(int startNo) {
        this.startNo = startNo;
    }

    public int getStartNo() {
        return startNo;
    }

    public void setEndNo(int endNo) {
        this.endNo = endNo;
    }

    public int getEndNo() {
        return endNo;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public String getSubmitFormName() {
        return submitFormName;
    }

    public void setSubmitFormName(String submitFormName) {
        this.submitFormName = submitFormName;
    }

    public String getCmdName() {
        return cmdName;
    }

    public void setCmdName(String cmdName) {
        this.cmdName = cmdName;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }
}