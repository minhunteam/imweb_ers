package com.common.module.custom.pager;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class PageMax extends BodyTagSupport {

    /**
     * 페이지당 출력물수
     */
    private static final long serialVersionUID = 4908532937956094472L;

    public int doAfterBody() throws JspTagException {
        PageNavigation paging = (PageNavigation) findAncestorWithClass(this, PageNavigation.class);
        if(paging == null) {
            throw new JspTagException("pageMax PageNavigation안에 있어야 됩니다.");
        }
        String pageMax = getBodyContent().getString().trim();
        paging.setPageMax(pageMax);
        return SKIP_BODY;
    }
}