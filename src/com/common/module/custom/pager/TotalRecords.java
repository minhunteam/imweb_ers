package com.common.module.custom.pager;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class TotalRecords extends BodyTagSupport {

    /**
     * totalRecords 태그; 총 레코드수
     */
    private static final long serialVersionUID = 4908532937956094472L;

    public int doAfterBody() throws JspTagException {
        PageNavigation paging = (PageNavigation) findAncestorWithClass(this, PageNavigation.class);
        if(paging == null) {
            throw new JspTagException("TotalRecords PageNavigation안에 있어야 됩니다.");
        }
        String totalRecords = getBodyContent().getString().trim();
        paging.setTotalRecords(totalRecords);
        return SKIP_BODY;
    }
}