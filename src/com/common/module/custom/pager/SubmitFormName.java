package com.common.module.custom.pager;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class SubmitFormName extends BodyTagSupport {

    private static final long serialVersionUID = -7271425996523593032L;

    /**
     * pageMax 태그; 페이지당 게시물 수를 받아들인다.
     */

    public int doAfterBody() throws JspTagException {
        PageNavigation paging = (PageNavigation) findAncestorWithClass(this, PageNavigation.class);
        if(paging == null) {
            throw new JspTagException("SubmitFormName는 PageNavigation안에 있어야 됩니다.");
        }
        String submitFormName = getBodyContent().getString().trim();
        paging.setCmdName(submitFormName);
        return SKIP_BODY;
    }
}