package com.common.module.custom.pager;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class CmdName extends BodyTagSupport {

    /**
     * pageMax 태그; 페이지당 게시물 수를 받아들인다.
     */
    private static final long serialVersionUID = 4908532937956094472L;

    public int doAfterBody() throws JspTagException {
        PageNavigation paging = (PageNavigation) findAncestorWithClass(this, PageNavigation.class);
        if(paging == null) {
            throw new JspTagException("cmdName는 PageNavigation안에 있어야 됩니다.");
        }
        String cmdName = getBodyContent().getString().trim();
        paging.setCmdName(cmdName);
        return SKIP_BODY;
    }
}