package com.common.module.custom.pager;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class PageNo extends BodyTagSupport {

    /**
     * pageMax 태그; 페이지당 게시물 수를 받아들인다.
     */
    private static final long serialVersionUID = 4908532937956094472L;

    public int doAfterBody() throws JspTagException {
        PageNavigation paging = (PageNavigation) findAncestorWithClass(this, PageNavigation.class);
        if(paging == null) {
            throw new JspTagException("pageNo는 PageNavigation안에 있어야 됩니다.");
        }
        String pageNo = getBodyContent().getString().trim();
        paging.setPageNo(pageNo);
        return SKIP_BODY;
    }
}