package com.common.module.custom.tag;

import java.io.IOException;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class StringCutting extends TagSupport {

    private int    maxlen = 20;
    private String value  = "";
    private String method = "POST"; // 링크사용방식

    public StringCutting() {
    }

    /**
     * 태그 시작처리
     */
    public int doStartTag() {
        return EVAL_BODY_INCLUDE;
    }

    /**
     * 태그 종료처리
     */
    public int doEndTag() {
        if(this.getMethod().equals("GET")) {
            return doEndTag_GET();
        } else {
            return doEndTag_POST();
        }
    }

    public int doEndTag_GET() {
        return EVAL_PAGE;
    }

    public int doEndTag_POST() {
        StringBuilder strcut = new StringBuilder();
        String str = "";
        try {
            if(!"".equals(value) || value != null) {
                byte[] byteString = value.getBytes();
                int length = maxlen;

                if(byteString.length <= length) {
                    str = new String(byteString);
                } else {
                    int minusByteCount = 0;
                    for(int i = 0; i < length; i++) {
                        minusByteCount += (byteString[i] < 0) ? 1 : 0;
                    }
                    if(minusByteCount % 2 != 0) {
                        length--;
                    }
                    str = new String(byteString, 0, length) + "...";
                }
            }
            strcut.append(str);
            JspWriter out = pageContext.getOut();
            out.print(strcut.toString());
        } catch (IOException e) {
            System.out.println("doEndTag_POST()" + e);
        }
        return EVAL_PAGE;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }

    public int getMaxlen() {
        return maxlen;
    }

    public void setMaxlen(int maxlen) {
        this.maxlen = maxlen;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}