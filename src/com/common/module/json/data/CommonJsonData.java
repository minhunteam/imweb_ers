package com.common.module.json.data;

import com.common.module.base.BaseJsonData;

public class CommonJsonData extends BaseJsonData {

    private String result;

    public CommonJsonData() {
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}