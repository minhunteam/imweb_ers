package com.common.module.email.dao;

import org.springframework.util.StopWatch;

import com.common.module.base.BaseDao;

public class EmailParserDaoImpl extends BaseDao implements EmailParserDao {

    private static final long serialVersionUID = 4219042344996256991L;

    @Override
    public Object checkProcess(Object param) throws Exception {

        Object result = null;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] checkProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            result = dao.select("ibatis.mysql.imweb.ers.store.item.checkProcess", param);

            watch.stop();

            logs.append(NL + "[" + TAG + "] checkProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] checkProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] checkProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return result;
    }

    @Override
    public Object saveProcess(Object param) throws Exception {
        int i = 0;

        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] saveProcess START");

            StopWatch watch = new StopWatch();

            watch.start();

            i = dao.save("ibatis.mysql.imweb.ers.store.item.updateProcess", "ibatis.mysql.imweb.ers.store.item.insertProcess", param);

            if(i == 0) {
                returnCode = "0001";
            } else if(i == -1) {
                returnCode = "9999";
            } else {
                returnCode = "0000";
            }

            watch.stop();

            logs.append(NL + "[" + TAG + "] saveProcess 실행시간[" + (watch.getTotalTimeMillis() / 1000.0) + " ms]");

        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] saveProcess Error | ", e);
            returnCode = "9999";
        } finally {
            logs.append(NL + "[" + TAG + "] saveProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }

        return returnCode;
    }
}