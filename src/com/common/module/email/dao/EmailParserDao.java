package com.common.module.email.dao;

public interface EmailParserDao {
    public Object checkProcess(Object param) throws Exception;

    public Object saveProcess(Object param) throws Exception;
}