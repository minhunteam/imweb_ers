package com.common.module.email.vo;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class EmailParserVO implements Serializable {

    private static final long serialVersionUID = 6519443964362500602L;

    private String orderno       = "";
    private String orderdate     = "";
    private String ordertime     = "";
    private String orderdatetime = "";
    private String cnt           = "";
    private String storeitemcode = "";
    private String storecode     = "";
    private String storeitemname = "";
    private String itemsize      = "";
    private String qty           = "1";
    private String val           = "";

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(String orderdate) {
        this.orderdate = orderdate;
    }

    public String getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(String ordertime) {
        this.ordertime = ordertime;
    }

    public String getOrderdatetime() {
        return orderdatetime;
    }

    public void setOrderdatetime(String orderdatetime) {
        this.orderdatetime = orderdatetime;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = Integer.toString(cnt);
    }

    public String getStoreitemcode() {
        return storeitemcode;
    }

    public void setStoreitemcode(String storeitemcode) {
        this.storeitemcode = storeitemcode;
    }

    public String getStorecode() {
        return storecode;
    }

    public void setStorecode(String storecode) {
        this.storecode = storecode;
    }

    public String getStoreitemname() {
        return storeitemname;
    }

    public void setStoreitemname(String storeitemname) {
        this.storeitemname = storeitemname;
    }

    public String getItemsize() {
        return itemsize;
    }

    public void setItemsize(String itemsize) {
        this.itemsize = itemsize;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}