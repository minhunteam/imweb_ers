package com.common.module.email.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.common.module.email.task.EmailParserTask;
import com.common.module.session.task.SessionTask;

public class EmailParserJob extends QuartzJobBean {

    private EmailParserTask emailParserTask;

    public void setEmailParserTask(EmailParserTask emailParserTask) {
        this.emailParserTask = emailParserTask;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        try {
            emailParserTask.check();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}