package com.common.module.email.task;

import com.common.module.base.BaseTask;
import com.common.module.email.service.EmailParserService;

public class EmailParserTask extends BaseTask {

    private EmailParserService emailParserService;

    public void setEmailParserService(EmailParserService emailParserService) {
        this.emailParserService = emailParserService;
    }

    public void check() throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] check START");
            emailParserService.parserProcess();
        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] check Error | ", e);
        } finally {
            logs.append(NL + "[" + TAG + "] check END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            }
        }
    }
}