package com.common.module.email.service;

public interface EmailParserService {
    public void parserProcess() throws Exception;
}