package com.common.module.email.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.common.module.base.BaseService;
import com.common.module.create.helper.CreateTranCodeHelper;
import com.common.module.create.helper.CreateTranKeyHelper;
import com.common.module.create.vo.CreateTranCodeVO;
import com.common.module.create.vo.CreateTranKeyVO;
import com.common.module.email.dao.EmailParserDao;
import com.common.module.email.protocol.IMAPAgent;
import com.common.module.email.vo.EmailParserVO;
import com.common.module.transaction.TransactionHelper;
import com.imweb.ers.store.item.vo.StoreItemVO;
import com.imweb.ers.store.order.dao.StoreOrderDetailDao;
import com.imweb.ers.store.order.vo.StoreOrderDetailVO;

public class EmailParserServiceImpl extends BaseService implements EmailParserService {

    private TransactionHelper transactionHelper;

    private CreateTranKeyHelper createTranKeyHelper;

    private CreateTranCodeHelper createTranCodeHelper;

    private EmailParserDao emailParserDao;

    private StoreOrderDetailDao storeOrderDetailDao;

    public void setStoreOrderDetailDao(StoreOrderDetailDao storeOrderDetailDao) {
        this.storeOrderDetailDao = storeOrderDetailDao;
    }

    public void setEmailParserDao(EmailParserDao emailParserDao) {
        this.emailParserDao = emailParserDao;
    }

    public void setTransactionHelper(TransactionHelper transactionHelper) {
        this.transactionHelper = transactionHelper;
    }

    public void setCreateTranKeyHelper(CreateTranKeyHelper createTranKeyHelper) {
        this.createTranKeyHelper = createTranKeyHelper;
    }

    public void setCreateTranCodeHelper(CreateTranCodeHelper createTranCodeHelper) {
        this.createTranCodeHelper = createTranCodeHelper;
    }

    @Override
    public void parserProcess() throws Exception {
        try {
            logs = new StringBuilder();
            logs.append(NL + "[" + TAG + "] parserProcess START");

            IMAPAgent mailagent = null;

            if("192.168.0.250".equals(getIpAddress()) || "192.168.1.2".equals(getIpAddress())) {
                logs.append(NL + "[" + TAG + "] 테스트 이메일");
                mailagent = new IMAPAgent("imap.gmail.com", "black.desert.sayaka@gmail.com", "01066285684");
            } else {
                logs.append(NL + "[" + TAG + "] 파싱용 이메일");
                mailagent = new IMAPAgent("imap.gmail.com", "timeorderkorea@gmail.com", "time1004!");
            }

            mailagent.open();

            // mailagent.createFolder("CleanFolder");

            File file = new File("/source/log/order.txt");

            if(file.exists()) {
                file.mkdirs();
            }

            FileWriter writer = null;

            Message[] msg = mailagent.getUnReadMessages();

            int i = 0;
            int j = 0;
            int cnt = 0;

            StringBuilder sb;

            // List<Message> items = new ArrayList<Message>();

            List<EmailParserVO> items = new ArrayList();

            for(Message m : msg) {
                String sub = checkSubject(m.getSubject());
                // System.out.println("isEmpty[" + sub + "]");
                if(sub.isEmpty() == false) {
//                    System.out.println(NL + "cnt[" + cnt + "]");

                    String html = m.getContent().toString();

                    // System.out.println("html[" + NL + html + NL + "]");

                    Document doc = Jsoup.parse(html);

                    Elements order = doc.select("[style=font-size:14px;]");
                    Elements item = doc.select("[style^=font-size:14px; vertical-align: top]");
                    Elements qty = doc.select("[style^=font-size:14px; border-bottom: 1px solid rgba(0,0,0,0.1); padding-bottom: 10px; text-align:right;]");

                    i = 0;

                    sb = new StringBuilder();

                    if(order != null) {
//                        System.out.println("text(1) order [" + order.get(1).text() + "]");
//                        System.out.println("text(2) order ConvertDateTimeFormat [" + ConvertDateTimeFormat(order.get(2).text()) + "]");
//                        System.out.println("text(2) order ConvertYMDFormat [" + ConvertYMDFormat(order.get(2).text()) + "]");
//                        System.out.println("text(2) order ConvertTimeFormat [" + ConvertTimeFormat(order.get(2).text()) + "]");
                    }

//                    logs.append(NL + "[" + TAG + "] html [" + html + "]");

                    if(item == null) {
                        mailagent.setUnSeenFlag(m);
                    } else {

                        for(Element s : item) {

                            int f = item.indexOf(s);

                            List<EmailParserVO> rows = new ArrayList();

                            rows = (ArrayList) checkItem(s.text().toString(), qty.get(f).text().toString());

                            logs.append(NL + "[" + TAG + "] item [").append(s.text()).append("] rows.size()  [" + rows.size() + "]").append("] qty  [" + qty.get(f).text() + "]");

                            for(EmailParserVO or : rows) {

                                StoreItemVO search = new StoreItemVO();

//                                logs.append(NL + "[" + TAG + "] StoreItemVO search [" + search + "]");

                                search.setStorecode(or.getStorecode());
                                search.setStoreitemname(or.getStoreitemname());
                                search.setItemsize(or.getItemsize());

                                StoreItemVO val = (StoreItemVO) emailParserDao.checkProcess(search);

//                                logs.append(NL + "[" + TAG + "] val [" + val.getStoreitemcode() + "]");

                                if(val == null) {

                                    CreateTranCodeVO key = new CreateTranCodeVO();

                                    key.setSeqType("SP");
                                    key.setSeqLen(8);

                                    String itemcode = (String) createTranCodeHelper.createProcess(key);

                                    or.setStoreitemcode(itemcode);

                                    StoreItemVO row = new StoreItemVO();

                                    row.setStoreitemcode(itemcode);
                                    row.setStoreitemname(or.getStoreitemname());
                                    row.setStorecode(or.getStorecode());
                                    row.setItemsize(or.getItemsize());

                                    transactionHelper.start();

                                    String returnCode = (String) emailParserDao.saveProcess(row);

                                    if("0000".equals(returnCode)) {
                                        transactionHelper.commit();
                                    }

                                    transactionHelper.end();

                                } else {
                                    or.setStoreitemcode(val.getStoreitemcode());
                                }

//                                logs.append(NL + "[" + TAG + "] or.getStoreitemcode() [" + or.getStoreitemcode() + "]");

                                or.setOrderno(order.get(1).text());
                                or.setOrderdate(ConvertYMDFormat(order.get(2).text()));
                                or.setOrdertime(ConvertTimeFormat(order.get(2).text()));
                                or.setOrderdatetime(ConvertDateTimeFormat(order.get(2).text()));
                                or.setVal(s.text());
                                items.add(or);
                            }
                        }

//                        mailagent.setUnSeenFlag(m);

                        if(j == 99) {

                            /**
                             * <pre>
                             * sb.append(items.toString()).append(NL);
                             * 
                             * if(sb.toString().isEmpty() == false) {
                             *     try {
                             *         // 기존 파일의 내용에 이어서 쓰려면 true를, 기존 내용을 없애고 새로 쓰려면
                             *         // false를
                             *         // 지정한다.
                             *         writer = new FileWriter(file, true);
                             *         writer.write(sb.toString());
                             *         writer.flush();
                             *     } catch (IOException ie) {
                             *         ie.printStackTrace();
                             *     } finally {
                             *         try {
                             *             if(writer != null) {
                             *                 writer.close();
                             *             }
                             *         } catch (IOException ie) {
                             *             ie.printStackTrace();
                             *         }
                             *     }
                             * }
                             * </pre>
                             */
                            break;
                        } else {
                            j++;
                        }
                    }

                }
                cnt++;
            }

            mailagent.close();

            StoreOrderDetailVO row = null;

            CreateTranKeyVO key = null;

            List rows = new ArrayList();

            for(EmailParserVO p : items) {

                int r = items.indexOf(p);

//                logs.append(NL + "[" + TAG + "] r [" + r + "]");

                key = new CreateTranKeyVO();
                key.setSeqType("OS");
                key.setSeqCode(p.getStorecode());
                key.setSeqLen(6);

                String subordercode = (String) createTranKeyHelper.createProcess(key);

                row = new StoreOrderDetailVO();

                row.setOrdercode(p.getOrderno());
                row.setSubordercode(subordercode);
                row.setStorecode(p.getStorecode());
                row.setStoreitemcode(p.getStoreitemcode());
                row.setQty(p.getQty().replaceAll("개", ""));
                row.setDescription(p.getVal());
                row.setCreatedate(p.getOrderdate());
                row.setCreatetime(p.getOrdertime());
                row.setCreatedatetime(p.getOrderdatetime());

                rows.add(row);

                transactionHelper.start();

                String returnCode = (String) storeOrderDetailDao.insertProcess(row);

                if("0000".equals(returnCode)) {
                    transactionHelper.commit();
                }

                transactionHelper.end();

            }

            logs.append(NL + "[" + TAG + "] rows [" + rows.size() + "]");

            logs.append(NL + "[" + TAG + "] items [" + items.size() + "]");

//            logs.append(NL + "[" + TAG + "] items [" + items.toString() + "]");

            System.out.println("Finish");
        } catch (Exception e) {
            log.error(NL + "[" + TAG + "] parserProcess Error | ", e);
            transactionHelper.end();
        } finally {
            logs.append(NL + "[" + TAG + "] parserProcess END");
            if(isDevMode()) {
                log.info(logs);
                logs.setLength(0);
            } else {
                logs.setLength(0);
            }
        }
    }

    public static String checkSubject(String sub) throws Exception {

        String str = "";
        Pattern p;
        Matcher m;
        StringBuilder sb;

        sb = new StringBuilder();

        sb.append("(^[\\[\\]타임오더]+[\\s][0-9]+[\\s]주문에 대한 결제가 완료되었습니다.)");

        p = Pattern.compile(sb.toString());

        m = p.matcher(sub);

        while (m.find()) {
            if(m.groupCount() == 1) {
                str = m.group();
            } else {
                str = "";
            }
        }

        sb = new StringBuilder();

        sb.append("(^[\\[\\]타임오더]+[\\s]주문이 접수되어 입금 확인이 필요합니다.)");

        p = Pattern.compile(sb.toString());

        m = p.matcher(sub);

        while (m.find()) {
            if(m.groupCount() == 1) {
                str = m.group();
            } else {
                str = "";
            }
        }

        return str;
    }

    public static Object checkItem(String sub, String qty) throws Exception {

        List items = new ArrayList();

        String str = "";
        Pattern p;
        Pattern pp;
        Pattern ppp;
        Matcher m;
        Matcher mm;
        Matcher mmm;
        StringBuilder sb;
        StringBuilder sb2;
        StringBuilder sb3;

        sb = new StringBuilder();

        sb.append("(^[0-9가-힣a-zA-Z\\s]+)(([(]{1})([a-zA-Z0-9]+)([)]{1}))()");

        p = Pattern.compile(sb.toString());

        m = p.matcher(sub);

        EmailParserVO item = null;

        while (m.find()) {

            String storeitemname = m.group();

            item = new EmailParserVO();

//            item.setStoreitemname(m.group());

            sb2 = new StringBuilder();

            sb2.append("(Size : )([a-zA-Z\\s]+)()");

            pp = Pattern.compile(sb2.toString());

            mm = pp.matcher(sub);
//            mm = pp.matcher(sub.replaceAll(" ", " "));

            sb3 = new StringBuilder();

            sb3.append("([0-9]+개)");

            ppp = Pattern.compile(sb3.toString());

            mmm = ppp.matcher(sub);
//            System.out.println("mm["+mm.find()+"]");
            if(mm.find()) {
                mm = pp.matcher(sub);
//                mm = pp.matcher(sub.replaceAll(" ", " "));
                while (mm.find()) {
                    if(mmm.find()) {
                        mmm = ppp.matcher(sub);
                        while (mmm.find()) {
                            item = new EmailParserVO();
                            item.setStoreitemname(storeitemname);
                            item.setStorecode(m.group(4));
                            item.setItemsize(mm.group(2));
                            item.setQty(mmm.group());
                            items.add(item);
                        }
                    } else {
                        item = new EmailParserVO();
                        item.setStoreitemname(storeitemname);
                        item.setStorecode(m.group(4));
                        item.setItemsize(mm.group(2));
                        items.add(item);
                    }
                }
            } else {
                if(mmm.find()) {
                    mmm = ppp.matcher(sub);
                    int i = 0;
                    while (mmm.find()) {
                        i++;
                        item = new EmailParserVO();
                        item.setStorecode(m.group(4));
                        item.setStoreitemname(storeitemname);
                        item.setCnt(i);
                        item.setQty(mmm.group());
                        items.add(item);
                    }
                } else {
                    item = new EmailParserVO();
                    item.setStorecode(m.group(4));
                    item.setStoreitemname(storeitemname);
                    item.setQty(qty);
                    items.add(item);
                }
            }

        }

        return items;
    }

    public static String ConvertDateTimeFormat(String date) {

        String result = "";

        SimpleDateFormat original_format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
        SimpleDateFormat new_format = new SimpleDateFormat("yyyyMMddHHmmss");

        // 날짜 형식 변환시 파싱 오류를 try.. catch..로 체크한다.
        try {
            // 문자열 타입을 날짜 타입으로 변환한다.
            Date original_date = original_format.parse(date);

            // 날짜 형식을 원하는 타입으로 변경한다.
            result = new_format.format(original_date);

            // 결과를 출력한다.
            // System.out.println("result[" + result + "]");
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        return result;
    }

    public static String ConvertTimeFormat(String date) {

        String result = "";

        SimpleDateFormat original_format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
        SimpleDateFormat new_format = new SimpleDateFormat("HHmmss");

        // 날짜 형식 변환시 파싱 오류를 try.. catch..로 체크한다.
        try {
            // 문자열 타입을 날짜 타입으로 변환한다.
            Date original_date = original_format.parse(date);

            // 날짜 형식을 원하는 타입으로 변경한다.
            result = new_format.format(original_date);

            // 결과를 출력한다.
            // System.out.println("result[" + result + "]");
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        return result;
    }

    public static String ConvertYMDFormat(String date) {

        String result = "";

        SimpleDateFormat original_format = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.US);
        SimpleDateFormat new_format = new SimpleDateFormat("yyyyMMdd");

        // 날짜 형식 변환시 파싱 오류를 try.. catch..로 체크한다.
        try {
            // 문자열 타입을 날짜 타입으로 변환한다.
            Date original_date = original_format.parse(date);

            // 날짜 형식을 원하는 타입으로 변경한다.
            result = new_format.format(original_date);

            // 결과를 출력한다.
            // System.out.println("result[" + result + "]");
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

        return result;
    }
}