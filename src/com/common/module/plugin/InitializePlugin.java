package com.common.module.plugin;

import javax.servlet.ServletException;

import org.apache.struts.action.ActionServlet;
import org.apache.struts.action.PlugIn;
import org.apache.struts.config.ModuleConfig;

import com.common.module.conf.Constants;
import com.common.module.logger.LoggerManager;

/**
 * 어플리케이션 초기화데이터 설정
 * 
 * @author Hanuli
 */
public class InitializePlugin implements PlugIn {

    public final String TAG = this.getClass().getSimpleName();

    public final static LoggerManager log = new LoggerManager();

    public InitializePlugin() {
        super();
    }

    /**
     * 초기화 작업
     * 
     * @param servlet
     * @param config
     * @throws ServletException
     */
    public void init(ActionServlet servlet, ModuleConfig config) throws ServletException {
        // context_path 정보 설정
        Constants.CONTEXT_PATH = servlet.getServletContext().getRealPath("");

        log.info("Application Initialize[" + Constants.CONTEXT_PATH + "]");
        System.out.println("Application Initialize[" + Constants.CONTEXT_PATH + "]");
    }

    public void destroy() {
    }
}