package com.common.module.cipher;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.CharEncoding;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

public class ASECipher {

    public static void main(String[] args) {
        System.out.println("main");
        String encText = "";
        String decText = "";
        String fillText = "";

        String text = "cs6e57f1g8vf96o4";
        fillText = StringUtils.rightPad(text, 32, FILL);
        encText = encrypt(fillText);
        System.out.println("ASE 256 encText 1 : " + encText);
        decText = decrypt(encText);
        System.out.println("ASE 256 decText 1 : " + decText);
        text = "90285n15x1c1fu1o";
        fillText = StringUtils.rightPad(text, 32, FILL);
        encText = encrypt(fillText);
        System.out.println("ASE 256 encText 2 : " + encText);
        decText = decrypt(encText);
        System.out.println("ASE 256 decText 2 : " + decText);
        text = "cs6e57f1";
        fillText = StringUtils.rightPad(text, 32, FILL);
        encText = encrypt(fillText);
        System.out.println("ASE 256 encText 3 : " + encText);
        decText = decrypt(encText);
        System.out.println("ASE 256 decText 3 : " + decText);
    }

    private final static String KEY = "cs6e57f1g8vf96o490285n15x1c1fu1o";

    private final static String FILL = "79674r2qu1u4n13j4678kpb7worb0256";

    private final static String KEY_128 = KEY.substring(0, 128 / 8);

    private final static String KEY_256 = KEY.substring(0, 256 / 8);

    private final static String transformation = "AES/CBC/PKCS5Padding";

    public static Key generateKey(byte[] keyData, String algorithm) throws Exception {
        return new SecretKeySpec(keyData, algorithm);
    }

    public static String encrypt(String text) {
        try {

            byte[] key256Bytes = KEY_256.getBytes(CharEncoding.UTF_8);

            byte[] key128Bytes = KEY_128.getBytes(CharEncoding.UTF_8);

            Cipher cipher = Cipher.getInstance(transformation);

            Key key = generateKey(key256Bytes, "AES");

            cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(key128Bytes));

            byte[] encrypt = cipher.doFinal(text.getBytes(CharEncoding.UTF_8));

            byte[] base64Encrypt = Base64.encodeBase64(encrypt);

            String result = new String(base64Encrypt, CharEncoding.UTF_8);

            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public static String decrypt(String text) {
        try {

            byte[] key256Bytes = KEY_256.getBytes(CharEncoding.UTF_8);

            byte[] key128Bytes = KEY_128.getBytes(CharEncoding.UTF_8);

            Cipher cipher = Cipher.getInstance(transformation);

            Key key = generateKey(key256Bytes, "AES");

            cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(key128Bytes));

            byte[] base64Decrypt = Base64.decodeBase64(text.getBytes(CharEncoding.UTF_8));

            byte[] decrypt = cipher.doFinal(base64Decrypt);

            String result = new String(decrypt, CharEncoding.UTF_8);

            return result;
        } catch (Exception e) {
            return null;
        }
    }
}