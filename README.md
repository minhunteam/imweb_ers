# ImWeb 관리자 프로젝트

### **library 관련 설명**

다운로드 링크
https://dev.mysql.com/downloads/connector/j

mysql-connector-java-5.1.47.jar

다운로드 링크
https://tomcat.apache.org/download-taglibs.cgi

taglibs-standard-impl-1.2.5.jar
taglibs-standard-jstlel-1.2.5.jar
taglibs-standard-spec-1.2.5.jar

다운로드후 Tomcat 프로그램에 lib 폴더에 복사

### **DB 관련 프로퍼티 설정**
/src/db.properties
##### **별도 작성**
db_type=mysql
driver=com.mysql.jdbc.Driver
url=jdbc\:mysql\://{서버주소}\:3306/{DB명}?autoReconnect=false&autoReconnectForPools=true&useSSL=false&verifyServerCertificate=false
username={계정}
password={패스워드}