//용  도: 두 날짜의 차이 일수를 구한값 반환
//fncDaysBetween("2005-08-01", "2006-08-08");
//위와같이 사용했을 경우 두 날짜의 차이 일수를 리턴함.
function fncDaysBetween(sDate, eDate) {

// 시작일, 종료인 길이 확인
if (sDate.length != 10 || eDate.length != 10)
 return null;

// 년도, 월, 일로 분리
var start_dt = new Array( sDate.substring(0,4), sDate.substring(5,7), sDate.substring(8,10) );
var end_dt = new Array( eDate.substring(0,4), eDate.substring(5,7), eDate.substring(8,10) );

// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
start_dt[1] = (Number(start_dt[1]) - 1) + "";
end_dt[1] = (Number(end_dt[1]) - 1) + "";

var from_dt = new Date(start_dt[0], start_dt[1], start_dt[2]);
var to_dt = new Date(end_dt[0], end_dt[1], end_dt[2]);

return (to_dt.getTime() - from_dt.getTime()) / 1000 / 60 / 60 / 24;
}

/**
 * 공백제거
 */
function trim(str) {
    if (str == null || str.length == 0)
        return str;

    return str.replace(/(^\s)|(\s$)/g, '');
}

String.prototype.toDate = function(pattern) {
    var index = -1;
    var year;
    var month;
    var day;
    var hour = 0;
    var min  = 0;
    var sec  = 0;
    var ms   = 0;
    var newDate;

    if (pattern == null) {
        pattern = "YYYYMMDD";
    }

    if ((index = pattern.indexOf("YYYY")) == -1 ) {
        index = pattern.indexOf("YY");
        year = "20" + this.substr(index, 2);
    } else {
        year = this.substr(index, 4);
    }

    if ((index = pattern.indexOf("MM")) != -1 ) {
        month = this.substr(index, 2);
    } else {
        month = 1;
    }

    if ((index = pattern.indexOf("DD")) != -1 ) {
        day = this.substr(index, 2);
    } else {
        day = 1;
    }

    if ((index = pattern.indexOf("HH")) != -1 ) {
        hour = this.substr(index, 2);
    }

    if ((index = pattern.indexOf("mm")) != -1 ) {
        min = this.substr(index, 2);
    }

    if ((index = pattern.indexOf("ss")) != -1 ) {
        sec = this.substr(index, 2);
    }

    if ((index = pattern.indexOf("SS")) != -1 ) {
        ms = this.substr(index, 2);
    }

    newDate = new Date(year, month - 1, day, hour, min, sec, ms);
    if (month > 12) {
        newDate.setFullYear(year + 1);
    } else {
        newDate.setFullYear(year);
    }

    return newDate;
}

//용   도: 현재 Date String 반환
//return : date 문자열 (YYYY-MM-DD)
function getCurrentDate(){
    var day=new Date();
    var year=day.getFullYear();
    var month=day.getMonth()+1;
    var date=day.getDate();
    var getdage;
    if (month<10){month="0"+month;}
    if (date<10){date="0"+date;}
    getdate=year.toString() + "-" + month.toString() + "-" + date.toString();
    return (getdate);
}

//용  도: 현재 Date object 가지고 하루전 Date String 반환
//return : 1일전 date 문자열 (YYYY-MM-DD)
function getBeforDay(toDate) {
    var beforeDate = toDate;
    var rtnDate;
    beforeDate.setDate(beforeDate.getDate() -1);

    var year=beforeDate.getFullYear();
    var month=beforeDate.getMonth()+1;
    var date=beforeDate.getDate();
    var getdage;
    if (month<10){month="0"+month;}
    if (date<10){date="0"+date;}
    rtnDate=year.toString() + "-" + month.toString() + "-" + date.toString();
    return rtnDate;
}

//용  도: 현재 Date object 가지고 하루전 Date String 반환
//return : 원하는 날짜로 일전 date 문자열 (YYYY-MM-DD)
function getBeforeDay(toDate,num,type) {
    var beforeDate = toDate;
    var rtnDate;
    if(num == null || num == ''){
        num = 1;
    }
    if(type == null || type == ''){
        type = "p";
    }

    if(type == 'p'){
        beforeDate.setDate(beforeDate.getDate() + num);
    } else {
        beforeDate.setDate(beforeDate.getDate() - num);
    }

    var year=beforeDate.getFullYear();
    var month=beforeDate.getMonth()+1;
    var date=beforeDate.getDate();
    var getdage;
    if (month<10){month="0"+month;}
    if (date<10){date="0"+date;}
    rtnDate=year.toString() + "-" + month.toString() + "-" + date.toString();
    return rtnDate;
}
function getLastYearDay(toDate,num,type) {
    var beforeDate = toDate;
    var rtnDate;
    if(num == null || num == ''){
        num = 1;
    }
    if(type == null || type == ''){
        type = "p";
    }
    if(type == 'p'){
        beforeDate.setDate(beforeDate.getDate() + num);
    } else {
        beforeDate.setDate(beforeDate.getDate() - num);
    }
    var year=beforeDate.getFullYear();
    var month=12;
    var date= new Date(year,month,0).getDate();
    var getdage;
    if (month<10){month="0"+month;}
    if (date<10){date="0"+date;}
    rtnDate=year.toString() + "-" + month.toString() + "-" + date.toString();
    return rtnDate;
}
/**
 * 특정날짜의 요일을 구한다.
 */
function getDayOfWeek(time) {
    var datestr = time.replace(/-/gi,'');
    var year  = datestr.substr(0,4);
    var month = datestr.substr(4,2)-1; // 1월=0,12월=11
    var day   = datestr.substr(6,2);
    var now = new Date(year,month,day);
    var day = now.getDay(); //일요일=0,월요일=1,...,토요일=6
    return day;
}
/**
 * 특정날짜의 요일을 구한다.
 */
function getDayOfWeekName(time) {
    var datestr = time.replace(/-/gi,'');
    var year  = datestr.substr(0,4);
    var month = datestr.substr(4,2)-1; // 1월=0,12월=11
    var day   = datestr.substr(6,2);
    var now = new Date(year,month,day);
    var day = now.getDay(); //일요일=0,월요일=1,...,토요일=6
    var week = new Array('일요일','월요일','화요일','수요일','목요일','금요일','토요일');
    return week[day];
}
/**
 * Time 스트링을 자바스크립트 Date 객체로 변환
 * parameter time: Time 형식의 String
 */
function toTimeObject(time) { //parseTime(time)
    var year  = time.substr(0,4);
    var month = time.substr(4,2)-1; // 1월=0,12월=11
    var day   = time.substr(6,2);
    var ymd = new Date('2013',month,'21');
    return ymd;
}

function email_chk(email) {
    var invalidChars = "\"|&;<>!*'\"";
    for (var i = 0; i < invalidChars.length; i++) {
      if (email.indexOf(invalidChars.charAt ) != -1) {
      alert("잘못된 이메일 주소입니다.");
        return false;
      }
    }
    if (email.indexOf("@")==-1){
      alert("잘못된 이메일 주소입니다. '@'가 없습니다..");
      return false;
    }
    if (email.indexOf(" ") != -1){
      alert("잘못된 이메일 주소입니다. 공백이 있습니다");
      return false;
    }
    var reg_email = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
    if (!reg_email.test(email)) {
        alert("잘못된 이메일 주소입니다.");
        return;
    }
    return true;
}

phonenumberchk=function(phone){
    var regExp = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,4}-?[0-9]{4}$/gi;
    var phoneflag = regExp.exec(phone);
    if(phoneflag == null){
        return false;
    } else {
        return true;
    }
}


engimeMode=function(obj){
    var pattern = /[^a-zA-Z0-9.]/;
    if(pattern.test(obj.value)){
        //alert("영문만 허용");
        //obj.value = '';
        var a =obj.value.replace(/[^a-zA-Z0-9.]/gi,'');
        obj.value = a;
        obj.focus();
        return false;
    }
}
contentsMode=function(obj){
    var pattern = /[<>]/;
    if(pattern.test(obj.value)){
        var a =obj.value.replace(/[<>]/gi,'');
        obj.value = a;
        obj.focus();
        return false;
    }
}
engimeModes=function(obj){
    var pattern = /[^a-zA-Z0-9.\/]/;
    if(pattern.test(obj.value)){
        //alert("영문만 허용");
        //obj.value = '';
        var a =obj.value.replace(/[^a-zA-Z0-9.\/]/gi,'');
        obj.value = a;
        obj.focus();
        return false;
    }
}
urlParamMode=function(obj){
    var pattern = /[^a-zA-Z0-9&=/]/;
    if(pattern.test(obj.value)){
        //alert("영문만 허용");
        //obj.value = '';
        var a =obj.value.replace(/[^a-zA-Z0-9&=/]/gi,'');
        obj.value = a;
        obj.focus();
        return false;
    }
}

imeMode=function(obj){
    var RegNotNum  = /[^a-zA-Z0-9]/gi;
    var RegNum = "";
    var str = "";
    str = obj.value;
    if( str == "" || str == null ){
        return "";
    }
    if(RegNotNum.test(str)){
        obj.value = str.replace(RegNotNum,'');
        obj.focus();
        return false;
    }
}

codeImeMode=function(obj){
    var RegNotNum  = /[^a-zA-Z0-9_-]/gi;
    var RegNum = "";
    var str = "";
    str = obj.value;
    if( str == "" || str == null ){
        return "";
    }
    if(RegNotNum.test(str)){
        obj.value = str.replace(RegNotNum,'');
        obj.focus();
        return false;
    }
}

emailMode=function(obj){
    var pattern = /[^a-zA-Z0-9.@]/;
    if(pattern.test(obj.value)){
        //alert("영문만 허용");
        //obj.value = '';
        var a =obj.value.replace(/[^a-zA-Z0-9.@]/gi,'');
        obj.value = a;
        obj.focus();
        return false;
    }
}

numMode=function(obj){
    var RegNotNum  = /[^0-9]/gi;
    var RegNum = "";
    var str = "";
    str = obj.value;
    if( str == "" || str == null ){
        return "";
    }
    if(RegNotNum.test(str)){
        obj.value = str.replace(RegNotNum,'');
        obj.focus();
        return false;
    }
}

pointMode=function(obj){
    var pattern = /[^0-9.]/;
    if(pattern.test(obj.value)){
        //alert("영문만 허용");
        var a =obj.value.replace(/[^0-9.]/gi,'');
        obj.value = a;
        obj.focus();
        return false;
    }
}

hanMode=function(obj){
    var RegNotNum  = /[^ㄱ-ㅎㅏ-ㅣ가-힣]/gi;
    var RegNum = "";
    var str = "";
    str = obj.value;
    if( str == "" || str == null ){
        return "";
    }
    if(RegNotNum.test(str)){
        obj.value = str.replace(RegNotNum,'');
        obj.focus();
        return false;
    }
}
hanEngMode=function(obj){
    var RegNotNum  = /[^ㄱ-ㅎㅏ-ㅣ가-힣a-zA-Z() ]/gi;
    var RegNum = "";
    var str = "";
    str = obj.value;
    if( str == "" || str == null ){
        return "";
    }
    if(RegNotNum.test(str)){
        obj.value = str.replace(RegNotNum,'');
        obj.focus();
        return false;
    }
}

lowerCase=function(obj){
    obj.value=obj.value.toLowerCase();
}
upperCase=function(obj){
    obj.value=obj.value.toUpperCase();
}

fn_maskPhone=function(obj) {
    obj.value =  fn_PhoneNumStr(obj.value);
}

fn_PhoneNumStr=function(str){
var RegNotNum  = /[^0-9]/g;
var RegPhoneNum = "";
var DataForm   = "";

// return blank
if( str == "" || str == null ) return "";

// delete not number
str = str.replace(RegNotNum,'');

if( str.length < 4 ) return str;

if( str.length > 3 && str.length < 7 ) {
       DataForm = "$1-$2";
    RegPhoneNum = /([0-9]{3})([0-9]+)/;
} else if(str.length == 7 ) {
    DataForm = "$1-$2";
    RegPhoneNum = /([0-9]{3})([0-9]{4})/;
} else if(str.length == 8 ) {
    DataForm = "$1-$2-$3";
    RegPhoneNum = /([0-9]{3})([0-9]{4})([0-9]+)/;
} else if(str.length == 9 ) {
    if(str.substring(0,2)=="02"){
        DataForm = "$1-$2-$3";
        RegPhoneNum = /([0-9]{2})([0-9]{3})([0-9]+)/;
    } else {
        DataForm = "$1-$2-$3";
        RegPhoneNum = /([0-9]{3})([0-9]{4})([0-9]+)/;
    }
} else if(str.length == 10){
    if(str.substring(0,2)=="02"){
        DataForm = "$1-$2-$3";
        RegPhoneNum = /([0-9]{2})([0-9]{4})([0-9]+)/;
    }else{
        DataForm = "$1-$2-$3";
        RegPhoneNum = /([0-9]{3})([0-9]{3})([0-9]+)/;
    }
} else if(str.length > 10){
    DataForm = "$1-$2-$3";
    RegPhoneNum = /([0-9]{3})([0-9]{4})([0-9]+)/;
}

if(RegPhoneNum != ''){
    while( RegPhoneNum.test(str) ) {
        str = str.replace(RegPhoneNum, DataForm);
    }
}
return str;
}

fileExt=function(obj){
    var filetype;
    var filename;
    var fileLow;
    filename = obj.toLowerCase(obj);
    fileLow = filename.lastIndexOf(".");
    filetype = filename.substring(fileLow+1);
    switch(filetype){
        case "gif" :
        case "jpg" :
        case "png" :
        case "bmp" :
        case "tif" :
        case "jpe" :
            return true;
            break;
        default :
            return false;
    }
}

excelFileCheck=function(obj){
    var str = obj.toLowerCase();
    var uploadExt = new RegExp();
    uploadExt = /\.(xls|xlsx)$/gi;
    if(str.match(uploadExt) == null){
        return false;
    } else {
        return true;
    }
}

function previewImage(targetObj, previewId) {

    var preview = document.getElementById(previewId); //div id
    var ua = window.navigator.userAgent;

    if (ua.indexOf("MSIE") > -1) {//ie일때

        targetObj.select();

        try {
            var src = document.selection.createRange().text; // get file full path
            var ie_preview_error = document.getElementById("ie_preview_error_" + previewId);

            if (ie_preview_error) {
                preview.removeChild(ie_preview_error); //error가 있으면 delete
            }

            var img = document.getElementById(previewId); //이미지가 뿌려질 곳

            img.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+ src + "', sizingMethod='scale')"; //이미지 로딩, sizingMethod는 div에 맞춰서 사이즈를 자동조절 하는 역할
        } catch (e) {
            if (!document.getElementById("ie_preview_error_" + previewId)) {
                var info = document.createElement("<p>");
                info.id = "ie_preview_error_" + previewId;
                info.innerHTML = "a";
                preview.insertBefore(info, null);
            }
        }
    } else { //ie가 아닐때
        var files = targetObj.files;
        for ( var i = 0; i < files.length; i++) {

            var file = files[i];

            var imageType = /image.*/; //이미지 파일일경우만.. 뿌려준다.

            var prevImg = document.getElementById("prev_" + previewId); //이전에 미리보기가 있다면 삭제
            if (prevImg) {
                preview.removeChild(prevImg);
            }

            if (!file.type.match(imageType)) {
                //continue;
                alert("이미지파일이 아닙니다.");
                preview.innerHTML = '';
                targetObj.value= '';
                continue;
            }

            var img = document.createElement("img"); //크롬은 div에 이미지가 뿌려지지 않는다. 그래서 자식Element를 만든다.
            img.id = "prev_" + previewId;
            img.classList.add("obj");
            img.file = file;
            img.style.width = '100px'; //기본설정된 div의 안에 뿌려지는 효과를 주기 위해서 div크기와 같은 크기를 지정해준다.
            img.style.height = '70px';
            preview.innerHTML = '';
            preview.appendChild(img);

            if (window.FileReader) { // FireFox, Chrome, Opera 확인.
                var reader = new FileReader();
                reader.onloadend = (function(aImg) {
                    return function(e) {
                        aImg.src = e.target.result;
                    };
                })(img);
                reader.readAsDataURL(file);
            } else { // safari is not supported FileReader
                //alert('not supported FileReader');
                if (!document.getElementById("sfr_preview_error_"
                        + previewId)) {
                    var info = document.createElement("p");
                    info.id = "sfr_preview_error_" + previewId;
                    info.innerHTML = "not supported FileReader";
                    preview.insertBefore(info, null);
                }
            }
        }
    }
}