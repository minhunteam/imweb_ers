<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql"       prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml"       prefix="x"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld"           prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld"           prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/pageNavigator.tld"         prefix="p"  %>
<%@ taglib uri="/WEB-INF/tld/stringFormat.tld"          prefix="sf" %>
<fmt:setLocale value="ko_KR"/>