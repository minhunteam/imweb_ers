<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
    <div class="main">
        <div class="main_title"><img src="/images/main_logo.gif"></div>
        <ul class="login_info">
            <li style="position:relative; bottom:4px; padding-left:5px;"><a href="/connect.do?method=logout&route=R001"><img src="/images/top_logout.gif" /></a></li>
            <li>님  </li>
            <li style="font-weight:bold; color:#FFF;"><c:out value="${userSession.username}"/></li>
        </ul>
    </div>
    <div>
        <ul class="top_menu">
            <li class="select"><a href="/manage/base/root.do?method=list">기초 자료 관리</a></li>
            <li class="select"><a href="/store/info.do?method=list&rownum=15">매장 및 주문 관리</a></li>
            <!--
            <li><a href="/manage/base/auth.do?method=list&rownum=15">관리자 권한</a></li>
            <c:forEach items="${topMenulist}" var="vo" varStatus="status">
            <li <c:if test="${vo.menuno eq menuVo.menuno }">class="select"</c:if> ><a href="javascript:goTopLink('<c:out value="${vo.menuurl}"/>','<c:out value="${vo.menucmd}"/>','<c:out value="${vo.subtype}"/>','<c:out value="${vo.menuno}"/>');"><c:out value="${vo.menuname}"/></a></li>
            </c:forEach> 
            <li class="select"><a href="/manage/base/administrator/main.do?cmd=list">기초 자료 관리</a></li>
            <li><a href="/manage/base/auth/main.do?cmd=list">관리자 권한</a></li>
            <li><a href="/manage/base/menu/main.do?cmd=list">메뉴 관리</a></li>
             --> 
        </ul>
    </div>
<form name="topfrm" id="topfrm" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="method"      id="method"     value="" />
<input type="hidden" name="subtype"     id="subtype"    value="" />
<input type="hidden" name="topMenuNo"   id="topMenuNo"  value="" />
</form>
<script type="text/javascript" lang="javascript">
goTopLink=function(url,cmd,subtype,topMenuNo){
    var f = document.getElementById("topfrm");
    f.cmd.value = cmd;
    f.subtype.value = subtype;
    f.topMenuNo.value = topMenuNo;
    f.action = url;
    f.submit();
}
</script>