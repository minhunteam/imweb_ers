<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<div class="left" id="dropdown">
    <div class="left_menu_top">판매 관리</div>
    <ul class="left_menu">
        <li>
            <img src="/images/left_menu_icon1.gif" />&nbsp;판매현황
            <ul class="drop">
                <ol><a href="/store/order/item.do?method=list&rownum=15&storecode=<c:out value="${userSession.storecode}"/>">판매현황</a></ol>
                <ol><a href="/store/order/sale.do?method=list&rownum=15&storecode=<c:out value="${userSession.storecode}"/>">매출현황</a></ol>
            </ul>
        </li>
    </ul>
</div>
<form name="leftfrm" id="leftfrm" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="cmd"         id="cmd"        value="" />
<input type="hidden" name="subtype"     id="subtype"    value="" />
<input type="hidden" name="menuno"      id="menuno"     value="" />
<input type="hidden" name="topMenuNo"   id="topMenuNo"  value="<c:out value="${menuVo.menuno}"/>" />
</form>
<script type="text/javascript" lang="javascript">
goLeftLink=function(url,cmd,subtype,menuno){
    var f = document.getElementById("leftfrm");
    f.cmd.value = cmd;
    f.subtype.value = subtype;
    f.menuno.value = menuno;
    f.action = url;
    f.submit();
}
</script>