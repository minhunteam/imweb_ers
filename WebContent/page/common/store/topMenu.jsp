<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
    <div class="main">
        <div class="main_title"><img src="/images/main_logo.gif"></div>
        <ul class="login_info">
            <li style="position:relative; bottom:4px; padding-left:5px;"><a href="/connect/store.do?method=logout&route=R002"><img src="/images/top_logout.gif" /></a></li>
            <li>님  </li>
            <li style="font-weight:bold; color:#FFF;"><c:out value="${userSession.username}"/></li>
        </ul>
    </div>
    <div>
        <ul class="top_menu">
            <li class="select"><a href="/store/order/item.do?method=list&rownum=15&storecode=<c:out value="${userSession.storecode}"/>">판매 관리</a></li>
        </ul>
    </div>
<form name="topfrm" id="topfrm" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="cmd"         id="cmd"        value="" />
<input type="hidden" name="subtype"     id="subtype"    value="" />
<input type="hidden" name="topMenuNo"   id="topMenuNo"  value="" />
</form>
<script type="text/javascript" lang="javascript">
goTopLink=function(url,cmd,subtype,topMenuNo){
    var f = document.getElementById("topfrm");
    f.cmd.value = cmd;
    f.subtype.value = subtype;
    f.topMenuNo.value = topMenuNo;
    f.action = url;
    f.submit();
}
</script>