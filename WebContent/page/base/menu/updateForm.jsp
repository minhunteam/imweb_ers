<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<%@ page import="java.util.*" %>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><bean:message key="site.title" /></title>
    </head>
<body>
<%@ include file="/page/common/root/topMenu.jsp"%>
<%@ include file="/page/common/root/leftMenu.jsp"%>
<form name="frm" id="frm" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="cmd"         id="cmd"        value="updateProcess"/>
<input type="hidden" name="rowno"       id="rowno"      value="1"/>
<input type="hidden" name="menuno"      id="menuno"     value="<c:out value="${view.menuno }"/>"/>
<input type="hidden" name="menutype"    id="menutype"   value="<c:out value="${view.menutype }"/>"/>
<input type="hidden" name="topMenuNo"   id="topMenuNo"  value="<c:out value="${topMenuNo}"/>"/>
<div class="right">
    <div class="right_title"><h1>메뉴 정보</h1><h5>메뉴관리 수정</h5></div>
    <hr class="line">
        <div class="cont">
            <table class="goods_table" cellpadding="0" cellspacing="0">
            <caption><img src="/images/title_note.gif"> <b>수정정보</b></caption>
                <colgroup><col width="200" /><col /></colgroup>
                    <tbody>
                    <tr>
                        <th><img src="/images/table_icon.gif">메뉴 명</th>
                        <td><input type="text" name="menuname" id="menuname" style="IME-MODE:active" maxlength="50" class="box" style="width:150px;" value="<c:out value="${view.menuname}"/>"/></td>
                    </tr>
                    <tr>
                        <th><img src="/images/table_icon.gif">메뉴 타입</th>
                        <td>
                            <c:if test="${view.menutype eq 'CD001'}">최상위</c:if>
                            <c:if test="${view.menutype eq 'CD002'}">탑메뉴</c:if>
                            <c:if test="${view.menutype eq 'CD003'}">서브메뉴</c:if>
                            <c:if test="${view.menutype eq 'CD004'}">프로그램명</c:if>
                        </td>
                    </tr>
                    <c:if test="${view.menutype eq 'CD002' || view.menutype eq 'CD004'}">
                    <tr>
                        <th><img src="/images/table_icon.gif">메뉴 주소</th>
                        <td><input type="text" name="menuurl" id="menuurl" onKeyUp="lowerCase(this);engimeModes(this);" onKeyDown="lowerCase(this);engimeModes(this);"  maxlength="100" class="box" style="ime-mode:disabled; !important;width:200px;" value="<c:out value="${view.menuurl}"/>"/></td>
                    </tr>
                    <tr>
                        <th><img src="/images/table_icon.gif">메뉴 명령어</th>
                        <td><input type="text" name="menucmd" id="menucmd" onKeyUp="urlParamMode(this);" onkeydown="urlParamMode(this);" maxlength="20" class="box" style="width:200px;ime-mode:disabled; !important" value="<c:out value="${view.menucmd}"/>"/></td>
                    </tr>
                    <tr>
                        <th><img src="/images/table_icon.gif">메뉴 파라미터</th>
                        <td><input type="text" name="subtype" id="subtype" onKeyUp="urlParamMode(this);" onkeydown="urlParamMode(this);" maxlength="20" class="box" style="width:200px;ime-mode:disabled; !important" value="<c:out value="${view.subtype}"/>"/></td>
                    </tr>
                    </c:if>
                    </tbody>
             </table>
        </div>
    <div class="new_write">
    <ul class="bottom_btn">
        <li style="float:right;"><input type="button" class="button" value="취소" onclick="goList();" style="cursor: pointer;"/></li>
        <li style="float:right; padding-right:3px;"><input type="button" class="button2" value="확인" onclick="goUpdateProcess();" style="cursor: pointer;"></li>
    </ul>
    </div>
</div>
</form>

<script type="text/javascript" lang="javascript" src="/js/jquery.js"></script>
<script type="text/javascript" lang="javascript" src="/js/jquery.alphanumeric.js"></script>
<script type="text/javascript" lang="javascript" src="/js/common.js" charset="UTF-8"></script>
<script type="text/javascript" lang="javascript">
var j$  = jQuery.noConflict(); //prototype과 같은 javascript lib, 프레임웍을 같이 사용할 때 충돌을 막기 위함.
j$(document).ready(function() {
    j$('#menuname').alphan(({allow:'()/ '}));
    j$('#menuname').css('ime-mode','active');
    j$('#menuurl').alpha(({allow:'/. '}));
    j$('#menuurl').css('ime-mode','disabled');
    j$('#menucmd').alpha(({allow:'1234567890'}));
    j$('#menucmd').css('ime-mode','disabled');
    j$('#subtype').alpha(({allow:'1234567890'}));
    j$('#subtype').css('ime-mode','disabled');
});

goList=function(){
    var f = document.getElementById("frm");
        f.cmd.value="list";
        f.action="/manage/base/menu/main.do";
        f.submit();
}

goUpdateProcess=function(){
    var f = document.getElementById("frm");

    if(f.menuname.value == ''){
        alert("메뉴 명을 입력해주세요.");
        return false;
    }

    if(f.menutype.value == ''){
        alert("메뉴 타입을 선택해주세요.");
        return false;
    }

    if(f.menutype.value == 'CD004'){
        if(f.menuurl.value == ''){
            alert("메뉴 주소를 입력해주세요.");
            return false;
        }

        if(f.menucmd.value == ''){
            alert("메뉴 파라미터를 입력해주세요.");
            return false;
        }
    }

    if(confirm("수정하시겠습니까?")){
        f.action="/manage/base/menu/main.do";
        f.submit();
    } else {
        return;
    }
}
</script>
</body>
</html>