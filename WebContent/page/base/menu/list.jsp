<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<%@ page import="java.util.*" %>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><bean:message key="site.title" /></title>
    </head>
<body>
<%@ include file="/page/common/root/topMenu.jsp"%>
<%@ include file="/page/common/root/leftMenu.jsp"%>
<div class="right">
    <div class="right_title"><h1>메뉴 정보</h1><h5>메뉴관리 리스트</h5></div>
    <hr class="line">
        <div class="search_result">
        <table class="goods_table_02" cellpadding="0" cellspacing="0">
        <caption><img src="/images/title_note.gif"> <b>메뉴관리 리스트</b></caption>
            <colgroup><col /><col /><col /><col /><col /><col /><col /><col /></colgroup>
                <tbody>
                <tr>
                    <th>최상위</th>
                    <th>탑 메뉴</th>
                    <th>서브 메뉴</th>
                    <th>프로그램명</th>
                    <th>메뉴 주소</th>
                    <th>메뉴 명령어</th>
                    <th>메뉴 파라미터</th>
                    <th style="border-right:none;">비고</th>
                </tr>
            <c:choose>
                <c:when test="${empty list}">
                    <tr>
                        <td colspan="8" style="border-right:none;">조회된 데이터가 없습니다.</td>
                    </tr>
                </c:when>
                <c:otherwise>
                <c:forEach items="${list}" var="vo" varStatus="status">
                    <tr>
                        <td><a href="javascript:goView('<c:out value="${vo.menuno}"/>')"><c:if test="${vo.menutype eq 'CD001' }"><c:out value="${vo.menuname}"/></c:if></a></td>
                        <td><a href="javascript:goView('<c:out value="${vo.menuno}"/>')"><c:if test="${vo.menutype eq 'CD002' }"><c:out value="${vo.menuname}"/></c:if></a></td>
                        <td><a href="javascript:goView('<c:out value="${vo.menuno}"/>')"><c:if test="${vo.menutype eq 'CD003' }"><c:out value="${vo.menuname}"/></c:if></a></td>
                        <td><a href="javascript:goView('<c:out value="${vo.menuno}"/>')"><c:if test="${vo.menutype eq 'CD004' }"><c:out value="${vo.menuname}"/></c:if></a></td>
                        <td><c:out value="${vo.menuurl}"/></td>
                        <td><c:out value="${vo.menucmd}"/></td>
                        <td><c:out value="${vo.subtype}"/></td>
                        <td style="border-right:none;"><c:if test="${vo.menutype ne 'CD004'}"><input type="button" class="button" value="메뉴 추가" onclick="goInsertForm('<c:out value="${vo.menuno}"/>');"/></c:if></td>
                    </tr>
                </c:forEach>
                </c:otherwise>
                    </c:choose>
                </tbody>
        </table>
    </div>
    <div class="new_write">
    <ul class="bottom_btn">
        <li><input type="button" class="button" value="목록" style="cursor: pointer;" onclick="goList();"/></li>
        <li style="float:right;"><input type="button" class="button2" value="등록" onclick="goInsertForm('M0000');" style="cursor: pointer;"/></li>
    </ul>
    </div>
</div>
<form name="frm" id="frm" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="cmd"         id="cmd"        value="list"/>
<input type="hidden" name="rowno"       id="rowno"      value="1"/>
<input type="hidden" name="menuno"      id="menuno"     value=""/>
<input type="hidden" name="topMenuNo"   id="topMenuNo"  value="<c:out value="${topMenuNo}"/>"/>
</form>
<script type="text/javascript" lang="javascript">
goView=function(menuno){
    var f = document.getElementById("frm");
        f.menuno.value = menuno;
        f.cmd.value = "view";
        f.action = "/manage/base/menu/main.do";
        f.submit();
}
goList=function(){
    var f = document.getElementById("frm");
        f.cmd.value="list";
        f.action = "/manage/base/menu/main.do";
        f.submit();
}
goInsertForm=function(menuno){
    var f = document.getElementById("frm");
        f.cmd.value = "insertForm";
        f.menuno.value = menuno;
        f.action = "/manage/base/menu/main.do";
        f.submit();
}
</script>
</body>
</html>