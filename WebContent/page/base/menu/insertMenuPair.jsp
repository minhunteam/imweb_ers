<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<%@ page import="java.util.*" %>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><bean:message key="site.title" /></title>
    </head>
<body>
<%@ include file="/page/common/root/topMenu.jsp"%>
<%@ include file="/page/common/root/leftMenu.jsp"%>
<div class="right">
    <div class="right_title"><h1>권한 메뉴 정보</h1><h5>권한  메뉴관리 리스트</h5></div>
    <hr class="line">
        <div class="search_result">
        <table class="goods_table_03" cellpadding="0" cellspacing="0">
        <caption><img src="/images/title_note.gif"> <b>권한 메뉴관리 리스트</b></caption>
            <colgroup><col /><col /><col /><col /><col /><col /><col /></colgroup>
                <tbody>
                <tr>
                    <th>최상위</th>
                    <th>탑 메뉴</th>
                    <th>서브 메뉴</th>
                    <th>프로그램명</th>
                    <th>메뉴 주소</th>
                    <th>메뉴 명령어</th>
                    <th style="border-right:none;">메뉴 파라미터</th>
                </tr>
            <c:choose>
                <c:when test="${empty list}">
                    <tr>
                        <td colspan="7" style="border-right:none;">조회된 데이터가 없습니다.</td>
                    </tr>
                </c:when>
                <c:otherwise>
                <c:forEach items="${list}" var="vo" varStatus="status">
                    <tr>
                        <td><c:if test="${vo.menutype eq 'CD001' }"><input type="checkbox" name="menuchk" id="menuchk" menuno="<c:out value="${vo.menuno}"/>" pmenuno="<c:out value="${vo.pmenuno}"/>" onclick="checkMenu(this);" value="<c:out value="${vo.menuno}"/>" <c:if test="${vo.menustate eq '00' }">checked="checked"</c:if>/><c:out value="${vo.menuname}"/></c:if></td>
                        <td><c:if test="${vo.menutype eq 'CD002' }"><input type="checkbox" name="menuchk" id="menuchk" menuno="<c:out value="${vo.menuno}"/>" pmenuno="<c:out value="${vo.pmenuno}"/>" onclick="checkMenu(this);" value="<c:out value="${vo.menuno}"/>" <c:if test="${vo.menustate eq '00' }">checked="checked"</c:if>/><c:out value="${vo.menuname}"/></c:if></td>
                        <td><c:if test="${vo.menutype eq 'CD003' }"><input type="checkbox" name="menuchk" id="menuchk" menuno="<c:out value="${vo.menuno}"/>" pmenuno="<c:out value="${vo.pmenuno}"/>" onclick="checkMenu(this);" value="<c:out value="${vo.menuno}"/>" <c:if test="${vo.menustate eq '00' }">checked="checked"</c:if>/><c:out value="${vo.menuname}"/></c:if></td>
                        <td><c:if test="${vo.menutype eq 'CD004' }"><input type="checkbox" name="menuchk" id="link"    menuno="<c:out value="${vo.menuno}"/>" pmenuno="<c:out value="${vo.pmenuno}"/>" onclick="checkMenu(this);" value="<c:out value="${vo.menuno}"/>" <c:if test="${vo.menustate eq '00' }">checked="checked"</c:if>/><c:out value="${vo.menuname}"/></c:if></td>
                        <td><c:out value="${vo.menuurl}"/></td>
                        <td><c:out value="${vo.menucmd}"/></td>
                        <td style="border-right:none;"><c:out value="${vo.subtype}"/></td>
                    </tr>
                </c:forEach>
                </c:otherwise>
                    </c:choose>
                </tbody>
        </table>
    </div>
    <div class="new_write">
    <ul class="bottom_btn">
        <li><input type="button" class="button" value="취소" style="cursor: pointer;" onclick="goList();"/></li>
        <li style="float:right;"><input type="button" class="button2" value="등록" onclick="goInsertProcess();" style="cursor: pointer;"/></li>
    </ul>
    </div>
</div>
<form name="frm" id="frm" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="cmd"         id="cmd"        value="list"/>
<input type="hidden" name="rowno"       id="rowno"      value="1"/>
<input type="hidden" name="authcode"    id="authcode"   value="<c:out value="${view.authcode}"/>"/>
<input type="hidden" name="menuno"      id="menuno"     value=""/>
<input type="hidden" name="topMenuNo"   id="topMenuNo"  value="<c:out value="${topMenuNo}"/>"/>
</form>
<script type="text/javascript" lang="javascript">
checkMenu=function(el){
    var pid = el.getAttribute('menuno');
    checkLower(pid,el.checked);
    //alert("menuno["+menuno+"]");
}
checkLower=function(code,is_checked){
    //alert("menuno["+pid+"]"+is_checked);
    var menuno = document.all['menuchk'];
    if( menuno ){
        for(i=0;i<menuno.length;i++){
            var obj = menuno[i];
            if( obj.getAttribute("pmenuno") == code ){
                obj.checked = is_checked;
                checkLink(obj.getAttribute("menuno"),is_checked);
            }
        }
    }
}
checkLink=function(pid,is_checked){
    var links = document.all['link'];
    if( links ){
        for(j=0;j<links.length;j++){
            var obj3 = links[j];
            if( obj3.getAttribute("pmenuno") == pid ){
                obj3.checked = is_checked;
            }
        }
    }
}
goList=function(){
    var f = document.getElementById("frm");
        f.cmd.value="view";
        f.action = "/manage/base/auth/main.do";
        f.submit();
}
goInsertProcess=function(menuno){
    var f = document.getElementById("frm");
    var ff = document.getElementsByName("menuchk");
    var menuArray = new Array();
        f.cmd.value = "insertMenuPairProcess";
    var j = 0;
    for(var i=0;i<ff.length;i++){
        if(ff[i].checked == true) {
            menuArray[j] = ff[i].value;
            j++;
        }
    }
    if(menuArray.length > 0){
        if(confirm("등록 하시겠습니까?")){
            f.menuno.value = menuArray;
            f.action = "/manage/base/menu/main.do";
            f.submit();
        } else {
            return;
        }
    } else {
        alert("메뉴를 선택해주세요.");
        return;
    }
}
</script>
</body>
</html>