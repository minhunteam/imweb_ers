<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<%@ page import="java.util.*" %>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><bean:message key="site.title" /></title>
    </head>
<body>
<%@ include file="/page/common/root/topMenu.jsp"%>
<%@ include file="/page/common/root/leftMenu.jsp"%>
<div class="right">
    <div class="right_title"><h1>관리자 정보</h1><h5>관리자의 등록/수정/삭제등을 관리 할 수 있습니다.</h5></div>
    <hr class="line">
        <div class="cont">
        <form name="frm" id="frm" method="post" style="margin: 0px;" onsubmit="return false;">
        <input type="hidden" name="method"      id="method" value="saveDone"/>
        <input type="hidden" name="rowno"       id="rowno" value="1"/>
        <input type="hidden" name="topMenuNo"   id="topMenuNo" value="<c:out value="${topMenuNo}"/>"/>
        <input type="hidden" name="rootcode"    id="rootcode" value="${view.rootcode}"/>
        <table class="goods_table" cellpadding="0" cellspacing="0">
        <caption><img src="/images/title_note.gif"> <b>등록정보</b></caption>
            <colgroup><col width="200" /><col /></colgroup>
                <tbody>
                <tr>
                    <th><img src="/images/table_icon.gif">관리자 아이디</th>
                    <td><input type="text" name="rootid" id="rootid" class="box" style="width:150px;IME-MODE:disabled; !important"  onkeydown="imeMode(this);" onkeyup="lowerCase(this);imeMode(this);" <c:if test="${view.rootid eq ''}">onfocus="this.value='';"</c:if> maxlength="20" value="${view.rootid}" <c:if test="${view.rootid ne ''}">readonly="readonly"</c:if>/></td>
                </tr>
                <tr>
                    <th><img src="/images/table_icon.gif">관리자 패스워드</th>
                    <td><input type="password" name="rootpw" id="rootpw" class="box" style="width:150px;" onfocus="this.value='';" onkeyup="lowerCase(this);" maxlength="20" value=""/></td>
                </tr>
                <tr>
                    <th><img src="/images/table_icon.gif">관리자 패스워드 확인</th>
                    <td><input type="password" name="rootpwd" id="rootpwd" class="box" style="width:150px;" onfocus="this.value='';" onkeyup="lowerCase(this);" maxlength="20" value=""/></td>
                </tr>
                <tr>
                    <th><img src="/images/table_icon.gif">관리자 이름</th>
                    <td><input type="text" class="box" style="width:150px;" name="rootname" id="rootname" style="ime-mode:active" <c:if test="${view.rootid eq ''}">onfocus="this.value='';"</c:if> maxlength="10" value="${view.rootname}" <c:if test="${view.rootid ne ''}">readonly="readonly"</c:if>/></td>
                </tr>
                <tr>
                    <th><img src="/images/table_icon.gif">관리자 권한</th>
                    <td>
                        <select class="box" style="width:80px;height:19px;" name="authcode" id="authcode">
                            <option value="" <c:if test="${view.authcode eq ''}">selected="selected"</c:if>>선택</option>
                            <c:if test="${userSession.authcode eq 'AT001' }">
                            <option value="AT001" <c:if test="${view.authcode eq 'AT001'}">selected="selected"</c:if>>최고 관리자</option>
                            <option value="AT002" <c:if test="${view.authcode eq 'AT002'}">selected="selected"</c:if>>관리자</option>
                            <option value="AT003" <c:if test="${view.authcode eq 'AT003'}">selected="selected"</c:if>>운영자</option>
                            <option value="AT004" <c:if test="${view.authcode eq 'AT004'}">selected="selected"</c:if>>상담원</option>
                            </c:if>
                            <c:if test="${userSession.authcode eq 'AT002' }">
                            <option value="AT002" <c:if test="${view.authcode eq 'AT002'}">selected="selected"</c:if>>관리자</option>
                            <option value="AT003" <c:if test="${view.authcode eq 'AT003'}">selected="selected"</c:if>>운영자</option>
                            <option value="AT004" <c:if test="${view.authcode eq 'AT004'}">selected="selected"</c:if>>상담원</option>
                            </c:if>
                            <c:if test="${userSession.authcode eq 'AT003' }">
                            <option value="AT003" <c:if test="${view.authcode eq 'AT003'}">selected="selected"</c:if>>운영자</option>
                            <option value="AT004" <c:if test="${view.authcode eq 'AT004'}">selected="selected"</c:if>>상담원</option>
                            </c:if>
                        </select>
                    </td>
                </tr>
         </tbody>
         </table>
         </form>
         </div>
    <div class="new_write">
    <ul class="bottom_btn">
        <li style="float:right;"><input type="button" class="button" value="취소" onclick="goList();" style="cursor: pointer;"/></li>
        <li style="float:right; padding-right:3px;"><input type="button" class="button2" value="저장" id="saveProcess" style="cursor: pointer;"/></li>
    </ul>
    </div>
</div>
<form name="list" id="list" method="post" style="margin: 0px;" onsubmit="return false;">
    <input type="hidden" name="method"      id="method" value="list"/>
    <input type="hidden" name="rowno"       id="rowno" value="1"/>
    <input type="hidden" name="topMenuNo"   id="topMenuNo" value="<c:out value="${topMenuNo}"/>"/>
</form>
<script type="text/javascript" lang="javascript" src="/js/jquery.js"></script>
<script type="text/javascript" lang="javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" lang="javascript" src="/js/jquery.alphanumeric.js"></script>
<script type="text/javascript" lang="javascript" src="/js/common.js" charset="UTF-8"></script>
<script type="text/javascript" lang="javascript">
var j$  = jQuery.noConflict(); //prototype과 같은 javascript lib, 프레임웍을 같이 사용할 때 충돌을 막기 위함.
j$(document).ready(function() {
    j$('#rootid').alphanumeric();
    j$('#rootname').alphan();
    j$('#rootname').css('ime-mode','active');
    var options = {
             target       : '#frm'   // target element(s) to be updated with server response
            ,beforeSubmit : showRequest  // pre-submit callback
            ,success      : showResponse  // post-submit callback
            ,error        : showError  // post-submit callback
            ,url          : '/manage/base/root.do'
            ,type         : 'post'        // 'get' or 'post', override for form's 'method' attribute
            ,dataType     : 'json'        // 'xml', 'script', or 'json' (expected server response type)
            //clearForm: true        // clear all form fields after successful submit
            //resetForm: true        // reset the form after successful submit
            // $.ajax options can be used here too, for example:
            //timeout:   3000
        };
    j$('#frm').ajaxForm(options);
    j$('#frm').submit(function(){return false; });
    j$('#saveProcess').click(function(){
        goSaveProcess();
    });
});

goList=function(){
    var f = document.getElementById("list");
        f.method.value="list";
        f.action = "/manage/base/root.do";
        f.submit();
}

goSaveProcess=function(){
    if(j$('#rootid').val() == '') {
        alert("관리자 아이디를 입력해 주세요.");
        return;
    }
    if(j$('#rootcode').val() == ''){
        if(j$('#rootpw').val() == '') {
            alert("관리자 패스워드를 입력해 주세요.");
            return;
        } else {
            j$('#rootpw').val(j$.trim(j$("#rootpw").val()));
        }

        if(j$('#rootpwd').val() == '') {
            alert("관리자 패스워드 확인을 입력해 주세요.");
            return;
        } else {
            j$('#rootpwd').val(j$.trim(j$("#rootpwd").val()));
        }

        if(j$('#rootpw').val() != '' && j$('#rootpwd').val() != ''){
            if(j$('#rootpw').val() != j$('#rootpwd').val()){
                alert("동일한 패스워드를 입력해 주세요.");
                return;
            }
        }
    } else {
        if(j$('#rootpw').val() != '') {
            if(j$('#rootpwd').val() == '') {
                alert("관리자 패스워드 확인을 입력해 주세요.");
                return;
            }

            if(j$('#rootpw').val() != '' && j$('#rootpwd').val() != ''){
                if(j$('#rootpw').val() != j$('#rootpwd').val()){
                    alert("동일한 패스워드를 입력해 주세요.");
                    return;
                }
            }
        }
    }


    if(j$('#rootname').val() == '') {
        alert("사용자 이름을 입력해 주세요.");
        return;
    } else {
        j$('#rootname').val(j$.trim(j$("#rootname").val()));
    }


    if(j$('#authcode').val() == '') {
        alert("관리자 권한을 선택해주세요.");
        return;
    }

    if(confirm("저장 하시겠습니까?")){
        j$('#frm').submit();
        return "ok";
    } else {
        return;
    }
}

showRequest=function(formData, jqForm, options) {
    var queryString = j$.param(formData);
    //alert('About to submit: \n\n' + queryString);
    return true;
}

showResponse=function(responseText, statusText, xhr, j$form)  {
    var data=eval(responseText);
    //alert(data.success);
    alert(data.message);
    if(data.success =='0000'){
        goList();
    }
    //alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + '\n\nThe output div should have already been updated with the responseText.');
}

showError=function(){
    alert('errer');
}
</script>
</body>
</html>