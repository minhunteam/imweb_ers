<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<%@ page import="java.util.*" %>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><s:text name="site_title"/></title>
    </head>
<body>
<%@ include file="/page/common/root/topMenu.jsp"%>
<%@ include file="/page/common/root/leftMenu.jsp"%>
<div class="right">
    <div class="right_title"><h1>관리자 정보</h1><h5>관리자의 등록/수정/삭제등을 관리 할 수 있습니다.</h5></div>
    <hr class="line">
        <div class="cont">
        <table class="goods_table" cellpadding="0" cellspacing="0">
        <caption><img src="/images/title_note.gif"> <b>등록정보</b></caption>
            <colgroup><col width="200" /><col /></colgroup>
                <tbody>
                <tr>
                    <th><img src="/images/table_icon.gif">관리자 아이디</th>
                    <td><c:out value="${view.userid }"/></td>
                </tr>
                <tr>
                    <th><img src="/images/table_icon.gif">관리자 이름</th>
                    <td><c:out value="${view.username}"/></td>
                </tr>
                <tr>
                    <th><img src="/images/table_icon.gif">관리자 권한</th>
                    <td>
                        <c:if test="${view.authcode eq 'A0001'}">최고 관리자</c:if>
                        <c:if test="${view.authcode eq 'A0002'}">관리자</c:if>
                        <c:if test="${view.authcode eq 'A0003'}">운영자</c:if>
                    </td>
                </tr>
                <tr>
                    <th><img src="/images/table_icon.gif">관리자 상태</th>
                    <td>
                        <c:if test="${view.userstate eq '10'}">정상</c:if>
                        <c:if test="${view.userstate eq '20'}">삭제</c:if>
                    </td>
                </tr>
         </tbody>
         </table>
         </form>
         </div>
    <div class="new_write">
    <ul class="bottom_btn">
        <li><input type="button" class="button" value="목록" onclick="goList();" style="cursor: pointer;"/></li>
        <li style="float:right;"><input type="button" class="button2" value="수정" onclick="goSaveForm();" style="cursor: pointer;"/></li>
        <li style="float:right;padding-right:3px;"><input type="button" class="button2" value="삭제" onclick="goDeleteProcess('<c:out value="${view.usercode}"/>');" style="cursor: pointer;"/></li>
    </ul>
    </div>
</div>
<form name="frm" id="frm" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="cmd" id="cmd" value="view"/>
<input type="hidden" name="rowno" id="rowno" value="1"/>
<input type="hidden" name="usercode" id="usercode" value="<c:out value="${view.usercode}"/>"/>
<input type="hidden" name="userstate" id="userstate" value="<c:out value="${view.userstate}"/>"/>
<input type="hidden" name="topMenuNo" id="topMenuNo" value="<c:out value="${topMenuNo}"/>"/>
</form>
<script type="text/javascript" lang="javascript" src="/js/jquery.js"></script>
<script type="text/javascript" lang="javascript">
var j$  = jQuery.noConflict(); //prototype과 같은 javascript lib, 프레임웍을 같이 사용할 때 충돌을 막기 위함.
j$(document).ready(function() {
});
goList=function(){
    var f = document.getElementById("frm");
        f.cmd.value="list";
        f.usercode.value="";
        f.userstate.value="";
        f.action = "/manage/base/administrator/main.do";
        f.submit();
}
goSaveForm=function(){
    var f = document.getElementById("frm");
        f.cmd.value = "saveForm";
        f.action = "/manage/base/administrator/main.do";
        f.submit();
}
goDeleteProcess=function(usercode){
    var str = "삭제 하시겠습니까?";
    if(confirm(str)){
        j$.ajax({
            type: "POST",
            url: "/manage/base/administrator/deleteProcess.do",
            data: "usercode="+usercode+"&userstate=20",
            dataType: "json",
            success: function(result) {
                if(result.success == '0000'){
                    alert(result.message);
                    goList();
                } else {
                    alert(result.message);
                    return;
                }
            }
        });
    } else {
        return;
    }
}
</script>
</body>
</html>