<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<%@ page import="java.util.*" %>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><s:text name="site_title"/></title>
    </head>
<body>
<%@ include file="/page/common/root/topMenu.jsp"%>
<%@ include file="/page/common/root/leftMenu.jsp"%>
<div class="right">
    <div class="right_title"><h1>관리자 권한</h1><h5>관리자 권한을 관리 할 수 있습니다.</h5></div>
    <hr class="line">
        <div class="cont">
            <form name="frm" id="frm" method="post" style="margin: 0px;" onsubmit="return false;">
            <input type="hidden" name="cmd" id="cmd" value="saveProcess"/>
            <input type="hidden" name="rowno" id="rowno" value="1"/>
            <input type="hidden" name="authcode" id="authcode" value="${view.authcode}"/>
            <input type="hidden" name="topMenuNo" id="topMenuNo" value="<c:out value="${topMenuNo}"/>"/>
            <table class="goods_table" cellpadding="0" cellspacing="0">
            <caption><img src="/images/title_note.gif"> <b>등록정보</b></caption>
                <colgroup><col width="200" /><col /></colgroup>
                    <tbody>
                    <tr>
                        <th><img src="/images/table_icon.gif">권한 명</th>
                        <td><input type="text" name="authname" id="authname" style="IME-MODE:active;width:200px;" maxlength="10" class="box" style="width:150px;" value="<c:out value="${view.authname}"/>"></td>
                    </tr>
                    </tbody>
             </table>
             </form>
        </div>
    <div class="new_write">
    <ul class="bottom_btn">
        <li style="float:right;"><input type="button" class="button" value="취소" onclick="goList();"/></li>
        <li style="float:right;padding-right:3px;"><input type="button" class="button2" value="저장" onclick="goSaveProcess();" /></li>
    </ul>
    </div>
</div>
<script type="text/javascript" lang="javascript" src="/js/jquery.js"></script>
<script type="text/javascript" lang="javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" lang="javascript" src="/js/jquery.alphanumeric.js"></script>
<script type="text/javascript" lang="javascript" src="/js/common.js" charset="UTF-8"></script>
<script type="text/javascript" lang="javascript">
var j$  = jQuery.noConflict(); //prototype과 같은 javascript lib, 프레임웍을 같이 사용할 때 충돌을 막기 위함.
j$(document).ready(function() {
    j$('#authname').alphan();
    j$('#authname').css('ime-mode','active');
    var options = {
            target       : '#frm'   // target element(s) to be updated with server response
           ,beforeSubmit : showRequest  // pre-submit callback
           ,success      : showResponse  // post-submit callback
           ,error        : showError  // post-submit callback
           ,url          : '/manage/base/auth/saveProcess.do'
           ,type         : 'post'        // 'get' or 'post', override for form's 'method' attribute
           ,dataType     : 'json'        // 'xml', 'script', or 'json' (expected server response type)
           //clearForm: true        // clear all form fields after successful submit
           //resetForm: true        // reset the form after successful submit
           // $.ajax options can be used here too, for example:
           //timeout:   3000
       };
   j$('#frm').ajaxForm(options);
   j$('#frm').submit(function(){return false; });
   j$('#saveProcess').click(function(){
       goSaveProcess();
   });
});

goList=function(){
    var f = document.getElementById("frm");
        f.cmd.value="list";
        f.action = "/manage/base/auth/main.do";
        f.submit();
}

goSaveProcess=function(){
    var frm = document.getElementById("frm");

    if(j$('#authname').val() == '') {
        alert("권한 명을 입력해주세요.");
        return;
    } else {
        j$('#authname').val(j$.trim(j$("#authname").val()));
    }

    if(confirm("저장 하시겠습니까?")){
        j$('#frm').submit();
        return "ok";
    } else {
        return;
    }
}

showRequest=function(formData, jqForm, options) {
    var queryString = j$.param(formData);
    //alert('About to submit: \n\n' + queryString);
    return true;
}

showResponse=function(responseText, statusText, xhr, j$form)  {
    var data=eval(responseText);
    //alert(data.success);
    alert(data.message);
    if(data.success =='0000'){
        goList();
    }
    //alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + '\n\nThe output div should have already been updated with the responseText.');
}

showError=function(){
    alert('errer');
}
</script>
</body>
</html>