<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<%@ page import="java.util.*" %>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><bean:message key="site.title" /></title>
    </head>
<body>
<%@ include file="/page/common/root/topMenu.jsp"%>
<%@ include file="/page/common/root/leftMenu.jsp"%>
<div class="right">
    <div class="right_title"><h1>관리자 권한</h1><h5>관리자 권한을 관리 할 수 있습니다.</h5></div>
    <hr class="line">
        <div class="search_result">
        <table class="goods_table_02" cellpadding="0" cellspacing="0">
        <caption><img src="/images/title_note.gif"> <b>관리자 권한 리스트</b></caption>
            <colgroup><col width="200" /><col width="200" /></colgroup>
                <tbody>
                <tr>
                    <th>권한 코드명</th>
                    <th style="border-right:none;">상태</th>
                </tr>
        <c:choose>
            <c:when test="${empty items}">
                <tr><td colspan="2" style="border-right:none;">조회된 데이터가 없습니다.</td></tr>
            </c:when>
            <c:otherwise>
                <c:forEach items="${items}" var="vo" varStatus="status">
                <tr>
                    <td><a href="javascript:goView('<c:out value="${vo.authcode }"/>');"><c:out value="${vo.authname}"/></a></td>
                    <td style="border-right:none;">
                        <c:if test="${vo.authstate eq '10'}">정상</c:if>
                    </td>
                </tr>
                </c:forEach>
            </c:otherwise>
        </c:choose>
                </tbody>
        </table>
    </div>
    <div class="new_write">
    <ul class="bottom_btn">
        <li><input type="button" class="button" value="목록" onclick="goList();" style="cursor: pointer;" /></li>
        <li style="float:right"><input type="button" class="button2" value="저장" onclick="goSaveForm();" style="cursor: pointer;" /></li>
    </ul>
    </div>
    <div id="centerDiv">
        <ul class="centerUL">
            <p:pagingNavigator pageGroupSize="20" method="POST" paramValue="/manage/base/auth" cmdName="list">
                <p:pageNo><c:out value="${pager.pageNo}"/></p:pageNo>
                <p:pageMax>20</p:pageMax>
                <p:totalRecords><c:out value="${pager.totalRecords}"/></p:totalRecords>
            </p:pagingNavigator>
        </ul>
    </div>
</div>
<form name="frm" id="frm" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="method"      id="method"     value="list"/>
<input type="hidden" name="rowno"       id="rowno"      value="1"/>
<input type="hidden" name="authcode"    id="authcode"   value=""/>
<input type="hidden" name="topMenuNo"   id="topMenuNo"  value="<c:out value="${topMenuNo}"/>"/>
</form>
<script type="text/javascript" lang="javascript">
goView=function(authcode){
    var f = document.getElementById("frm");
        f.authcode.value = authcode;
        f.method.value = "view";
        f.action = "/manage/base/auth.do";
        f.submit();
}
goList=function(){
    var f = document.getElementById("frm");
        f.method.value="list";
        f.action = "/manage/base/auth.do";
        f.submit();
}
goSaveForm=function(){
    var f = document.getElementById("frm");
        f.method.value = "saveForm";
        f.action = "/manage/base/auth.do";
        f.submit();
}
</script>
</body>
</html>