<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<%@ page import="java.util.*" %>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><bean:message key="site.title" /></title>
    </head>
<body>
<%@ include file="/page/common/root/topMenu.jsp"%>
<%@ include file="/page/common/root/leftMenu.jsp"%>
<div class="right">
    <div class="right_title"><h1>관리자 권한</h1><h5>관리자 권한을 관리 할 수 있습니다.</h5></div>
    <hr class="line">
        <div class="cont">
            <table class="goods_table" cellpadding="0" cellspacing="0">
            <caption><img src="/images/title_note.gif"> <b>관리자 권한 상세보기</b></caption>
                <colgroup><col width="200" /><col /></colgroup>
                    <tbody>
                    <tr>
                        <th><img src="/images/table_icon.gif">권한 명</th>
                        <td><c:out value="${item.authname}"/></td>
                    </tr>
                    <tr>
                        <th><img src="/images/table_icon.gif">상태</th>
                        <td>
                            <c:if test="${item.authstate eq '10'}">정상</c:if>
                        </td>
                    </tr>
                    </tbody>
             </table>
        </div>
        <div class="cont">
            <table class="goods_table_03" cellpadding="0" cellspacing="0">
            <caption style="text-align:left; padding-bottom:7px;"><img src="/images/title_note.gif"> <b>사용가능한 메뉴 리스트</b></caption>
                <colgroup><col /><col /><col /><col /><col /><col /><col /></colgroup>
                    <tbody>
                    <tr>
                        <th>최상위</th>
                        <th>탑메뉴</th>
                        <th>서브메뉴</th>
                        <th>프로그램명</th>
                        <th>메뉴주소</th>
                        <th>메뉴파라미터</th>
                        <th style="border-right:none;">메뉴파라미터</th>
                    </tr>
            <c:choose>
                <c:when test="${empty items}">
                    <tr><td colspan="7">조회된 데이터가 없습니다.</td></tr>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${items}" var="vo" varStatus="status">
                    <tr>
                        <td><c:if test="${vo.menutype eq 'CD001' }"><c:out value="${vo.menuname}"/></c:if></td>
                        <td><c:if test="${vo.menutype eq 'CD002' }"><c:out value="${vo.menuname}"/></c:if></td>
                        <td><c:if test="${vo.menutype eq 'CD003' }"><c:out value="${vo.menuname}"/></c:if></td>
                        <td><c:if test="${vo.menutype eq 'CD004' }"><c:out value="${vo.menuname}"/></c:if></td>
                        <td><c:out value="${vo.menuurl}"/></td>
                        <td><c:out value="${vo.menucmd}"/></td>
                        <td style="border-right:none;"><c:out value="${vo.subtype}"/></td>
                    </tr>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
                    </tbody>
             </table>
        </div>
    <div class="new_write">
    <ul class="bottom_btn">
        <li><input type="button" class="button" value="목록" onclick="goList();" style="cursor: pointer;" /></li>
        <li style="float:right;"><input type="button" class="button2" value="수정" onclick="goSaveForm('<c:out value="${item.authcode}"/>');" style="cursor: pointer;" /></li>
        <li style="float:right;padding-right:3px;"><input type="button" class="button2" value="삭제" onclick="goDeleteProcess('<c:out value="${item.authcode}"/>');" style="cursor: pointer;" /></li>
        <li style="float:right;padding-right:3px;"><input type="button" class="button2" value="메뉴 권한 등록" onclick="goInsertMenuPair('<c:out value="${item.authcode}"/>');" style="cursor: pointer;" /></li>
    </ul>
    </div>
</div>
<form name="frm" id="frm" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="cmd"         id="cmd"        value="view"/>
<input type="hidden" name="rowno"       id="rowno"      value="1"/>
<input type="hidden" name="authcode"    id="authcode"   value=""/>
<input type="hidden" name="topMenuNo"   id="topMenuNo"  value="<c:out value="${topMenuNo}"/>"/>
</form>
<script type="text/javascript" lang="javascript" src="/js/jquery.js"></script>
<script type="text/javascript" lang="javascript" src="/js/common.js" charset="UTF-8"></script>
<script type="text/javascript" lang="javascript">
var j$  = jQuery.noConflict(); //prototype과 같은 javascript lib, 프레임웍을 같이 사용할 때 충돌을 막기 위함.
j$(document).ready(function() {
});

goList=function(){
    var f = document.getElementById("frm");
        f.method.value="list";
        f.action = "/manage/base/auth.do";
        f.submit();
}

goInsertMenuPair=function(authcode){
    var f = document.getElementById("frm");
        f.authcode.value = authcode;
        f.method.value="insertMenuPair";
        f.action = "/manage/base/menu/main.do";
        f.submit();
}

goSaveForm=function(authcode){
    var f = document.getElementById("frm");
        f.method.value = "saveForm";
        f.authcode.value = authcode;
        f.action = "/manage/base/auth.do";
        f.submit();
}

goDeleteProcess=function(authcode){
    var str = "삭제 하시겠습니까?";
    if(confirm(str)){
        j$.ajax({
            type: "POST",
            url: "/manage/base/auth.do",
            data: "method=delDong&authcode="+authcode+"&authstate=20",
            dataType: "json",
            success: function(result) {
                if(result.success == '0000'){
                    alert(result.result.message);
                    goList();
                } else {
                    alert(result.result.message);
                    return;
                }
            }
        });
    } else {
        return;
    }
}
</script>
</body>
</html>