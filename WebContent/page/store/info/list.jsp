<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<%@ page import="java.util.*" %>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><bean:message key="site.title" /></title>
    </head>
<body>
<c:if test="${userSession.authcode eq 'AT001' }">
<%@ include file="/page/common/super/topMenu.jsp"%>
<%@ include file="/page/common/super/leftMenu.jsp"%>
</c:if>
<c:if test="${userSession.authcode eq 'AT002' }">
<%@ include file="/page/common/root/topMenu.jsp"%>
<%@ include file="/page/common/root/leftMenu.jsp"%>
</c:if>
<div class="right">
    <div class="right_title"><h1>매장 정보</h1><h5>매장 정보를 조회 할 수 있습니다.</h5></div>
    <hr class="line">
    <div class="search_result">
        <table class="goods_table_02" cellpadding="0" cellspacing="0">
        <caption><img src="/images/title_note.gif"> <b>매장 정보 리스트</b></caption>
            <colgroup>
                <col />
                <col />
                <col />
                <col />
            </colgroup>
            <tbody>
                <tr>
                    <th>매장코드</th>
                    <th>매장명</th>
                    <th>매장아이디</th>
                    <th>비고</th>
                </tr>
                <c:choose>
                    <c:when test="${empty items}">
                        <tr>
                            <td colspan="4" style="border-right: none;">조회된 데이터가 없습니다.</td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${items}" var="row" varStatus="status">
                            <tr>
                                <td style="height: 20px;"><c:out value="${row.storecode}" /></td>
                                <td style="height: 20px;"><c:out value="${row.storename}" /></td>
                                <td style="height: 20px;"><c:out value="${row.storeid}" /></td>
                                <td style="height: 20px; border-right: none;"></td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </tbody>
        </table>
    </div>
    <div class="new_write">
    <ul class="bottom_btn">
        <li><input type="button" class="button" value="목록" id="listProcess"/></li>
        <li style="float:right;padding-right:5px;"><input type="button" class="button3" id="batchForm" value="일괄등록" /></li>
    </ul>
    </div>
    <div id="centerDiv">
        <ul class="centerUL">
            <p:pagingNavigator pageGroupSize="15" method="POST" paramValue="/store/info" cmdName="list">
                <p:pageNo><c:out value="${pager.pageNo}"/></p:pageNo>
                <p:pageMax>15</p:pageMax>
                <p:totalRecords><c:out value="${pager.totalRecords}"/></p:totalRecords>
            </p:pagingNavigator>
        </ul>
    </div>
</div>
<form name="frm" id="frm" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="method"    id="method"    value="list"/>
<input type="hidden" name="rowno"     id="rowno"     value="1"/>
<input type="hidden" name="rownum"    id="rownum"    value="15"/>
<input type="hidden" name="mode"      id="mode"      value="<c:out value="${search.mode}"/>"/>
<input type="hidden" name="value"     id="value"     value="<c:out value="${search.value}"/>">
<input type="hidden" name="topMenuNo" id="topMenuNo" value="<c:out value="${topMenuNo}"/>"/>
</form>
<form name="excel" id="excel" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="method"   id="method"   value="excel"/>
</form>
<form name="batch" id="batch" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="method"   id="method"   value="batchForm"/>
</form>
<form name="view" id="view" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="method"    id="method"    value="list"/>
<input type="hidden" name="rowno"     id="rowno"     value="1"/>
<input type="hidden" name="usercode"  id="usercode"  value=""/>
<input type="hidden" name="topMenuNo" id="topMenuNo" value="<c:out value="${topMenuNo}"/>"/>
</form>
<script type="text/javascript" lang="javascript" src="/js/jquery.js"></script>
<script type="text/javascript" lang="javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" lang="javascript">
var j$  = jQuery.noConflict(); //prototype과 같은 javascript lib, 프레임웍을 같이 사용할 때 충돌을 막기 위함.
j$(document).ready(function() {
    j$.ajaxSettings.traditional = true;

    j$('#modes').change(function() {
        j$('#mode').val(j$(this).val());
    });

    j$('#listProcess').click(function() {
        listProcess();
    });

    j$('#batchForm').click(function() {
    	batchForm();
    });

    j$('#excelDown').click(function() {
        excelDown();
    });

    j$('#searchProcess').click(function() {
        searchProcess();
    });
});

listProcess=function() {
    var f = document.getElementById("frm");
        f.method.value="list";
        f.action = "/store/info.do";
        f.submit();
}

batchForm=function() {
    var f = document.getElementById("batch");
        f.method.value= "batchForm";
        f.action = "/store/info.do";
        f.submit();
}

excelDown=function() {
    var f = document.getElementById("excel");
        f.method.value="excel";
        f.action = "/store/info.do";
        f.submit();
}

searchProcess=function() {
    var f = document.getElementById("search");
        f.method.value="list";
        f.action = "/store/info.do";
        f.submit();
}
</script>
</body>
</html>