<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc" %>
<%@ page import="java.util.*" %>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><bean:message key="site.title" /></title>
        <style type="text/css">
            .file_input_div {position:relative; width:100px; height:20px;top:-3px; overflow: hidden;cursor: pointer;}
            .file_input_hidden {position:absolute;border:1px solid #cccccc; right:0px; top:0px; opacity:0; filter:alpha(opacity=0); -ms-filter:"alpha(opacity=0)"; -khtml-opacity:0; -moz-opacity:0;cursor: pointer;}
        </style>
    </head>
<body>
<c:if test="${userSession.authcode eq 'AT001' }">
<%@ include file="/page/common/super/topMenu.jsp"%>
<%@ include file="/page/common/super/leftMenu.jsp"%>
</c:if>
<c:if test="${userSession.authcode eq 'AT002' }">
<%@ include file="/page/common/root/topMenu.jsp"%>
<%@ include file="/page/common/root/leftMenu.jsp"%>
</c:if>
<div class="right">
    <div class="right_title"><h1>매장 상품</h1><h5>매장 상품 가격 일괄 등록 처리를 할 수 있습니다.</h5></div>
    <hr class="line">
        <div class="cont">
            <form name="frm" id="frm" method="post" enctype="multipart/form-data" style="margin: 0px;" onsubmit="return false;">
            <input type="hidden" name="method" id="method" value="batchProcess"/>
            <table class="goods_table" cellpadding="0" cellspacing="0">
            <caption><img src="/images/title_note.gif"> <b>매장 상품 가격 일괄 등록 처리</b></caption>
                <colgroup><col width="200" /><col /></colgroup>
                    <tbody>
                    <tr>
                        <th><img src="/images/table_icon.gif">매장 상품 가격 리스트 파일</th>
                        <td>
                            <input type="text" class="box" style="float:left;width:300px; margin-right:7px;" id="tempExeclbatch" disabled="disabled">
                            <div class="file_input_div">
                                <input type="button" value="찾아보기" class="button" style="cursor: pointer;"/>
                                <input type="file" name="execlbatch" id="execlbatch" class="file_input_hidden" value="" onchange="getValue('execlbatch','tempExeclbatch');"/>
                            </div>
                        </td>
                    </tr>
                    </tbody>
             </table>
             </form>
        </div>
    <div class="new_write">
    <ul class="bottom_btn">
        <li><input type="button" class="button" value="목록" id="listProcess"/></li>
        <li style="float:right;padding-left: 5px;"><input type="button" class="button" id="reset" value="초기화" onclick="goReset();" style="cursor: pointer;"/></li>
        <li style="float:right;padding-left: 5px;"><input type="button" class="button2" id="batchProcess" value="일괄 처리" style="cursor: pointer;"/></li>
    </ul>
    </div>
</div>
<form name="batch" id="batch" method="post" style="margin: 0px;" onsubmit="return false;">
<input type="hidden" name="method"   id="method"   value="batchForm"/>
</form>
<script type="text/javascript" lang="javascript" src="/js/jquery.js"></script>
<script type="text/javascript" lang="javascript" src="/js/jquery.form.js"></script>
<script type="text/javascript" lang="javascript" src="/js/common.js?r=<%=r%>" charset="UTF-8"></script>
<script type="text/javascript" lang="javascript">
var j$  = jQuery.noConflict(); //prototype과 같은 javascript lib, 프레임웍을 같이 사용할 때 충돌을 막기 위함.
j$(document).ready(function() {
    var options = {
            target       : '#frm'   // target element(s) to be updated with server response
           ,traditional  : true
           ,beforeSubmit : showRequest  // pre-submit callback
           ,success      : showResponse  // post-submit callback
           ,error        : showError  // post-submit callback
           ,url          : '/store/item.do?method=batchProcess'
           ,type         : 'post'        // 'get' or 'post', override for form's 'method' attribute
           ,dataType     : 'json'        // 'xml', 'script', or 'json' (expected server response type)
           //clearForm: true        // clear all form fields after successful submit
           //resetForm: true        // reset the form after successful submit
           // $.ajax options can be used here too, for example:
           //timeout:   3000
       };
   j$('#frm').ajaxForm(options);
   j$('#frm').submit(function(){return false; });
   j$('#listProcess').click(function(){
       listProcess();
   });
   j$('#batchProcess').click(function(){
       goBatchProcess();
   });
});

getValue=function(oid,id){
    document.getElementById(id).value = document.getElementById(oid).value;
}

listProcess=function(){
    var f = document.getElementById("batch");
        f.method.value = "list";
        f.action = "/store/item.do";
        f.submit();
}

goReset=function(){
    var f = document.getElementById("batch");
        f.method.value = "batchForm";
        f.action = "/store/item.do";
        f.submit();
}

goBatchProcess=function(){
    var f = document.getElementById("frm");
    var b = document.getElementById("batchProcess");
    var r = document.getElementById("reset");
    var flag = false;

    flag = excelFileCheck(f.execlbatch.value);

    if(!flag){
        alert("#.xls or #.xlsx 확장자만 업로드 가능합니다[EXCEL 파일]");
        return;
    }
    if(confirm("매장 상품 가격 일괄 처리를 하시겠습니까?")){
        b.disabled=true;
        r.disabled=true;
        b.className='button3';
        r.className='button3';
        j$('#frm').submit();
        return "ok";
    } else {
        return;
    }
}

showRequest=function(formData, jqForm, options) {
    var queryString = j$.param(formData);
    //alert('About to submit: \n\n' + queryString);
    return true;
}

showResponse=function(responseText, statusText, xhr, j$form)  {
    var data=eval(responseText);
    //alert(data.success);
    //alert(data.message);
    if(data.success =='0000'){
        goReset();
    }
    //alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + '\n\nThe output div should have already been updated with the responseText.');
}

showError=function(){
    alert('errer');
    j$("#sendHidden").html('');
}
</script>
</body>
</html>