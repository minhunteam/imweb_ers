<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
    // 요거이 그겁니다. 서버 ip
    InetAddress inet = InetAddress.getLocalHost();
    String ip = request.getHeader("X-Forwarded-For");
    //out.println(inet.getHostAddress());
    //out.println(request.getRemoteAddr());
    //out.println(ip);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Cache-Control" content="no-cache" />
        <meta http-equiv="Expires" content="0" />
        <meta http-equiv="Pragma" content="no-cache" />
        <link type="text/css" rel="stylesheet" href="/css/admin_login.css?r=<%=r%>">
        <title><bean:message key="site.title" /></title>
    </head>
<body class="login_wrap">
    <form name="frm" id="frm" method="post" style="margin: 0px;"
        onsubmit="return false;">
        <input type="hidden" name="method" id="method" value="login" />
        <input type="hidden" name="route" id="route" value="R002" />
        <div id="wrap" class="login_gnb">
            <div class="login_title">
                <img src="/images/login_logo.gif" alt="Administrator Managements System" />
            </div>
            <div class="login_bar">
                <ul>
                    <li><img src="/images/login_text.gif" alt="관리자/운영자 로그인" /></li>
                    <li class="login2">
                        <dl>
                            <%
                                if (inet.getHostAddress().equals("192.168.0.250") || inet.getHostAddress().equals("192.168.1.2") || inet.getHostAddress().equals("211.177.246.234")) {
                            %>
                            <dt>
                                <img src="/images/login_id.gif" alt="아이디" />
                            </dt>
                            <dd>
                                <input type="text" name="userid" id="userid" value="supervisor" />
                            </dd>
                            <dt style="clear: both;">
                                <img src="/images/login_pass.gif" alt="패스워드" />
                            </dt>
                            <dd>
                                <input type="password" name="userpw" id="userpw" value="supervisor" onkeydown="if (event.keyCode == 13){ login();return false;}" />
                            </dd>
                            <%
                                } else {
                            %>
                            <dt>
                                <img src="/images/login_id.gif" alt="아이디" />
                            </dt>
                            <dd>
                                <input type="text" name="userid" id="userid" value="" />
                            </dd>
                            <dt style="clear: both;">
                                <img src="/images/login_pass.gif" alt="패스워드" />
                            </dt>
                            <dd>
                                <input type="password" name="userpw" id="userpw" value="" onkeydown="if (event.keyCode == 13){ login();return false;}" />
                            </dd>
                            <%
                                }
                            %>
                        </dl>
                        <p>
                            <img src="/images/login_btn.gif" alt="로그인" border="0" onclick="login();" style="cursor: pointer;" />
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </form>
</body>
<script type="text/javascript" lang="javascript">
    login = function() {
        var f = document.getElementById("frm");
        if (f.userid.value == '') {
            alert("아이디를 입력해 주세요.");
            return false;
        }
        if (f.userpw.value == '') {
            alert("패스워드를 입력해 주세요.");
            return false;
        }
        f.action = "/connect.do";
        f.submit();
    }
</script>
</html>