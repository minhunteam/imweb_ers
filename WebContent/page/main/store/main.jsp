<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common-tag-define.inc"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%
    if(request.getProtocol().equals("HTTP/1.0")) {
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
    } else if(request.getProtocol().equals("HTTP/1.1")) {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    } else {
        response.setHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
    }
    Random oRandom = new Random();
    int r = oRandom.nextInt(99999999) + 1;
    // 요거이 그겁니다. 서버 ip
    InetAddress inet = InetAddress.getLocalHost();
    String ip = request.getHeader("X-Forwarded-For");
    //out.println(inet.getHostAddress());
    //out.println(request.getRemoteAddr());
    //out.println(ip);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Cache-Control" content="no-cache"/>
        <meta http-equiv="Expires" content="0"/>
        <meta http-equiv="Pragma" content="no-cache"/>
        <link type="text/css" rel="stylesheet" href="/css/admin_body.css?r=<%=r%>">
        <title><bean:message key="site.title" /></title>
    </head>
<body>
<%@ include file="/page/common/store/topMenu.jsp"%>
<%@ include file="/page/common/store/leftMenu.jsp"%>
<div class="right">
    <table border="0" width="100%" align="center">
        <tr>
            <td style="vertical-align: text-bottom;">
                <table width="100%">
                    <tr>
                        <td><br/></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
<script type="text/javascript" lang="javascript">
    login = function() {
        var f = document.getElementById("frm");
        if (f.userid.value == '') {
            alert("관리자 아이디를 입력해 주세요.");
            return false;
        }
        if (f.userpw.value == '') {
            alert("관리자 패스워드를 입력해 주세요.");
            return false;
        }
        f.action = "/manage/connect/login.do";
        f.submit();
    }
</script>
</html>